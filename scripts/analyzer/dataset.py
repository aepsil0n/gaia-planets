from itertools import *
from defs import *

class dataset(object):
	""" Class to handle reading of statistically processed simulation results. """
	
	""" Methods """
	
	def __init__(self):
		self._keys = ['name', 'di_tot', 'ddi_tot', 'i0', 'ntr', 'ac', 'msini', 'period', 'ecc', 'tperi', 'argperi', 'thetaedge', 'lambda', 'beta', 'mura', 'mudec', 'plx', 'sigplx', 'mstar', 'di_stat', 'ddi_stat', 'i_limit_low', 'i_limit_high']
		self._converters = [str]+[float]*3+[int]*2+[float]*17
		self._filter_dict = {}
	
	def read_data(self, filename):
		""" Reads statistically averaged data from given file.
# Expected format
#  0 Name
#  1 Inclination precision (total) [rad]
#  2 Inclination precision (total) uncertainty [rad]
#  3 True inclination [rad]
#  4 Number of transits
#  5 Across scan information? (1: yes, 0: no)
#  6 Projected mass [M_J]
#  7 Orbital period [d]
#  8 Eccentricity
#  9 Time of periastron [JD]
# 10 Argument of periastron [rad]
# 11 Edge-on astrometric signature [muas]
# 12 Ecliptic longitude [deg]
# 13 Ecliptic latitude [deg]
# 14 Right ascension proper motion [mas/yr]
# 15 Declination proper motion [mas/yr]
# 16 Parallax [mas]
# 17 Averaged parallax error [mas]
# 18 Stellar mass [M_solar]
# 19 Inclination precision (stat) [rad]
# 20 Inclination precision (stat) uncertainty [rad]
# 21 Minimum detectable inclination angle [rad]
# 22 Lower limit on inclination angle [rad]
# 23 Upper limit on inclination angle [rad]
		 """
		datafile = open(filename)
		self._data = []
		for line in [x.split("\t") for x in datafile]:
			# Standard values
			dp = dict([(key, conv(line[index])) for index, key, conv in zip(range(len(self._keys)), self._keys, self._converters)])
			# Derived values
			dp['signal'] = signal(dp['thetaedge'], dp['ecc'], dp['i0'])
			dp['noise'] = noise(dp['sigplx'], dp['ntr'], dp['ac'])
			dp['snra'] = dp['signal']/dp['noise']
			dp['snrb'] = dp['signal']/dp['noise']/sin(dp['i0'])
			dp['snrrv'] = dp['signal']/dp['noise']*sin(dp['i0'])
			self._data.append(dp)
		datafile.close()
	
	def _get_data(self):
		""" Returns list of data. """
		return self._data
	
	def add_filter(self, name, filter_fct):
		""" Adds a filter to the filter dictionary. """
		self._filter_dict[name] = filter_fct
	
	def get_filtered_data(self, the_filter):
		""" Returns filtered data. """
		if type(the_filter) == str:
			return ifilter(self._filter_dict[the_filter], self.data)
		if callable(the_filter):
			return ifilter(the_filter, self.data)
		return self.data
		
	def get_column(self, key, fkey=None):
		""" Returns column corresponding to key.
		key: corresponding key
		fkey: name of filter for the data
		"""
		return imap(lambda point: point[key], self.get_filtered_data(fkey))
	
	def add_column(self, key, cfct):
		self._keys.append(key)
		for dp in self._data:
			dp[key] = cfct(dp)
	
	""" Properties """
	
	data = property(_get_data)
	

if __name__ == '__main__':
	test_dataset = dataset()
	test_dataset.read_data('proc/random3/scaling')
	subset = islice(test_dataset.get_column('i0'), 0, 10)
	for x in subset:
		print x
	
