'''
This script illustrates the problem with nuisance parameters.
'''

# Imports

import reader


# Main class

if __name__ == '__main__':

    nuiparse = reader.SimulationOutputParser('/home/eduard/dev/science/esac/priv/sim_results/output/nuisance')

    data = nuiparse.read_system('equator_short', 1)

    print data['i_fit']


