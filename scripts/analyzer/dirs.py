"""
Module containing directory specifications.
"""

NAME = 'rnd-full'
PATH = '/home/eduard/dev/science/esac/priv/sim_results/'
DATADIR = PATH + ('output/%s/' % (NAME))
PLOTDIR = PATH + ('plot/%s/' % (NAME))
PROCDIR = PATH + ('proc/%s/' % (NAME))

