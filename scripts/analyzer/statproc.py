"""
Python script to process output of beta fits -- calculates means and standard deviations
"""

import random
import os
import re
import numpy as np
from math import *
import string
from scipy.special import chdtr
from dirs import *
from scipy.optimize import leastsq

# Constants
syslistfile = open(DATADIR+"sys")
SYSNAMES = [sys.strip() for sys in syslistfile]
syslistfile.close()
CONF_LOW = 0.05 # Lower probability limit
CONF_HIGH = 0.95 # Upper probability limit
NPARAM = 9  # Number of fitting parameters

# Functions
def adjustIncl(i):
	while i < 0:
		i += pi
	while i > pi:
		i -= pi
	return i
col = lambda ll,i: map(lambda p: p[i], ll)

# Setup matplotlib
import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plt
import matplotlib.font_manager as pfmgr
import matplotlib.colors as clrs
font = pfmgr.FontProperties
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=18)
rc('legend', fontsize=18)
rc('axes', labelsize=22)
rc('xtick', labelsize=18)
rc('ytick', labelsize=18)

# Bias array
globbiases = []
biasAnalysisSys = []#['rnd_226', 'rnd_389']#random.sample(SYSNAMES, 10)
histSys = []

# Ensure existence of output parent dir 
for path in [PROCDIR]+[PLOTDIR+sys+'/fits' for sys in biasAnalysisSys]:
	if not os.path.exists(path):
		os.makedirs(path)

scFile = open(PROCDIR+"scaling", "w")

# Read out file with list of simulated systems 
listFile = open(DATADIR+"list")
listLines = [map(lambda s: s.strip(), line.split(",")) for line in listFile]
listFile.close()

# Calculate statistical properties of beta fits
for sys in SYSNAMES:
	
	# Ensure existence of output dir
	if not os.path.exists(PROCDIR+sys):
		os.mkdir(PROCDIR+sys)
	
	# Open output file
#	sFile = open(PROCDIR+sys+"/errors", "w")
	infoRetrieved = False
	sysInfo = [[], sys]
	# Loop through simulations
	sims = sorted(os.listdir(DATADIR+sys+"/"))
	
	# System bias list
	sysbias = []
	
	# Open system bias output file
	sysBiasFile = None
	if len(biasAnalysisSys) != 0:
		sysBiasFile = open(PROCDIR+sys+"/biases", "w")
	
	for sim in sims:
		
		# Read and parse head lines
		fits = []
		try:
			dFile = open(DATADIR+sys+"/"+sim+"/results")
			fits = map(lambda line: map(lambda value: float(value), line.split()), dFile)
		except IOError:
			print 'No file found for %s, %s!' % (sys, sim)
			pass
		else:
			dFile.close()
		
		# Filter for good fits
		fitsGood = filter(lambda fit: CONF_LOW < chdtr(fit[28]-NPARAM, fit[27]*float(fit[28]-NPARAM)) < CONF_HIGH, fits)
		NFITS = len(fitsGood)
		
		if NFITS > 10:
			k = random.randint(0, len(fitsGood)-1)
			
			# Calculate mean and std. deviation 
			# Inclination
			trueIncl = fitsGood[k][0]
			adjIncls = [adjustIncl(fit[1]) for fit in fitsGood]
			meanIncl = sum(adjIncls) / NFITS
			sigmaInclTotal = (sum([(adjinc-trueIncl)**2 for adjinc in adjIncls]) / (NFITS-1)) ** .5
			sigmaInclStat = (sum([(adjinc-meanIncl)**2 for adjinc in adjIncls]) / (NFITS-1)) ** .5
			
			# Calculate maximum detectable inclination / lower limit in case of non-detection
			idx = NFITS*0.025
			adjIncls_sort = sorted(adjIncls)
			inclLimitBottom = adjIncls_sort[int(idx)]*(1.0 - idx + int(idx)) + adjIncls_sort[int(idx)+1]*(idx - int(idx))
			inclLimitTop = adjIncls_sort[-int(idx)]*(1.0 - idx + int(idx)) + adjIncls_sort[-int(idx)-1]*(idx - int(idx))
			
			# Right ascension, alpha
			trueAlpha = fitsGood[k][12]
			meanAlpha = sum([fit[13] for fit in fitsGood]) / NFITS
			sigmaAlphaTotal = (sum([(fit[13]-trueAlpha)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			sigmaAlphaStat = (sum([(fit[13]-meanAlpha)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			
			# Declination, delta
			trueDelta = fitsGood[k][15]
			meanDelta = sum([fit[16] for fit in fitsGood]) / NFITS
			sigmaDeltaTotal = (sum([(fit[16]-trueDelta)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			sigmaDeltaStat = (sum([(fit[16]-meanDelta)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			
			# Parallax
			trueParallax = fitsGood[k][18]
			meanParallax = sum([fit[19] for fit in fitsGood]) / NFITS
			sigmaParallaxTotal = (sum([(fit[19]-trueParallax)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			sigmaParallaxStat = (sum([(fit[19]-meanParallax)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			
			# True arc right ascension proper motion
			trueMuAlpha = fitsGood[k][21]
			meanMuAlpha = sum([fit[22] for fit in fitsGood]) / NFITS
			sigmaMuAlphaTotal = (sum([(fit[22]-trueMuAlpha)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			sigmaMuAlphaStat = (sum([(fit[22]-meanMuAlpha)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			
			# Declination proper motion
			trueMuDelta = fitsGood[k][24]
			meanMuDelta = sum([fit[25] for fit in fitsGood]) / NFITS
			sigmaMuDeltaTotal = (sum([(fit[25]-trueMuDelta)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			sigmaMuDeltaStat = (sum([(fit[25]-meanMuDelta)**2 for fit in fitsGood]) / (NFITS-1)) ** .5
			
			# Write to file
			out = [fitsGood[k][0], fitsGood[k][1], sigmaInclTotal, sigmaInclStat, fitsGood[k][2], fitsGood[k][12], fitsGood[k][13], sigmaAlphaTotal, sigmaAlphaStat, fitsGood[k][14], fitsGood[k][15], fitsGood[k][16], sigmaDeltaTotal, sigmaDeltaStat, fitsGood[k][17], fitsGood[k][18], fitsGood[k][19], sigmaParallaxTotal, sigmaParallaxStat, fitsGood[k][20], fitsGood[k][21], fitsGood[k][22], sigmaMuAlphaTotal, sigmaMuAlphaStat, fitsGood[k][23], fitsGood[k][24], fitsGood[k][25], sigmaMuDeltaTotal, sigmaMuDeltaStat, fitsGood[k][26], fitsGood[k][27], NFITS]
#			s = ""
#			for x in out:
#				s += str(x)+"\t"
#			s = s.strip("\t")
#			sFile.write(s+"\n")
			
			# Get additional simulation info
			if not infoRetrieved:
				plLine = []
				stLine = []
				namesys = sys
				for x in listLines:
					if x[0] == "P" and (x[1] == namesys + "_b" or x[1] == namesys + " b" or x[1] == namesys + "_c" or x[1] == namesys + " c"):
						plLine = x
					if x[0] == "S" and x[1] == namesys:
						stLine = x
				dFile = open(DATADIR+sys+"/"+sim+"/results")
				statLine = map(lambda line: line.split(), dFile)[0]
				dFile.close()
				
				alpha = float(stLine[5]) / 12.0 * pi
				delta = float(stLine[6]) / 180.0 * pi
				axtilt = (23.0 + 26.0/60.0 + 21.406/3600.0) / 180.0 * pi
				eclong = 180.0/pi*atan2((sin(alpha)*cos(axtilt)+tan(delta)*sin(axtilt)), cos(alpha))
				eclat = 180.0/pi*asin(sin(delta)*cos(axtilt)-cos(delta)*sin(alpha)*sin(axtilt))
				
#				print statLine, stLine, plLine
#				print len(statLine), len(stLine), len(plLine)
				astrosini = float(plLine[2]) * .0009546 * float(stLine[9]) * 1e-3 * (float(plLine[4]) / 365.25 / float(stLine[2])) ** (2./3.)
				sysInfo = [[statLine[28], statLine[29], plLine[2], plLine[4], plLine[6], plLine[8], plLine[10], str(astrosini*1e6), str(eclong), str(eclat), stLine[7], stLine[8], stLine[9], statLine[33], stLine[2]], plLine[1]]
				infoRetrieved = True

			scOut = [sysInfo[1].replace(' ', '_'), sigmaInclTotal, sigmaInclTotal/sqrt(float(2*(NFITS-1))), trueIncl]+sysInfo[0]+[sigmaInclStat, sigmaInclStat/sqrt(float(2*(NFITS-1))), inclLimitBottom, inclLimitTop]
			scFile.write(('%s\t'*len(scOut)+'\n') % tuple(map(str, scOut)))
			
			# Create inclination histogram -- not needed any more
			if sys in histSys:
				histRange = 5.0
				histLower = trueIncl - histRange*sigmaInclTotal
				histUpper = trueIncl + histRange*sigmaInclTotal
				histNBins = 20
				histCounts = [0]*histNBins
				for fit in fitsGood:
					incl = adjustIncl(fit[1])
					if histLower < incl < histUpper:
						bin = int((incl-histLower)/(histUpper-histLower)*histNBins)
						histCounts[bin] += 1
				histFile = open(PROCDIR+sys+"/hist."+sim, "w")
				for bin in range(histNBins):
					binCenter = histLower+(histUpper-histLower)/histNBins*(bin+0.5)
					histFile.write("%e %e %d\n" % (binCenter, (binCenter-trueIncl)/sigmaInclTotal, histCounts[bin]))
				histFile.close()

		
		# Bias analysis 
		# Prepare data for bias analysis
		nSample = filter(lambda k: k < NFITS, xrange(5,100,5))
		if len(nSample) > 3 and sys in biasAnalysisSys:
			print 'Performing bias analysis for %s, sim. %s.' % (sys, sim)
			biases = []
			for nn in nSample:
				sigSysSample = []
				NSAMP = 100
				for i in range(NSAMP):
					randSample = random.sample(fitsGood, nn)
					mu = sum([adjustIncl(fit[1]) for fit in randSample]) / nn
					sigSysSample.append(abs(mu - trueIncl))
				sigSys = sum(sigSysSample)/NSAMP
				dSigSys = sqrt(sum(map(lambda x: (x-sigSys)**2, sigSysSample)) / (NSAMP**2 - NSAMP))
				biases.append((nn, sigSys, dSigSys, trueIncl))
			
			# Do the individual simulation fits
			model1 = lambda p, N: p[0]*sqrt(float(NFITS)/float(N))
			model2 = lambda p, N: sqrt(p[0]**2*float(NFITS)/float(N) + p[1]**2)
			errf = lambda model: (lambda p, data: [(model(p, point[0]) - point[1])/point[2] for point in data])
			par1, cov1, dictinfo1, msg1, ier1 = leastsq(errf(model1), [.01], args=(biases), full_output=True)
			par2, cov2, dictinfo2, msg2, ier2 = leastsq(errf(model2), [.01, .01], args=(biases), full_output=True)
			dof1 = float(len(biases)-1)
			dof2 = float(len(biases)-2)
			dpar1 = par1
			dpar2 = par2[:]
			if cov1 != None:
				dpar1 = sqrt(cov1[0][0])
			if cov2 != None:
				dpar2 = map(sqrt, [cov2[0][0], cov2[1][1]])
			chi2 = lambda model, par, data: sum(map(lambda x: x**2, errf(model)(par, data)))
			csq1 = chi2(model1, [par1], biases)
			csq2 = chi2(model2, par2, biases)
			dsig = dpar2
			sig = map(abs, par2)
			pval = chdtr(dof1, csq1)
			if pval < 0.95 or csq1/dof1 < csq2/dof2:
				sig = [par1, 0]
				dsig = [dpar1, 0]
			# An alternative for 0; not working as expected though...
			# dsig[0] = 1/sqrt(dpar1**-2 + dpar2[0]**-2)
			# dsig[1] = dsig[0]**2*(par1/dpar1**2 + par2[0]/dpar2[0]**2)
			ratio = [sig[i]/sigmaInclStat for i in [0, 1]]
			dratio = [sqrt(dsig[i]**2 + sig[i]**2/NFITS) / sigmaInclStat for i in [0, 1]]
			summary = (trueIncl, meanIncl, sigmaInclTotal, sigmaInclStat, sig[0], dsig[0], sig[1], dsig[1], ratio[0], dratio[0], ratio[1], dratio[1], NFITS, csq1, csq2, dof1, dof2, pval)
			sysbias.append(summary)
			
			# Plot the fits 
			plt.figure()
			plt.errorbar(col(biases, 0), col(biases, 1), yerr=col(biases, 2), fmt='.', color='#2080c0', capsize=0, lw=0.3)
			xvals = np.linspace(5.0, 100.0, 100)
			plt.plot(xvals, map(lambda x: model1([par1], x), xvals), '#2050ff', label=r'Unbiased model, $\chi^2_{\mathrm{red}} = %.2f$'%(csq1/dof1))
			plt.plot(xvals, map(lambda x: model2(par2, x), xvals), '#ff4020', label=r'Biased model, $\chi^2_{\mathrm{red}} = %.2f$'%(csq2/dof2))
			leg = plt.legend(prop=font(size=11))
			leg.draw_frame(False)
			plt.xlabel(r'Number of fits')
			plt.ylabel(r'Mean deviation $\left[\mathrm{rad}\right]$')
			axes = plt.gca()
			plt.savefig(PLOTDIR+sys+'/fits/bias-'+sim+'.pdf')
			
			# Plot a histogram of the values
		if sys in biasAnalysisSys:
			plt.figure()
			plt.hist(map(lambda x: x*180.0/pi, col(fitsGood, 1)), bins=20, color='#3060ff', zorder=3)
			plt.axvline(trueIncl*180.0/pi, lw=2, color='#800000', zorder=2)
			plt.axvspan((meanIncl-sigmaInclStat/sqrt(NFITS))*180.0/pi, (meanIncl+sigmaInclStat/sqrt(NFITS))*180.0/pi, color='r', alpha=0.5, zorder=1)
			plt.xlabel(r'$i \, \left[^\circ\right]$')
			plt.ylabel(r'Bin frequency')
			plt.savefig(PLOTDIR+sys+'/fits/hist-'+sim+'.pdf')
	
	if sys in biasAnalysisSys:
		# Plot system biases
		plt.figure()
		draw0 = (col(sysbias, 0), col(sysbias, 8), col(sysbias, 9))
		plt.errorbar(map(lambda x: x*180.0/pi, draw0[0]), draw0[1], draw0[2], fmt='.', color='#ff0000', capsize=0, lw=0.3, label=r'$\sigma_0$')
		draw1 = (col(sysbias, 0), col(sysbias, 10), col(sysbias, 11))
		plt.errorbar(map(lambda x: x*180.0/pi, draw1[0]), draw1[1], draw1[2], fmt='.', color='#20c060', capsize=0, lw=0.3, label=r'$\sigma_1$')
		plt.ylim([-0.2, 2.2])
		leg = plt.legend(prop=font(size=11))
		leg.draw_frame(False)
		plt.xlabel(r'True inclination $\left[^\circ\right]$')
		plt.ylabel(r'Error components $\left[\sigma_{\mathrm{stat}}\right]$')
		plt.savefig(PLOTDIR+sys+'/incl-bias.pdf')
		
		# Compile global bias list
		bratios = col(sysbias, 1)
		bchisqs = map(lambda x: x[3], sysbias)
		globbiases.append((max(bratios), sum(bratios)/len(bratios), max(bchisqs), sum(bchisqs)/len(bchisqs)))

	# Close system file
	#sFile.close()

	print sys+" done."

if len(globbiases) != 0:
	biasesFile = open(PROCDIR+"biases", "w")
	for b in globbiases:
		biasesFile.write(('%.6f '*len(b)+'\n') % b)
	biasesFile.close()
