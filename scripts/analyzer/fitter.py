import time
import scipy.optimize
import scipy.interpolate
import numpy as np
from math import *
from itertools import *

def lsq_fit(fdata, ffct, fguess):
	""" General least squares fitting function. """
	par, cov, dictinfo, msg, ier = scipy.optimize.leastsq(ffct, fguess, args=(fdata), full_output=True)
	if len(fguess) == 1:
		par = [par]
	chisq = sum([x*x for x in ffct(par, fdata)])
	dof = len(fdata) - len(fguess)
	dpar = [sqrt(abs(cov[i][i])) for i in range(len(par))]
	return [par, dpar, chisq, dof]


def spline_ipl(gX, gY, gZ):
	""" Spline interpolator wrapper asking for grid and values at points. """
	nX = len(gX)
	nY = len(gY)
	return scipy.interpolate.RectBivariateSpline(gX, gY, np.array(gZ).reshape(nX, nY))


def splinefit_2d(X, Y, Z, DZ, gX, gY):
	""" Fits a spline to the 2d data.
	X, Y, Z, DZ -- data
	gX, gY -- spline grid
	"""
	# TODO: check dims and raise ValueError
	gX.sort()
	gY.sort()
	def err_fct(par, data):
		#x = time.time()
		spline = spline_ipl(gX, gY, par)
		ret = [(spline(d[0], d[1])[0][0] - d[2])/d[3] for d in data]
		#print len(data), time.time()-x
		return ret
	#print 'Creating list'
	data = zip(list(X), list(Y), list(Z), list(DZ))
	#print 'Fitting'
	par, dpar, chisq, dof = lsq_fit(data, err_fct, [0.0]*len(gX)*len(gY))
	#print 'Done.'
	return par, dpar, chisq, dof
	

grid_log_snr = np.array([-1.0, 0.0, 1.0, 1.1, 1.2, 1.3, 1.7, 2.8, 4.0])
#np.linspace(0.0, 4.0, 20)
grid_inc = np.linspace(pi/18-1e-5, pi/2+1e-5, 5)
