### Modules

from dirs import *
from dataset import *
from defs import *
from fitter import *

from itertools import *
from math import *
import numpy as np


### Read database

main_dataset = dataset()
main_dataset.read_data(PROCDIR+"scaling")
main_dataset.add_filter('p-good', lambda x: good_period(x['period']) and x['snra'] > 0.1)
# Column selector shortcut
col = main_dataset.get_column


### Model fits

# Spline model i vs. SNR 
print 'Fitting spline model for i, SNR...'

X = map(log10, col('snra', 'p-good'))
Y = list(col('i0', 'p-good'))
Z = map(log10, col('di_stat', 'p-good'))
DZ = map(lambda x,dx: dx/x, col('di_stat', 'p-good'), col('ddi_stat', 'p-good'))
gX_fit = grid_log_snr
gY_fit = grid_inc

spl_par, spl_dpar, spl_csq, spl_dof = splinefit_2d(X, Y, Z, DZ, gX_fit, gY_fit)
tempfile = open(PROCDIR+'splinefit', 'w')
for p, dp in zip(spl_par, spl_dpar):
	tempfile.write('%e %e %e %d\n' % (p, dp, spl_csq, spl_dof))
tempfile.close()

print 'csq    = %.2f' % (spl_csq)
print 'dof    = %d' % (spl_dof)
print 'csqred = %.2f' % (spl_csq/spl_dof)

