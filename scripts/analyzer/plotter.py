""" Plotting wrapper module.
"""

""" Matplotlib setup """

import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plt
import matplotlib.font_manager as pfmgr
import matplotlib.colors as clrs
from mpl_toolkits.mplot3d import Axes3D
font = pfmgr.FontProperties
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif', size=18)
rc('legend', fontsize=18)
rc('axes', labelsize=22)
rc('xtick', labelsize=18)
rc('ytick', labelsize=18)


""" Helper methods """

def findmaxmin(it):
	""" Get list, maximum and minimum of iterable object. """
	xmax, xmin = None, None
	for x in it:
		if x > xmax or xmax == None:
			xmax = x
		if x < xmin or xmin == None:
			xmin = x
	return xmax, xmin


""" Latex output """

def grid2tex(X, Y, Z, DZ, xlabel, ylabel, zfmt='%.3f', xfmt='%.0f', yfmt='%.1f'):
	""" Generates latex table from grid values. """

	head_str = r"""
\begin{tabular}{l |""" + ' c'*len(X) + r"""}
"""+ylabel+r"""& \multicolumn{"""+str(len(X))+r"""}{c}{"""+xlabel+r"""} \\
"""
	foot_str = r"""
\end{tabular}
"""
	lines = []
	lines.append(''.join([(r'& $'+xfmt+r'$ ') % (x) for x in X]) + r'\\' + '\n')
	lines.append(r'\hline' + '\n')
	for y, zline, dzline in zip(Y, Z, DZ):
		lines.append(((r'$'+yfmt+r'$ ') % (y)) + ''.join([(r'& $'+zfmt+r' \pm '+zfmt+r'$ ') % (z,dz) for z,dz in zip(zline, dzline)]) + r'\\' + '\n')
	return head_str + ''.join(lines) + foot_str


""" Plotter class """

class plotter(object):

	def __init__(self, outdir):
		""" Constructor setting up stuff. """
		
		self._ax = None
		self._outdir = outdir
		self._ext = '.pdf'
		

	def scatter(self, xiter, yiter, **kwargs):
		""" Plot x over y as scatter plot. """
		
		# Get keyword arguments
		color = kwargs.get('color', '#000000')
		marker = kwargs.get('marker', 'o')
		size = kwargs.get('size', 1)
		label = kwargs.get('label', None)
		
		self.init(**kwargs)
		
		# Make lists
		xlist, ylist = map(list, [xiter, yiter])
		
		# Plot
		plt.scatter(xlist, ylist, s=size, c=color, edgecolor='none', marker=marker, label=label)
		
		self.finish(**kwargs)
	
	
	def hist(self, values, **kwargs):
		""" Make histogram of values. """
		
		# Get keyword arguments
		bins = kwargs.get('bins', 20)
		
		self.init(**kwargs)
		
		# Make lists
		thelist = list(values)
		
		# Plot
		plt.hist(thelist, bins=bins)
		
		self.finish(**kwargs)
	
	
	def line(self, xiter, yiter, **kwargs):
		""" Plot x over y as scatter plot. """
		
		# Get keyword arguments
		color = kwargs.get('color', None)
		label = kwargs.get('label', None)
		fmt = kwargs.get('fmt', '-')
		
		# Listify
		xlist, ylist = list(xiter), list(yiter)
		
		# Plot
		self.init(**kwargs)
		if color == None:
			plt.plot(xlist, ylist, fmt, label=label)
		else:
			plt.plot(xlist, ylist, fmt, color=color, label=label)
		self.finish(**kwargs)
	
	def text(self, x, y, s, **kwargs):
		""" Add some string s at coordinates x,y. """
		
		self.init(**kwargs)
		plt.text(x, y, s, transform = self._ax.transAxes)
		self.finish(**kwargs)
		

	def error(self, xiter, yiter, dyiter, **kwargs):
		""" Plot x over y as scatter plot. """
		
		# Get keyword arguments
		color = kwargs.get('color', '#c00000')
		fmt = kwargs.get('fmt', '.')
#		mpl_kwargs = {}
#		for item in filter(lambda item: 'mpl_' in item[0], kwargs.items()):
#			mpl_kwargs[item[0]] = item[1]
		
		self.init(**kwargs)
		
		# Plot
		xlist, ylist, dylist = map(list, [xiter, yiter, dyiter])
		plt.errorbar(xlist, ylist, dylist, color=color, fmt=fmt)

		self.finish(**kwargs)
	
	
	def contour(self, xiter, yiter, zlist, **kwargs):
		""" Makes a contour plot. """
		
		# Get keyword arguments
		lvl = kwargs.get('lvl', None)
		colors = kwargs.get('colors', '#000000')
		clevels = 20
		cdict = None
		if type(lvl) == dict:
			cdict = lvl
			clevels = lvl.keys()
		if type(lvl) in [list, int]:
			clevels = lvl
		
		self.init(**kwargs)
		
		# Plot
		xlist, ylist = map(list, [xiter, yiter])
		cplt = plt.contour(xlist, ylist, zlist, clevels, linewidths=1.0, linestyles='solid', colors=colors)
		plt.clabel(cplt, fmt=cdict)
		
		self.finish(**kwargs)
	
	
	def init(self, **kwargs):
		""" Plot initial. """
		
		doinit = kwargs.get('init', True)
		solo = kwargs.get('solo', False)
		
		if doinit and not solo:
			# Initialize figure
			plt.figure()
			self._ax = plt.subplot(111)
	
	
	def finish(self, **kwargs):
		""" Plot final. """
		
		dofinish = kwargs.get('finish', True)
		solo = kwargs.get('solo', False)
		
		if dofinish and not solo:
			# Get keyword arguments
			xlabel = kwargs.get('xlabel', '')
			ylabel = kwargs.get('ylabel', '')
			log = kwargs.get('log', False)
			xlog = kwargs.get('xlog', False)
			ylog = kwargs.get('ylog', False)
			xlim = kwargs.get('xlim', None)
			ylim = kwargs.get('ylim', None)
			xticks = kwargs.get('xticks', None)
			yticks = kwargs.get('yticks', None)
			fname = kwargs.get('fname', 'default')
			legend = kwargs.get('legend', False)
			legend_loc = kwargs.get('legend_loc', 1)
			vline = kwargs.get('vline', None)
			
			# Add vertical line
			if vline != None:
				plt.axvline(x=float(vline), lw=2.0, color='#ffa020')
			
			# Add labels
			plt.xlabel(xlabel)
			plt.ylabel(ylabel)
			
			# Put to log scale if requested
			if xlog or log:
				self._ax.set_xscale('log')
			if ylog or log:
				self._ax.set_yscale('log')
			
			# Set limits
			if xlim != None:
				plt.xlim(xlim)
			if ylim != None:
				plt.ylim(ylim)
			
			# Set ticks
			if type(xticks) == dict:
				plt.xticks(xticks.keys(), xticks.values())
			elif xticks != None:
				plt.xticks(xticks)
			if type(yticks) == dict:
				plt.yticks(yticks.keys(), yticks.values())
			elif yticks != None:
				plt.yticks(yticks)
			
			# Add legend
			if legend:
				leg = plt.legend(loc=legend_loc)
				#leg.draw_frame(False)

			# Save in file
			plt.savefig(self._outdir + fname + self._ext)
			
			# Reset axes
			self._ax = None
