# Stuff...

def spline(p, sn):
	# Points
	x0, y0 = 0.0, log10(pi/(2.0*sqrt(3)))
	x1, y1, b1 = 1.0, p[0], p[1]
	x2, y2, c1 = 3.0, p[2], p[3]
	
	# Calculate parameters 
	a2 = 3*(y1-y0)/(x1-x0)**2 - b1/(x1-x0)
	a3 = b1/(x1-x0)**2 + 2*(y0-y1)/(x1-x0)**3
	b2 = 3*(y2-y1)/(x2-x1)**2 - (2*b1-c1)/(x2-x1)
	b3 = -2*(y2-y1)/(x2-x1)**3 + (c1+b1)/(x2-x1)**2
	
	# Functions
	f0 = lambda x: y0
	f1 = lambda x: y0 + a2*(x-x0)**2 + a3*(x-x0)**3
	f2 = lambda x: y1 + b1*(x-x1) + b2*(x-x1)**2 + b3*(x-x1)**3
	f3 = lambda x: y2 + c1*(x-x2)
	
	# Determine interval
	x = log10(sn)
	y = 0
	if x <= x0:
		y = f0(x)
	elif x <= x1:
		y = f1(x)
	elif x <= x2:
		y = f2(x)
	else:
		y = f3(x)
	return 10**y
