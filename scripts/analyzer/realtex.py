### Modules

from dataset import *
from defs import *
from plotter import *
from fitter import *

from itertools import *
from math import *
import operator as op
import scipy.stats
import numpy as np
import random

### Helper functions

def formatName(name):
	name = name.replace('_', '~')
	for gr in greek_letters:
		name = ('#'+name).replace('#'+gr+'~', '#$\\'+gr+'$~')[1:]
	name = name.replace('tau1', r'$\tau^1$')
	return name


### Load datasets

full_dataset = dataset()
full_dataset.read_data("/home/ebopp/data/proc/real-full/scaling")
early_dataset = dataset()
early_dataset.read_data("/home/ebopp/data/proc/real-early/scaling")
pess_dataset = dataset()
pess_dataset.read_data("/home/ebopp/data/proc/real-pess-full/scaling")
pessearly_dataset = dataset()
pessearly_dataset.read_data("/home/ebopp/data/proc/real-pess-early/scaling")

colof = full_dataset.get_column
coloe = early_dataset.get_column
colpf = pess_dataset.get_column
colpe = pessearly_dataset.get_column



### Generate LaTeX-formatted table values

print 'Sorted LaTeX table of system results'
texres_prefilter = lambda x: abs(x['i0'] - pi/2) < .1
texres_postfilter = lambda line: line[4] >= acos(0.95)/pi*180.0

texres_name = colof('name', texres_prefilter)
texres_snrrv = colof('snrrv', texres_prefilter)
texres_period = colof('period', texres_prefilter)
texres_distat_of = map(lambda x: x/pi*180.0, colof('di_stat', texres_prefilter))
texres_ilim_of = map(lambda x: x/pi*180.0, colof('i_limit_low', texres_prefilter))
texres_distat_oe = map(lambda x: x/pi*180.0, coloe('di_stat', texres_prefilter))
texres_ilim_oe = map(lambda x: x/pi*180.0, coloe('i_limit_low', texres_prefilter))
texres_distat_pf = map(lambda x: x/pi*180.0, colpf('di_stat', texres_prefilter))
texres_ilim_pf = map(lambda x: x/pi*180.0, colpf('i_limit_low', texres_prefilter))
texres_distat_pe = map(lambda x: x/pi*180.0, colpe('di_stat', texres_prefilter))
texres_ilim_pe = map(lambda x: x/pi*180.0, colpe('i_limit_low', texres_prefilter))

texres_table = zip(texres_name, texres_snrrv, texres_period, texres_distat_of, texres_ilim_of, texres_distat_oe, texres_ilim_oe, texres_distat_pf, texres_ilim_pf, texres_distat_pe, texres_ilim_pe)
texres_table_filtered = filter(texres_postfilter, texres_table)
texres_table_filtered.sort(key=lambda line: line[3])

greek_letters = ['alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta', 'eta', 'theta', 'iota', 'kappa', 'lambda', 'mu', 'nu', 'xi', 'omicron', 'pi', 'rho', 'sigma', 'tau', 'upsilon', 'phi', 'chi', 'psi', 'omega']

texres_lines = []
for name, snrrv, period, distat_of, ilim_of, distat_oe, ilim_oe, distat_pf, ilim_pf, distat_pe, ilim_pe in texres_table_filtered:
	# Format name correctly
	name = formatName(name)
	texres_lines.append(r'%s & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ & $%.1f$ \\' % (name, snrrv, period, distat_of, ilim_of, distat_oe, ilim_oe, distat_pf, ilim_pf, distat_pe, ilim_pe) + '\n')
texres_str = ''.join(texres_lines)
texres_file = open('/home/ebopp/data/tex/real-res.tex', 'w')
texres_file.write(texres_str)
texres_file.close()
