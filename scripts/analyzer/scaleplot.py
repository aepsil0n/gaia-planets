### Modules

from dirs import *
from dataset import *
from defs import *
from plotter import *
from fitter import *

from itertools import *
from math import *
import operator as op
import scipy.stats
import numpy as np
import random


### Read database

main_dataset = dataset()
main_dataset.read_data(PROCDIR+"scaling")
main_dataset.add_filter('p-good', lambda x: good_period(x['period']))# and x['snr'] > 1.0
main_dataset.add_filter('signal', lambda x: x['di_stat'] < pi/6.0)
main_dataset.add_filter('plx-peak', lambda x: x['di_stat'] < pi/6.0 and abs(x['period']-YEAR) < 80.0)
# Column selector shortcut
col = main_dataset.get_column


### Load fit results

tempfile = open(PROCDIR+'splinefit')
spl_par, spl_dpar = [], []
for line in tempfile:
	vals = line.split()
	spl_par.append(float(vals[0]))
	spl_dpar.append(float(vals[1]))
	spl_csq = float(vals[2])
	spl_dof = int(vals[3])
spl_par_matrix = np.reshape(spl_par, (len(grid_log_snr), len(grid_inc)))
spl_dpar_matrix = np.reshape(spl_dpar, (len(grid_log_snr), len(grid_inc)))
spl_best = spline_ipl(grid_log_snr, grid_inc, spl_par)
main_dataset.add_column('spl_res', lambda dp: log10(dp['di_stat']) - spl_best(log10(dp['snra']), dp['i0'])[0][0])


### Plots

print 'LaTeX table of spline values'
tex_str = grid2tex(map(np.degrees, grid_inc), grid_log_snr, map(op.neg, spl_par_matrix), spl_dpar_matrix, r'Inclination $[^\circ]$', r'$\log_{10}\snra$')
tex_file = open(PLOTDIR+'spline.tex', 'w')
tex_file.write(tex_str)
tex_file.close()


# Initialise plotter
plt = plotter(PLOTDIR)


print 'Contours of spline model fit'

# Calculate finer grid
gX_fine = map(lambda x: 10**x, np.linspace(0.0, 4.0, 100))
gY_fine = np.linspace(10, 90, 81)
gZA_fine = np.array([[spl_best(log10(x),y/180.0*pi)[0][0] for y in gY_fine] for x in gX_fine])
gZB_fine = np.array([[spl_best(log10(x*sin(y/180.0*pi)),y/180.0*pi)[0][0] for y in gY_fine] for x in gX_fine])

# Definition of levels and labels
cdict = {log10(pi/2): r'$90^\circ$', log10(pi/6): r'$30^\circ$', log10(pi/9): r'$20^\circ$', log10(pi/18): r'$10^\circ$', log10(pi/60): r'$3^\circ$', log10(pi/180): r'$1^\circ$', log10(pi/180/3): r'$20^\prime$', log10(pi/180/12): r'$5^\prime$', log10(pi/180/60): r'$1^\prime$'}

# Plot contours on grid
optionsA = {'lvl': cdict, 'ylog': True, 'ylim': [2.5, 1e3], 'ylabel': r'$\mathrm{SNR}_A$', 'xlabel': r'$i \, \left[^\circ\right]$'}
optionsB = optionsA.copy()
optionsB['ylabel'] = r'$\mathrm{SNR}_B$'
plt.contour(gY_fine, gX_fine, gZA_fine, fname='spline-model-a', **optionsA)
plt.contour(gY_fine, gX_fine, gZB_fine, fname='spline-model-b', **optionsB)


print 'Inclination curves for SNR_RV'

# Calculate curves
icurve_snrrv = [1.0, 3.0, 10.0, 30.0, 100.0, 300.0, 1000.0]
icurve_inc = np.linspace(10, 90, 81)
icurve_values = [[spl_best(log10(snrrv/sin(i/180*pi)), i/180*pi)[0][0] for i in icurve_inc] for snrrv in icurve_snrrv]

# Plot
plt.init()
for snrrv, vals in zip(icurve_snrrv, icurve_values):
	plt.line(icurve_inc, vals, label=r'$\mathrm{SNR}_0 = %d$' %(snrrv), solo=True)
plt.finish(fname='inc-curves', yticks=cdict, xlabel=r'$i [^\circ]$', ylabel=r'$\sigma_i$', legend=True)


print 'Inclination curves for example planetary system'

# Calculate curves for example system
excurve_pmasses = [0.25, 1.0, 4.0, 13.0]
excurve_inc = icurve_inc
excurve_snr = lambda mp: (1e6*9.546e-4*mp/1.0*2.0/50.0) / (2.58*7.0/sqrt(80.0))
excurve_values = [[spl_best(log10(excurve_snr(mp)/sin(i/180*pi)), i/180*pi)[0][0] for i in excurve_inc] for mp in excurve_pmasses]
excurve_fmts = ['k-', 'r--', 'g-.', 'b:']

# Plot
plt.init()
for mp, vals, fmt in zip(excurve_pmasses, excurve_values, excurve_fmts):
	plt.line(excurve_inc, vals, fmt=fmt, label=r'$m_P \sin i = %.2f \, M_J$' %(mp), solo=True)
plt.finish(fname='inc-curves-ex', yticks=cdict, xlabel=r'$i [^\circ]$', ylabel=r'$\sigma_i$', legend=True, legend_loc=4)


print 'SNR_B curves for different inclinations'

# Calculate curves
snrcurve_snrb = map(lambda x: 10**x, np.linspace(0.0, 4.0, 100))
snrcurve_inc = [10.,40.,90.]
snrcurve_values = [[spl_best(log10(snrb*sin(i/180*pi)), i/180*pi)[0][0] for snrb in snrcurve_snrb] for i in snrcurve_inc]
snrcurve_colors = [('#800000', '#ff8080'), ('#0000a0', '#8080ff'), ('#000000', '#808080')]
snrcurve_markers = ['o', 's', '*']
snrcurve_fmts = ['-', '--', '-.']

# Plot
plt.init()
for i, vals, color, marker, fmt in zip(snrcurve_inc, snrcurve_values, snrcurve_colors, snrcurve_markers, snrcurve_fmts):
	filname = lambda x: abs(x['i0']-i/180.0*pi) < .01 and good_period(x['period'])
	x, y = list(col('snrb', filname)), map(log10, col('di_stat', filname))
	plt.scatter(x, y, color=color[1], marker=marker, size=6+10*(marker=='*'), label=r'Results $i = %d \, ^\circ$' % (i), solo=True)
	plt.line(snrcurve_snrb, vals, fmt=fmt, color=color[0], label=r'Spline $i = %d \, ^\circ$' % (i), solo=True)
plt.finish(fname='snr-curves', xlog=True, xlim=[0.8,1.2e3], ylim=[log10(pi/180/60), log10(pi/2)], yticks=cdict, xlabel=r'$\mathrm{SNR}_B$', ylabel=r'$\sigma_i$', legend=True, legend_loc=3)


print 'SNR_B residuals'

# Do statistics
snr_bin_edges = map(lambda x: 10**x, np.linspace(0.0, 4.0, 9))
snr_bin_ctrs = [(snr_bin_edges[k]+snr_bin_edges[k+1])/2.0 for k in range(len(snr_bin_edges)-1)]
snr_bin_sets = [list(col('spl_res', lambda dp: snr_bin_edges[k] < dp['snrb'] < snr_bin_edges[k+1] and abs(dp['spl_res']) < 0.6 and good_period(dp['period']))) for k in range(len(snr_bin_edges)-1)]
snr_bin_mean, snr_bin_std = [], []
for set in snr_bin_sets:
	mean, std, n = kappa_sigma_clip(set, 3.0)
	#print mean, std, n, len(set)
	snr_bin_mean.append(mean)
	snr_bin_std.append(std)

# Plot scatter and averaged values
plt.init()
plt.scatter(col('snrb', 'p-good'), col('spl_res', 'p-good'), solo=True)
plt.error(snr_bin_ctrs, snr_bin_mean, snr_bin_std, solo=True)
plt.finish(fname='res-snr', xlog=True, xlim=[1,3e4], xlabel=r'$\mathrm{SNR}_B$', ylabel=r'Log residuals')


print 'Residual histogram'
plt.hist(col('spl_res', lambda x: x['snrb'] > 30 and good_period(x['period'])), bins=20, fname='res-hist')

print 'Period residuals (big)'
plt.init()
plt.scatter(col('period', 'signal'), col('spl_res', 'signal'), solo=True)
plt.finish(fname='res-peri', xlog=True, xlabel=r'$P \, [\mathrm{d}]$', ylabel=r'Log residuals', vline=MISSION_TIME)

print 'Period residuals (zoomed at 1 yr)'

# Calculate running average of peak >> move to scalefit.py
data_period, data_res = map(list, [col('period', 'plx-peak'), col('spl_res', 'plx-peak')])
data_complete = zip(data_period, data_res)
grid_bin_width = 140
grid_bin_no = 8
grid_period = np.linspace(YEAR-grid_bin_width/2., YEAR+grid_bin_width/2., grid_bin_no)
grid_res_mean, grid_res_std = [], []
grid_span = 0.5*grid_bin_width/(grid_bin_no-1)
for P in grid_period:
	subdata = filter(lambda x: abs(x[0]-P) < grid_span, data_complete)
	sub_period = [x[1] for x in subdata]
	mean, std = np.mean(sub_period), np.std(sub_period, ddof=1)
	grid_res_mean.append(mean)
	grid_res_std.append(std)

# Fit Gaussian profile >> move to scalefit.py

gauss = lambda mu, sigma, x: 1/sigma/sqrt(2*pi) * exp(-((x-mu)/sigma)**2*0.5)
err_fct = lambda p, d: [(p[0]*gauss(YEAR, p[1], x[0]) - x[1])/x[2] for x in d]
peak_p, peak_dp, peak_csq, peak_dof = lsq_fit(zip(grid_period, grid_res_mean, grid_res_std), err_fct, [0.5, 13.0])
for p, dp in zip(peak_p, peak_dp):
	print '\t', p, dp
peak_bestfit = lambda x: peak_p[0]*gauss(YEAR, peak_p[1], x)
print '\tcsq = %.2f' % (peak_csq)
print '\tdof = %d' % (peak_dof)
print '\t2-sigma: %.2f -- %.2f' % (YEAR-2*peak_p[1], YEAR+2*peak_p[1])
print '\tmean degradation factor @ peak: %.2f +/- %.2f' % (10**peak_bestfit(YEAR), 10**peak_bestfit(YEAR)*peak_p[0]/peak_p[1]*log(10)/sqrt(2*pi)*sqrt((peak_dp[0]/peak_p[0])**2 + (peak_dp[1]/peak_dp[1])**2))
grid_period_fine = np.linspace(YEAR-grid_bin_width/2., YEAR+grid_bin_width/2., 100)
grid_gauss_fine = map(peak_bestfit, grid_period_fine)

# Plot it
plt.init()
plt.scatter(data_period, data_res, solo=True)
plt.error(grid_period, grid_res_mean, grid_res_std, fmt=None, solo=True)
plt.line(grid_period_fine, grid_gauss_fine, solo=True)
plt.finish(fname='res-peri-1yr', xlabel=r'$P \, [\mathrm{d}]$', ylabel=r'Log residuals', xlim=[YEAR-80, YEAR+80])

# Inclination limits

print 'Lower/upper limits on inclination'
filter_40 = lambda x: abs(x['i0']-2./9.*pi) < .01 and good_period(x['period'])
filter_90 = lambda x: abs(x['i0']-0.5*pi) < .01 and good_period(x['period'])

# Fit to 90 deg setup
limfit_snrrv = col('snrrv', filter_90)
limfit_snrrv_sorted = sorted(col('snrrv', filter_90))
limfit_ilimlow = col('i_limit_low', filter_90)
limfit_err_fct = lambda p, d: [atan(x[0]/p[0]) - x[1] for x in d]
limfit_p, limfit_dp, limfit_csq, limfit_dof = lsq_fit(zip(limfit_snrrv, limfit_ilimlow), limfit_err_fct, [2.0])
print '\t', limfit_p[0], limfit_dp[0]
print '\tcsq = %.2f' % (limfit_csq)
print '\tdof = %d' % (limfit_dof)

# Plot stuff
plt.init()
plt.scatter(map(log10, col('snrrv', filter_40)), map(lambda i: i/pi*180.0, col('i_limit_low', filter_40)), marker='v', color='#f0b040', size=12, label=r'Lower limit $i_\mathrm{input}=40^\circ$', solo=True)
plt.scatter(map(log10, col('snrrv', filter_40)), map(lambda i: i/pi*180.0, col('i_limit_high', filter_40)), marker='^', color='#f0b040', size=12, label=r'Upper limit $i_\mathrm{input}=40^\circ$', solo=True)
plt.scatter(map(log10, col('snrrv', filter_90)), map(lambda i: i/pi*180.0, col('i_limit_low', filter_90)), marker='v', color='#000000', size=12, label=r'Lower limit $i_\mathrm{input}=90^\circ$', solo=True)
plt.scatter(map(log10, col('snrrv', filter_90)), map(lambda i: i/pi*180.0, col('i_limit_high', filter_90)), marker='^', color='#000000', size=12, label=r'Upper limit $i_\mathrm{input}=90^\circ$', solo=True)
plt.line(map(log10, limfit_snrrv_sorted), map(lambda x: 180.0/pi*atan(x/limfit_p[0]), limfit_snrrv_sorted), label=r'$\mathrm{arctan} \left( \frac{\mathrm{SNR}_\mathrm{RV}}{\gamma} \right)$', solo=True)
plt.finish(fname='incl-limits', xlabel=r'$\log_{10} \mathrm{SNR}_\mathrm{RV}$', ylabel=r'$i \, [^\circ]$', xlim=[-2, 3], ylim=[0, 180])

# Mass limit
print 'Upper mass limit'
masslim_snrrv = np.linspace(-0.5, 2.0)
masslim_fct = lambda snrrv: 1.0/sin(atan(10**snrrv/limfit_p[0]))
masslim_rnd = 1.0/sin(acos(0.95))
#masslim_posterior = lambda snrrv: 1.0/sin(acos(0.95*cos(atan(10**snrrv/limfit_p[0]))))
plt.init()
plt.line(masslim_snrrv, map(masslim_fct, masslim_snrrv), solo=True)
#plt.line(masslim_snrrv, map(masslim_posterior, masslim_snrrv), solo=True)
plt.line([-0.5, 2.0], [masslim_rnd]*2, solo=True)
plt.finish(fname='mass-limit', xlabel=r'$\log_{10} \mathrm{SNR}_\mathrm{RV}$', ylabel=r'$m_{P,\mathrm{max}} / m_{P,\mathrm{proj}}$', ylim=[0,6])


print 'Inclination residuals'
plt.scatter(col('i0', 'p-good'), col('spl_res', 'p-good'), fname='res-inc', xlabel=r'$i_\mathrm{true}$', ylabel=r'Log residuals')

print 'Eccentricity residuals'
plt.scatter(col('ecc', 'p-good'), col('spl_res', 'p-good'), fname='res-ecc', xlim=[0,1], xlabel=r'$e$', ylabel=r'Log residuals')

print 'Number of transits residuals'
plt.scatter(col('ntr', 'p-good'), col('spl_res', 'p-good'), fname='res-ntr', xlog=True, xlabel=r'$N_\mathrm{tr}$', ylabel=r'Log residuals')

print 'plx residuals'
plt.scatter(col('sigplx', 'p-good'), col('spl_res', 'p-good'), fname='res-sigplx', ylim=[-.5,.5], xlog=True, xlabel=r'$\sigma_\pi [\mathrm{mas}]$', ylabel=r'Log residuals')
