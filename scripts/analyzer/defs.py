""" Python module comprising definitions of constants and derived quantities.
Includes definition of signal-to-noise ratio.
"""

import numpy as np
from math import *

""" Some natural and model constants. """

ALPHA_AL = 0.38077
ALPHA_AC = 0.07144
ALPHA_TOTAL = (ALPHA_AC**2 + ALPHA_AL**2)**0.5
YEAR = 365.25
MISSION_TIME = 5.0*YEAR
PEAK_WIDTH = 30.0

""" Some functions. """

def signal(astrosig, ecc, inc):
	""" Calculates modified astrometric signature in muas from edge-on astrosig, eccentricity and inclination. """
	return astrosig/(1+ecc**4)/sin(inc)

def noise(plxerr, ntr, ac):
	""" Calculates total noise scale in muas from parallax error and number of transits."""
	if ac == 1:
		single_transit_error = plxerr/ALPHA_TOTAL
	else:
		single_transit_error = plxerr/ALPHA_AL
	return 1e3*single_transit_error/sqrt(ntr)

def signal_to_noise(astrosig, plxerr, ntr, ecc, inc):
	""" Calculates signal-to-noise ratio based on some parameters. """
	return signal(astrosig, ecc, inc)/noise(plxerr, ntr)

def good_period(period):
	""" Returns True iff period is well behaved, i.e. smaller mission time and not around one year. """
	return period<MISSION_TIME and abs(period-YEAR) > PEAK_WIDTH

def kappa_sigma_clip(cliplist, kappa, maxit=30):
	""" Calculates mean and standard deviation filtering the input list by means of kappa-sigma-clipping. """
	newlist = cliplist
	nit = 0
	while True:
		filmean, filstd = np.mean(newlist), np.std(newlist)
		low = filmean - kappa*filstd
		high = filmean + kappa*filstd
		oldlist = newlist
		newlist = filter(lambda x: low < x < high, oldlist)
#		print low, high, len(newlist), len(oldlist)
		nit += 1
		if len(newlist) == len(oldlist) or nit >= maxit:
			return filmean, filstd, len(newlist)

""" Test cases. """

if __name__ == '__main__':

	# Kappa-sigma-clipping
	import random
	cliplist = [random.gauss(0,1) for x in xrange(100)]+[random.gauss(0,4) for x in xrange(20)]
	print np.mean(cliplist), np.std(cliplist)
	print kappa_sigma_clip(cliplist, 3.0)
