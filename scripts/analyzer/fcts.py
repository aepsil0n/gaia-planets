### Functions

import scipy.optimize
import scipy.interpolate
import numpy as np
from math import *


# General fitting method
def fitstuff(fdata, ffct, fguess):
	par, cov, dictinfo, msg, ier = scipy.optimize.leastsq(ffct, fguess, args=(fdata), full_output=True)
	if len(fguess) == 1:
		par = [par]
	chisq = sum([x*x for x in ffct(par, fdata)])
	dof = len(fdata) - len(fguess)
	dpar = [sqrt(abs(cov[i][i])) for i in range(len(par))]
	return [par, dpar, chisq, dof]


# Spline interpolator wrapper asking for grid and values at points
def spline_ipl(gX, gY, gZ):
	gX.sort()
	gY.sort()
	nX = len(gX)
	nY = len(gY)
#	print 'Evaluate for ', gZ
	return scipy.interpolate.RectBivariateSpline(gX, gY, gZ.reshape(nX, nY))


# Fits a spline to the 2d data
# X, Y, Z, DZ -- data
# gX, gY -- spline grid
def splinefit_2d(X, Y, Z, DZ, gX, gY):
	# TODO: check dims
	# raise ValueError
	def err_fct(par, data):
		spline = spline_ipl(gX, gY, par)
		return [(spline(d[0], d[1])[0][0] - d[2])/d[3] for d in data]
	data = zip(X, Y, Z, DZ)
	par, dpar, chisq, dof = fitstuff(data, err_fct, [0.0]*len(gX)*len(gY))
	return par, dpar, chisq, dof


# Bins data on unevenly placed 2D points in grid
def avg2dbin(X, Y, Z, XS, YS):
	XS.sort()
	YS.sort()
	NX = len(XS)
	NY = len(YS)
	WX = float(XS[-1] - XS[0])
	WY = float(YS[-1] - YS[0])
	Xg, Yg = XS, YS
	Zg = [[[] for x in Xg] for y in Yg]
	for x,y,z in zip(X,Y,Z):
		fieldX = None
		k = 0
		while fieldX == None and k < NX:
			if k == 0:
				if Xg[k] < x < (Xg[k]+Xg[k+1])/2.0:
					fieldX = k
			elif k == NX-1:
				if (Xg[k-1]+Xg[k])/2.0 < x < Xg[k]:
					fieldX = k
			elif (Xg[k-1]+Xg[k])/2.0 < x < (Xg[k]+Xg[k+1])/2.0:
				fieldX = k
			k += 1
		fieldY = None
		k = 0
		while fieldY == None and k < NY:
			if k == 0:
				if Yg[k] < y < (Yg[k]+Yg[k+1])/2.0:
					fieldY = k
			elif k == NY-1:
				if (Yg[k-1]+Yg[k])/2.0 < y < Yg[k]:
					fieldY = k
			elif (Yg[k-1]+Yg[k])/2.0 < y < (Yg[k]+Yg[k+1])/2.0:
				fieldY = k
			k += 1
		if not (fieldX == None or fieldY == None):
			#print fieldX, fieldY, x, y, z
			Zg[fieldY][fieldX].append(z)
	for i in range(len(Zg)):
		for j in range(len(Zg[i])):
			if len(Zg[i][j]) == 0:
				Zg[i][j] = None
			else:
				Zg[i][j] = sum(Zg[i][j])/len(Zg[i][j])
			#print i, j, Zg[i][j]
	return Xg, Yg, Zg
