""" Exobtain -- miner.py

Contains miner class.
"""

import os
import os.path
import re
import commands
from datetime import datetime

from objects import System, Planet, Star
from generator import magBVToMagVI, sigpar

class Miner:
	""" Obtains data from some online ressource. """

	# Path of exoplanet database mirror
	
	def __init__(self):
		""" Default constructor. """
		self.pathExoEU = "data/exoplanet.eu.csv"
		self.pathExoORG = "data/exoplanets.org.csv"
		self.pathSimbad = "data/simbad.csv"
		"""
		updateExoEU = False
		if not os.path.exists(self.pathExoEU):
			updateExoEU = True
			print "No copy of exoplanet.eu database found."
		else:
			stat = os.stat(self.pathExoEU)
			fileage = datetime.fromtimestamp(stat.st_mtime)
			now = datetime.now()
			delta = now - fileage
			secs = (delta.microseconds + (delta.seconds + delta.days * 24 * 3600) * 1e6) / 1e6
			if secs > 7*86400:	# older than one week
				updateExoEU = True
				print "Copy of exoplanet.eu database older than one week."
		if updateExoEU:
			print "Updating exoplanet database..."
			commands.getoutput('wget -O '+self.pathExoEU+' "http://www.exoplanet.eu/export.php?all=yes&outputType=csv"')
		else:
			print "Exoplanet database is up-to-date."
		"""

	def getRVSystems(self):
		""" Obtains all RV systems from the exoplanet database. """
		
		# Create return list
		systems = []
		
		# Parse SIMBAD query file
		simbad_file = open(self.pathSimbad)
		simbad_entries = [map(lambda x: x.strip('" \n'), line.split(";")) for line in simbad_file]
		simbad_file.close()
		
		# Parse each line as a planet
		dbFile = open(self.pathExoORG)
		for line in dbFile:
			
			# Create array from the line and strip quotation marks
			params = map(lambda x: x.strip('" \n'), line.split(","))
			
			# Parse planet and system names
			plname = params[0]
			sysname = params[7]
			
			# Create new system with star if it doesn't already exist
			isNew = True
			sys = None
			for x in systems:
				if x.name == sysname:
					sys = x
					isNew = False
			if isNew:
				sys = System(sysname)
				# Parse star data
				sys.star.mass = float(params[8])
				sys.star.magV = float(params[9])
				sys.star.magBV = float(params[12])
				if not params[13] == "":
					sys.star.feh = float(params[13])
				sys.star.alpha = float(params[14])
				sys.star.delta = float(params[15])
				if not params[17] == "":
					sys.star.parallax = float(params[17])
				simbad_entry = None
				for entry in simbad_entries:
					if entry[1] == sysname:
						simbad_entry = entry
				if simbad_entry == None:
					print "No Simbad entry found for "+sysname
				else:
					mu = simbad_entry[4].split()
					if not "~" in mu[0]:
						sys.star.muAlpha = float(mu[0])
					if not "~" in mu[1]:
						sys.star.muDelta = float(mu[1])
					if not "~" in simbad_entry[6]:
						sys.star.RV = float(simbad_entry[6])
					sys.star.sptype = simbad_entry[10]
				
			# Parse planet data
			pl = Planet(plname, sys.star)
			pl.msini = float(params[1])
			pl.period = float(params[3])
			pl.ecc = float(params[4])
			if params[5] != '':
				pl.argperi = float(params[5])
			pl.timeperi = float(params[6])
			sys.addPlanet(pl)
			
			# Append system to return list
			if isNew:
				systems.append(sys)
		
		# Close file
		dbFile.close()
		
		# Return the list of systems
		return systems#filter(lambda sys: len(sys.planets) == 1 and sys.planets[0].astrosini >= sigpar(sys.star.magV, magBVToMagVI(sys.star.magBV)) and 1.5 <= sys.star.magV <= 25.0, systems)
		

if __name__ == "__main__":
	m = Miner()
	sys = m.getRVSystems()
