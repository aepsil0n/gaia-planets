"""
Generator module.
"""

# Imports

from __future__ import division
from random import uniform, gauss, choice
from math import cos, pi, log10, sqrt, asin

from objects import System, Planet, Star
from imf import sampleCanonicalIMFStars


# Methods

def rejsamp(pdf, M, domain):
	""" Rejection sampling algorithm. """
	accepted = False
	draw = 0
	while not accepted:
		draw = uniform(float(domain[0]), float(domain[1]))
		check = uniform(0.0, 1.0)
		accepted = check < pdf(draw)/float(M)
	return draw


def masslum(mass):
	""" Converts mass to luminosity for a main sequence star. """
	if mass < 0.43:
		return 0.23 * mass ** 2.3
	elif mass < 2.0:
		return mass ** 4.0
	elif mass < 20.0:
		return 1.5 * mass ** 3.5
	else:
		return 1.5 * 20.0**2.5 * mass

def magVtoBV(magv):
	""" Converts magV to magBV for a main sequence star. """
	c0 = -0.090 # +/- 0.018
	c1 = 0.117 # +/-0.003
	c2 = 0.011 # +/-0.001
	c3 = -0.001 # +/-0.000
	return c0 + magv*(c1 + magv*(c2 + magv*c3))

def magBVtoBC(bv):
	b0 = -0.258 # +/-0.050
	b1 = -0.416 # +/-0.246
	b2 = 1.059 # +/-0.347
	b3 = -1.114 # +/-0.866
	b4 = -4.969 # +/-0.387
	b5 = 3.212 # +/-0.712
	return b0 + bv*(b1 + bv*(b2 + bv*(b3 + bv*(b4 + bv*b5))))

def magBVToMagVI(bv):
	a0 = 0.830 # +/-0.011
	a1 = 1.101 # +/-0.033
	a2 = -0.167 # +/-0.077
	a3 = 0.782 # +/-0.045
	a4 = 0.674 # +/-0.082
	return a0 + bv*(a1 + bv*(a2 + bv*(a3 + bv*a4)))

def magBolToMagV(mb):
	a3 = 0.481281   # +/- 0.04142      (8.606%)
	b3 = 0.748227   # +/- 0.0113       (1.51%)
	c3 = 0.0329034  # +/- 0.000901     (2.738%)
	d3 = 0.00201497 # +/- 0.0001673    (8.301%)
	return a3 + mb*(b3 + mb*(c3 + mb*d3))

def sigpar(magv, magvi):
	z = 10.0**(0.4*(max(magv, 12.0)-15.0))
	return 1e-6*sqrt(9.3 + z*(658.1 + z*4.568))*(0.986 + (1.0 - 0.986)*magvi)

def generate(N = 500, sig_threshold = 7.0e-6):
	"""
	Generates a bunch of systems.  The systems are randomly distributed
	according to some specified distributions of parameters.
	
	>>> systems = generate(10)
	>>> len(systems)
	10

	"""

	systems = []
	
	while len(systems) < N:
		
		i = len(systems)
		
		# Create system
		system = System("rnd_%03.d" % (i))

		# Draw stellar parameters
		star = system.star
		star.mass = sampleCanonicalIMFStars()
		star.sptype = "M3V"
		star.feh = 0.0122
		
		# Uniform distribution on the sky
		star.alpha = uniform(0,24)
		star.delta = asin(uniform(-1, 1))/pi*180.0 # cos distribution
		# Distances from 0 to 100 pc
		# according to d^2 distribution => homogenous spatial distribution
		MAXDISTPC = 1000.0
		star.parallax = 1e3/(uniform(0,1)**(1./1.) * MAXDISTPC) # r^2 distribution
		
		# Solar neighbourhood velocity dispersion, see Aumer & Binney (2009)
		SIGMAV = 15.0
		KMS2PCYR = 365.25*86400.0/3.0857e13
		star.muAlpha = KMS2PCYR * star.parallax * gauss(0, SIGMAV)
		star.muDelta = KMS2PCYR * star.parallax * gauss(0, SIGMAV)
		star.RV = gauss(0, SIGMAV)
		
		# Calculate magnitude and color
		lum = masslum(star.mass)
		magbol = 4.75 - 2.5 * log10(lum)
		absmagV = magBolToMagV(magbol)
		star.magV = absmagV + 5.0 * (log10(1e3/star.parallax) - 1.0)
		star.magBV = magVtoBV(absmagV)
		
		# Create a planet
		planet = Planet(system.name + " b")
		system.addPlanet(planet)
		
		# Draw planetary parameters
		planet.msini = uniform(0, 13.0)
		planet.period = choice([10**uniform(1, 3.5)]*4 + [gauss(365.25, 40)])#10**uniform(1, log10(10*365.25))
		planet.ecc = rejsamp(lambda e: 1/(1+e)**4 - e/16, 1, [0,1]) # Shen & Turner (2008); Hogg et al. (2010)
		planet.timeperi = uniform(0, planet.period)
		planet.argperi = uniform(0, 2*pi)
		
		# Check if astrometric signature is sufficient
		if planet.astrosini >= sigpar(star.magV, magBVToMagVI(star.magBV)) and 1.5 <= star.magV <= 25.0:
			systems.append(system)
			print i
	
	return systems

