"""
System module

Contains planet, star and system class.
"""

# Mass of Jupiter in solar masses

MJUP = 0.0009546


# Classes

class Planet(object):
	""" Contains information on a planet. """
	
	# Methods
	
	def __init__(self, name = None, star = None):
		"""
		Constructor.
		
		Arguments:
		star   -- The planet's host star.
		planet -- The planet's name.
		
		>>> p = Planet("test planet")
		>>> p.star is None
		True
		>>> p.name
		'test planet'
		>>> p.msini
		1.0
		>>> p.period
		100.0
		>>> p.ecc
		0.0
		>>> p.timeperi
		2450000.0
		>>> p.argperi
		0.0
		"""
		self.star = star
		self.name = name
		self.msini = 1.0
		self.msini_err = 0.0
		self.period = 100.0
		self.period_err = 0.0
		self.ecc = 0.0
		self.ecc_err = 0.0
		self.timeperi = 2450000.0
		self.timeperi_err = 0.0
		self.argperi = 0.0
		self.argperi_err = 0.0
	
	def __str__(self):
		""" String representation of the planet.
		>>> p = Planet("test_331")
		>>> print p
		P, test_331, 1.0, 0.0, 100.0, 0.0, 0.0, 0.0, 2450000.0, 0.0, 0.0, 0.0
		"""
		stuff = ["P", self.name, self.msini, self.msini_err, self.period, self.period_err, self.ecc, self.ecc_err, self.timeperi, self.timeperi_err, self.argperi, self.argperi_err, self.astrosini]
		s = ""
		for i, thing in enumerate(stuff):
			s += str(thing)
			if i < len(stuff) - 1:
				s += ", "
		return s
		
	@property
	def astrosini(self):
		return self.msini * MJUP * self.star.parallax * 1e-3 * (self.period / 365.25 / self.star.mass) ** (2./3.)


class Star(object):
	""" Contains information on a star. """
	
	# Methods
	
	def __init__(self, name = ""):
		""" Basic constructor.
		
		>>> s = Star("test_123")
		>>> s.name
		'test_123'
		>>> s.mass
		1.0
		>>> s.sptype
		'G2V'
		>>> s.feh
		0.0
		>>> s.alpha, s.delta
		(0.0, 0.0)
		>>> s.muAlpha, s.muDelta
		(0.0, 0.0)
		>>> s.parallax
		100.0
		>>> s.RV
		0.0
		>>> s.magV
		5.0
		>>> s.magBV
		0.0
		"""
		self.name = name
		self.mass = 1.0
		self.sptype = "G2V"
		self.feh = 0.0
		self.alpha = 0.0
		self.delta = 0.0
		self.muAlpha = 0.0
		self.muDelta = 0.0
		self.parallax = 100.0
		self.RV = 0.0
		self.magV = 5.0
		self.magBV = 0.0
		
	def __str__(self):
		""" String representation of star.
		
		>>> s = Star("test_123")
		>>> print s
		S, test_123, 1.0, G2V, 0.0, 0.0, 0.0, 0.0, 0.0, 100.0, 0.0, 5.0, 0.0
		"""
		stuff = ["S", self.name, self.mass, self.sptype, self.feh, self.alpha, self.delta, self.muAlpha, self.muDelta, self.parallax, self.RV, self.magV, self.magBV]
		s = ""
		for i, thing in enumerate(stuff):
			s += str(thing)
			if i < len(stuff) - 1:
				s += ", "
		return s


class System(object):
	"""
	An exoplanetary system.
	"""
	
	# Methods
	
	def __init__(self, name = "", star = None):
		"""
		Constructor.
		
		Arguments:
		name -- The name of the system.
		
		>>> sys = System("Alpha Probarum")
		>>> sys.name
		'Alpha Probarum'
		>>> sys.planets
		[]
		>>> sys.star.name
		'Alpha Probarum'
		"""
		self.name = name
		self.__planets = []
		if star is not None:
			self.star = star
		else:
			self.star = Star(name)
	
	
	def addPlanet(self, planet):
		"""
		Adds a planet. In case it has no unique name, a generic name is chosen.
		
		Arguments:
		planet -- The planet to be added.
		
		>>> sys = System("Alpha Probarum")
		>>> p = Planet()
		>>> sys.addPlanet(p)
		>>> p in sys.planets
		True
		>>> p.star is sys.star
		True
		"""
		if planet not in self.planets:
			self.planets.append(planet)
			planet.star = self.star
	
	def __str__(self):
		""" String representation of the system.
		
		>>> sys = System("Alpha Probarum")
		>>> sys.addPlanet(Planet("Alpha Probarum b"))
		>>> print sys
		S, Alpha Probarum, 1.0, G2V, 0.0, 0.0, 0.0, 0.0, 0.0, 100.0, 0.0, 5.0, 0.0
		P, Alpha Probarum b, 1.0, 0.0, 100.0, 0.0, 0.0, 0.0, 2450000.0, 0.0, 0.0, 0.0
		"""
		s = str(self.star)
		for planet in self.planets:
			s += "\n" + str(planet)
		return s
	
	# Properties
	@property
	def planets(self):
		""" The list of planets in the system.
		
		>>> sys = System()
		>>> sys.planets
		[]
		>>> sys.planets = []
		Traceback (most recent call last):
			...
		AttributeError: can't set attribute
		"""
		return self.__planets

