'''
This script is meant to generate a sample of single-planet systems only
differing in position on the sky and orbital period.

All systems are generated with sample planets each with randomized nuisance
parameters periastron epoch, periastron argument and longitude of the
ascending node.

Thereby the effect of these parameters can be investigated.

'''


# Imports

from __future__ import division
from random import uniform, gauss, choice

from objects import System, Planet, Star


# Generator

def generic_system(name, period, alpha, delta):
    '''
    Creates a system mostly equivalent to Sun-Jupiter system without any of the
    other planets of the solar system.
    
    The eccentricity is set to 0.2 to showcase its impact.  Distance is 5 pc to
    make the planet well detectable with Gaia.

    * name   -- name of the system
    * period -- orbital period
    * alpha  -- right ascension [hrs]
    * delta  -- declination [deg]

    '''

    # Create system
    system = System(name)

    # Create star
    star = system.star
    star.mass = 1.0
    star.sptype = 'G2V'
    star.feh = 0.0122
    star.alpha = alpha
    star.delta = delta
    distance_in_pc = 10.0
    star.parallax = 1e3/(distance_in_pc)
    star.muAlpha = 0.0
    star.muDelta = 0.0
    star.RV = 0.0

    # Create a planet
    planet = Planet("%s b" % (name))
    system.addPlanet(planet)
    planet.msini = 1.0
    planet.period = period
    planet.ecc = 0.2
    planet.timeperi = 0.0
    planet.argperi = 0.0

    # Return the system
    return system


def generate_systems():
    '''
    Generates a couple of generic systems of the type created using
    generic_system(...). Uses a couple of different orbital periods and
    positions on the sky.

    Three different positions at equator, 45 deg north and 45 deg south are
    used.  Furthermore, each position is generated with a planet in a
    300-days-orbit and a 1000-days-orbit.

    '''
    
    # Empty list
    systems = []
    
    # Add systems
    systems += generic_system('equator_short', 300.0, 0.0, 0.0),
    systems += generic_system('equator_long', 1000.0, 0.0, 0.0),
    systems += generic_system('north_short', 300.0, 0.0, 45.0),
    systems += generic_system('north_long', 1000.0, 0.0, 45.0),
    systems += generic_system('south_short', 300.0, 12.0, -45.0),
    systems += generic_system('south_long', 1000.0, 12.0, -45.0),

    # Return them
    return systems


if __name__ == '__main__':
    # Generate systems
    systems = generate_systems()

    # Output to file
    with open('nuisance_sample', 'w') as pfile:
	for system in systems:
	    pfile.write('%s\n' % (system))

