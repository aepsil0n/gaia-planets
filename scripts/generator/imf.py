"""
Initial mass function (IMF)
"""

from random import uniform

canonicalIMFNorm = 7.4736079061169045

def canonicalIMFStars(mass):
	""" Gives the normalized canonical stellar IMF at a given mass in solar masses.
	
	>>> canonicalIMFStars(0.06)
	0.0
	>>> canonicalIMFStars(151.0)
	0.0
	"""
	if 0.07 < mass <= 0.5:
		return canonicalIMFNorm*(mass/0.07)**(-1.3)
	elif 0.5 < mass <= 150.0:
		return canonicalIMFNorm*(0.5/0.07)**(-1.3) * (mass/0.5)**(-2.3)
	else:
		return 0.0
		
def cumulativeCanonicalIMFStars(mass):
	""" Gives the cumulative normalized canonical stellar IMF at a given mass in solar masses.
	"""
	if 0.07 < mass <= 0.5:
		return canonicalIMFNorm*(7./30. + mass**-.3 * (-0.10507664489940477))
	elif 0.5 < mass <= 150.0:
		return canonicalIMFNorm*(0.13382216076338738 + mass**-1.3 * (-0.012124228257623632))
	elif mass < 0.07:
		return 0.0
	elif mass > 150.0:
		return 1.0

def sampleCanonicalIMFStars():
	""" Samples a mass from the canonical stellar IMF.
	"""
	u = uniform(0,1)
	if u < 0.7770221128751856:
		A, B, C = -0.10507664489940477, -0.3, 7./30.
	else:
		A, B, C = -0.012124228257623632, -1.3, 0.13382216076338738
	A *= canonicalIMFNorm
	C *= canonicalIMFNorm
	return ((u-C)/A)**(1./B)

if __name__ == "__main__":
	import matplotlib.pyplot as plt
	import numpy as np
	
	sample = []
	N = 1000
	for i in xrange(N):
		sample += sampleCanonicalIMFStars(),
	plt.hist(sample)
	masses = np.linspace(0.07, 150.0)
	plt.plot(masses, map(lambda m: N*canonicalIMFStars(m), masses))
	plt.show()

