from math import log10
import numpy as np
from imf import canonicalIMFStars as xi
from imf import cumulativeCanonicalIMFStars as Xi


with open("imf", "w") as pfile:
	for m in map(lambda x: 10**x, np.linspace(-1.5, 2.5)):
		pfile.write("%g %g %g\n" % (m, xi(m), Xi(m)))

