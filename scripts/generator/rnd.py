#! /usr/bin/python
"""
This modules generates a set of random systems.
"""

# Imports

import matplotlib.pyplot as plt
from matplotlib import rc
from optparse import OptionParser
from math import log10

from generator import generate, sigpar, magBVToMagVI
from miner import Miner


# Main routine

if __name__ == "__main__":
	
	# Parse command line options
	parser = OptionParser()
	parser.add_option("-N", "--number", dest="number", help="Number of systems to be generated", default=500, metavar="NSYS")
	parser.add_option("-b", "--histbins", dest="hbins", help="Number of bins in the output histogram", default=10, metavar="BINS")
	parser.add_option("-c", "--catalogue", action="store_true", dest="catalogue", help="Use catalogue instead of random systems", default=False)
	options, args = parser.parse_args()
	
	N = int(options.number)
	HBINS = int(options.hbins)
	
	try:
		filename = args[0]
	except IndexError:
		parser.error("An output filename must be supplied")
	
	
	# Generate or load systems
	if options.catalogue:
		m = Miner()
		systems = m.getRVSystems()
	else:
		systems = generate(N)
	
	# Make plots
	rc("text", usetex=True)
	rc("font", family="serif")

	# Plot stellar parameter diagrams
	star_diags = [
		(r"Visual magnitude (mag)", lambda sys: sys.star.magV),
		(r"log S/N", lambda sys: log10(sys.planets[0].astrosini/sigpar(sys.star.magV, magBVToMagVI(sys.star.magBV)))),
		(r"log Distance d [pc]", lambda sys: log10(1e3/sys.star.parallax)),
		(r"Stellar mass $\log_{10} m/M_{\textrm{sun}}$", lambda sys: log10(sys.star.mass)),
		(r"Right ascension", lambda sys: sys.star.alpha),
		(r"Declination", lambda sys: sys.star.delta),
		(r"p.m. ras", lambda sys: sys.star.muAlpha),
		(r"p.m. dec", lambda sys: sys.star.muDelta),
		(r"B-V color index", lambda sys: sys.star.magBV),
	]

	fig1 = plt.figure(1, figsize=(12,9))
	for i, stuff in enumerate(star_diags):
		name, expr = stuff
		ax = fig1.add_subplot(3, 3, i+1)
		plt.hist(map(expr, systems), bins = HBINS)
		plt.xlabel(name)
		plt.ylabel("Number of stars in bin")
	plt.savefig("testst.pdf")
	
	# Plot planetary parameter diagrams
	planet_diags = [
		(r"Projected planet mass (MJup)", lambda sys: sys.planets[0].msini),
		(r"Orbital period (yrs)", lambda sys: sys.planets[0].period/365.25),
		(r"Eccentricity", lambda sys: sys.planets[0].ecc),
	]

	fig2 = plt.figure(2, figsize=(9,12))
	for i, stuff in enumerate(planet_diags):
		name, expr = stuff
		ax = fig2.add_subplot(3, 1, i+1)
		plt.hist(map(expr, systems), bins = HBINS)
		plt.xlabel(name)
		plt.ylabel("Number of stars in bin")
	plt.savefig("testpl.pdf")

	# Write to file
	try:
		pfile = open(filename, "w")
		for system in systems:
			pfile.write(str(system) + "\n")
	finally:
		pfile.close()

