'''
Experimental model for inclination precision

Note: seems to be outdated

'''

# Imports

from __future__ import division
from numpy import linspace, inf
from scipy.integrate import quad
from matplotlib.pyplot import plot, show, legend
from math import sin, asin, pi, log10, exp, sqrt


# Script
def tern(cond, a, b):
	if cond:
		return a
	else:
		return b

def estim_step(i0, snr0):
	dsnr = 3**.5
	snr_rv = snr0*sin(i0)
	snr_low = max(snr0 - dsnr, snr_rv)
	snr_up = snr0 + dsnr
	snr_norm = snr_up - snr_low
	i_mean = quad(lambda th: asin(snr_rv/th), snr_low, snr_up)[0] / snr_norm
	i_var = quad(lambda th: (asin(snr_rv/th) - i_mean)**2, snr_low, snr_up)[0] / snr_norm
	i_std = i_var ** 0.5
	return i_mean, i_std

def estim_normal(i0, snr0):
	dsnr = 1.0
	snr_rv = snr0*sin(i0)
	pgauss = lambda th: exp(-0.5*((th - snr0)/dsnr)**2)*snr_rv/th# * tern(th >= snr_rv, snr_rv/th, 0.0)
	snr_low = max(snr0 - 5.0*dsnr, snr_rv)
	snr_up = snr0 + 5.0*dsnr
	snr_norm = 1.0 * quad(pgauss, snr_low, snr_up)[0]
	i_mean = quad(lambda th: asin(snr_rv/th)*pgauss(th), snr_low, snr_up)[0] / snr_norm
	i_var = quad(lambda th: (asin(snr_rv/th) - i_mean)**2*pgauss(th), snr_low, snr_up)[0] / snr_norm
	i_std = i_var ** 0.5
	return i_mean, i_std

for i0 in [10, 40, 80, 90]:
	arr_snrb = []
	arr_i_mean = []
	arr_i_std = []
	print i0, i0/180*pi, sin(i0/180*pi)
	for snrb in map(lambda x:10**x, linspace(-1, 4, 200)):
		i_mean, i_std = estim_normal(i0/180*pi, snrb*sin(i0/180*pi))
		arr_snrb.append(snrb)
		arr_i_mean.append(i_mean)
		arr_i_std.append(i_std)
	plot(map(lambda snrb: log10(snrb), arr_snrb), map(lambda di: log10(di), arr_i_std), label=str(i0))
legend(loc=3)
show()
