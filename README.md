This is a simulation software (Java) and corresponding analysis scripts
(Python) that deals with synthetic data generation to assess Gaia performance
for planet characterization.

The source code is mainly maintained here for reference and potential further
development. Note, that you need the proprietary GaiaSimu library by the Gaia
DPAC to use this.

A note of caution: this entire project requires refactoring and rewriting in
many parts. Removing the dependency on GaiaSimu is also something that would
eventually be useful. Furthermore, the multi-threading implementation is not
particularly useful at this point.
