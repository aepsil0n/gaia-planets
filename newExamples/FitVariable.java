package newExamples;

public class FitVariable {

	/** Whether the parameter is fixed or variable */
	private boolean fixed;
	
	/** The default value to be used in fitting */
	private double defaultValue;
	
	/** The size of the differential increment used for derivatives w.r.t. this variable */
	private double diffH;
	

	/**
	 * Basic constructor.
	 * 
	 * @param fixed			Whether the parameter is fixed or variable
	 * @param defaultValue	The default value to be used in fitting
	 * @param diffH			The size of the differential increment used for derivatives w.r.t. this variable
	 */
	public FitVariable ( boolean fixed, double defaultValue, double diffH ) {
		
		// Set values
		this.setFixed ( fixed );
		this.setDefaultValue ( defaultValue );
		this.setDiffH ( diffH );
		
	}

	
	// Setters & getters
	// TODO: documentation

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}


	public boolean isFixed() {
		return fixed;
	}


	public void setDefaultValue(double defaultValue) {
		this.defaultValue = defaultValue;
	}


	public double getDefaultValue() {
		return defaultValue;
	}


	public void setDiffH(double diffH) {
		this.diffH = diffH;
	}


	public double getDiffH() {
		return diffH;
	}
	
}
