package newExamples;

public class CmdLineArgs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Read command line arguments
		if(args.length < 7) {
			System.out.println("Usage: <jarfile> SOURCEFILE BASEPATH NFITS NINCLS NGAIAS FAINTCUTOFF SUBCATALOGUE");
			return;
		}
		
		String bpath = args[1];
		String srcfile = args[0];
		int NFITS = Integer.parseInt(args[2]);
		int NINCLS = Integer.parseInt(args[3]);
		int NGAIAS = Integer.parseInt(args[4]);
		boolean faintCutOff = Boolean.parseBoolean(args[5]);
		double subCatalogue = Double.parseDouble(args[6]);
		
		// Set up simulation
		Simulation sim = new Simulation(NGAIAS);
		sim.setBasepath(bpath);
		sim.setRandGeo(true);
		sim.setFaintCutOff(faintCutOff);
		sim.setSubCatalogue(subCatalogue);
		
		// Load systems
		sim.loadSystemsFromFile(srcfile);
		
		// Inclination angles
		final double PI2 = Math.PI/2;
		final double START = PI2/9.0;
		final double END = PI2;
		final double[] INCS = new double[NINCLS];
		for(int i = 0; i < INCS.length; i++)
			INCS[i] = (START+(END-START)*(double)i/(double)(NINCLS-1));
		
		// Fit systems
		sim.fitSystems(NFITS, INCS);
		
	}

}
