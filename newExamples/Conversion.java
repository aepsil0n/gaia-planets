package newExamples;

import gaia.cu1.params.GaiaParam;
import gaia.cu1.tools.numeric.algebra.GVector2d;
import gaia.cu1.tools.numeric.algebra.GVector3d;

import java.lang.Math;

/**
 * Wrapper class for several conversion functions used throughout the code 
 */
public class Conversion {
	
	
	/**
	 * Converts B-V color index to V-I colour index assuming main sequence star
	 *  
	 * @param colorBV B-V color index
	 * @return V-I color index
	 */
	public static double colorBVtoVI(double colorBV) {
		
		// Set up polynomial coefficients from fit to reference data
		double a1 =  0.829548834503867;
		double b1 =  1.10061523472994;
		double c1 = -0.167247455595159;
		double d1 =  0.782382685328936;
		double e1 =  0.673657917425654;
		
		// Calculate V-I color index
		double colorVI = a1 + b1*(colorBV-.6) + c1*Math.pow(colorBV-.6, 2) + d1*Math.pow(colorBV-.6, 3) + e1*Math.pow(colorBV-.6, 4);
		return colorVI;
		
	}
	
	
	/**
	 * Calculate bolometric correction from B-V color index given a main sequence star
	 * 
	 * @param colorBV B-V color index
	 * @return Bolometric correction
	 */
	public static double colorBVtoBC(double colorBV) {
		
		// Set up polynomial coefficients from fit to reference data
		
		double a2 = -0.257533349330975;
		double b2 = -0.415565213946619;
		double c2 =  1.05876673739721;
		double d2 = -1.11390570543119;
		double e2 = -4.96887518965728;
		double f2 =  3.21220165590925;
		
		// Calculate bolometric magnitude
		
		double magBol = a2 + b2*(colorBV-.6) + c2*Math.pow(colorBV-.6, 2) + d2*Math.pow(colorBV-.6, 3) + e2*Math.pow(colorBV-.6, 4) + f2*Math.pow(colorBV-.6, 5);
		return magBol;
		
	}
	
	
	/**
	 * Calculates Gaia magnitude from Johnson-Cousins B and V passbands using simulated polynomials
	 * source: Jordi et al. (2010)
	 * 
	 * @param magB Magnitude in B band
	 * @param magV Magnitude in V band
	 * @return Gaia magnitude G
	 */
	public static double gaiaMag(double magB, double magV) {
		
		// Calculate B-V color index
		
		double magBV = magB - magV;
		
		// Set up polynomial coefficients
		
		final double a = -0.0424;
		final double b = -0.0851;
		final double c = -0.3348;
		final double d =  0.0205;
		
		// Calculate G magnitude with polynomial approximation for G-V 
		
		double magG = magV + a + b*magBV + c*Math.pow(magBV, 2) + d*Math.pow(magBV, 3);
		return magG;
		
	}
	
	
	/**
	 * Gets reference triad for given reference position on the sky
	 * According to GAIA-LL-016, eq. 1
	 * 
	 * @param refAlpha0	Right ascension of reference position [rad]
	 * @param refDelta0	Declination of reference position [rad]
	 * @return			Array of three unit vectors p0, q0 and r0, which make up the reference triad
	 */
	private static GVector3d[] getReferenceTriad ( double refAlpha0, double refDelta0 ) {
		
		// Calculate sines and cosines
		double	sa = Math.sin(refAlpha0),
				ca = Math.cos(refAlpha0),
				sd = Math.sin(refDelta0),
				cd = Math.cos(refDelta0);
		
		// Calculate reference triad
		GVector3d p0 = new GVector3d( -sa, ca, 0 );
		GVector3d q0 = new GVector3d( -sd*ca, -sd*sa, cd );
		GVector3d r0 = new GVector3d( cd*ca, cd*sa, sd );
		
		return new GVector3d[] { p0, q0, r0 };
	}
	
	
	/**
	 * Converts given CoMRS position to local plane coordinates
	 * Implementation of algorithm as described Gaia-LL-061, section 3: "Transformations involving local plane coordinates"
	 * 
	 * @param pos		CoMRS position as 3D unit vector
	 * @param refAlpha0	Right ascension of reference position [rad]
	 * @param refDelta0	Declination of reference position [rad]
	 * @return			2D vector, which contains the local plane coordinates in the order (a,d) [mas]
	 */
	public static GVector2d fromCoMRStoLPC ( GVector3d comrs, double refAlpha0, double refDelta0 ) {
		
		// Calculate local celestial coordinates via gnonomic projection with reference triad (GAIA-LL-061 eq. 2)
		GVector3d[] triad = getReferenceTriad(refAlpha0, refDelta0);
		double	a = triad[0].dot(comrs) / triad[2].dot(comrs) * GaiaParam.Nature.RADIAN_MILLIARCSECOND,
				d = triad[1].dot(comrs) / triad[2].dot(comrs) * GaiaParam.Nature.RADIAN_MILLIARCSECOND;
		
		return new GVector2d(a,d);
		
	}
	
	
	/**
	 * Converts a given CoMRS position to local scan coordinates
	 * Implementation of algorithm as described Gaia-LL-061, section 3: "Transformations involving local plane coordinates"
	 * 
	 * @param comrs		CoMRS position as 3D vector
	 * @param refAlpha0	Right ascension of reference position [rad]
	 * @param refDelta0	Declination of reference position [rad]
	 * @param scanAngle	Scanning angle [rad]; angle between scanning direction and CoMRS longitude circle
	 * @return			The 2D-vector, which contains the local scan coordinates in the order (Al, Ac), i.e. (w,z) [mas]
	 */
	public static GVector2d fromCoMRStoLSC ( GVector3d comrs, double refAlpha0, double refDelta0, double scanAngle ) {
		
		return fromLPCtoLSC( fromCoMRStoLPC ( comrs, refAlpha0, refDelta0 ), scanAngle );
		
	}
	
	
	/**
	 * Converts given local plane coordinates to CoMRS position 
	 * Implementation of algorithm as described Gaia-LL-061, section 3: "Transformations involving local plane coordinates"
	 * 
	 * @param lpc		The local plane coordinates [mas]
	 * @param refAlpha0	Right ascension of reference position [rad]
	 * @param refDelta0	Declination of reference position [rad]
	 * @return			CoMRS position as unit vector
	 */
	public static GVector3d fromLPCtoCoMRS ( GVector2d lpc, double refAlpha0, double refDelta0 ) {
		
		// Get reference triad
		GVector3d[] triad = getReferenceTriad(refAlpha0, refDelta0);
		
		// Perform inverse transformation to CoMRS coordinates using the triad
		GVector2d lpcRad = GVector2d.scale(GaiaParam.Nature.MILLIARCSECOND_RADIAN, lpc);
		
		GVector3d pos = new GVector3d(triad[2]);
		pos.add(GVector3d.scale(lpcRad.getX(), triad[0]));
		pos.add(GVector3d.scale(lpcRad.getY(), triad[1]));
		pos.scale(1/Math.sqrt(1+lpcRad.normSquared()));
		
		return pos;
		
	}
	
	
	/**
	 * Converts given local scan coordinates to CoMRS position
	 * Implementation of algorithm as described Gaia-LL-061, section 3: "Transformations involving local plane coordinates"
	 * 
	 * @param lsc		Local scan coordinates [mas]
	 * @param refAlpha0	Right ascension of reference position [rad]
	 * @param refDelta0	Declination of reference position [rad]
	 * @param scanAngle	Scanning angle [rad]; angle between scanning direction and CoMRS longitude circle
	 * @return			CoMRS position as unit vector
	 */
	public static GVector3d fromLSCtoCoMRS ( GVector2d lsc, double refAlpha0, double refDelta0, double scanAngle ) {
		
		return fromLPCtoCoMRS ( fromLSCtoLPC(lsc, scanAngle), refAlpha0, refDelta0 );
		
	}
	
	
	/**
	 * Transforms from local scan coordinates to local plane coordinates
	 * 
	 * @param lsc		Local scan coordinates [mas]
	 * @param scanAngle	Scanning angle [rad]; angle between scanning direction and CoMRS longitude circle
	 * @return			Local plane coordinates [mas]
	 */
	public static GVector2d fromLSCtoLPC ( GVector2d lsc, double scanAngle ) {
		
		// Transform to local plane coordinates
		double st = Math.sin(scanAngle);
		double ct = Math.cos(scanAngle);
		
		return new GVector2d( lsc.getX()*st - lsc.getY()*ct, lsc.getX()*ct + lsc.getY()*st );
		
	}
	
	
	/**
	 * Performs error calculation for LSC -> LPC transformation
	 * 
	 * @param lscError	The errors of the local scan coordinates [mas]
	 * @param scanAngle	The scanning angle of the transit
	 * 
	 * @return	LPC errors as vector
	 */
	public static GVector2d fromLSCtoLPCError ( GVector2d lscError, double scanAngle ) {
		
		// Setup some values
		double dw2 = Math.pow ( lscError.getX(), 2 );
		double dz2 = Math.pow ( lscError.getY(), 2 );
		double st2 = Math.pow ( Math.sin(scanAngle), 2 );
		double ct2 = Math.pow ( Math.cos(scanAngle), 2 );
		
		// Calculate and return LPC error
		return new GVector2d ( Math.sqrt(dw2*st2 + dz2*ct2), Math.sqrt(dw2*ct2 + dz2*st2) );
		
	}
	
	
	/**
	 * Transforms from local plane coordinates to local scan coordinates
	 * 
	 * @param lpc		Local plane coordinates [mas]
	 * @param scanAngle	Scanning angle [rad]; angle between scanning direction and CoMRS longitude circle
	 * @return			Local scan coordinates [mas]
	 */
	public static GVector2d fromLPCtoLSC ( GVector2d lpc, double scanAngle ) {
		
		// Transform to local plane coordinates
		double	st = Math.sin(scanAngle),
				ct = Math.cos(scanAngle);
		
		return new GVector2d( lpc.getX()*st + lpc.getY()*ct, - lpc.getX()*ct + lpc.getY()*st );
		
	}
	
	
}
