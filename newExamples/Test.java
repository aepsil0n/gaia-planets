package newExamples;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for (double G = 6.0; G <= 20.0; G += 0.5) {
			ErrorModel errmod_faint = new ErrorModel(G, 0.0, true);
			double sigPlx_faint = errmod_faint.parallaxError();
			ErrorModel errmod_normal = new ErrorModel(G, 0.0, false);
			double sigPlx_normal = errmod_normal.parallaxError();
			Logger.getInstance().log(String.format("%e %e %e", G, sigPlx_normal, sigPlx_faint));
		}
		
	}

}
