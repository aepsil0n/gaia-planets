package newExamples;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;


/**
 * Singleton class managing the logging functionality of the program.
 * @author ebopp
 *
 */
public class Logger {
	
	/** Sole singleton instance */ 
	private static Logger ourInstance = new Logger();
	
	/** True, if logger is in debug mode */
	private boolean debug = false;
	
	
	/**
	 * Getter for instance of singleton logger
	 * 
	 * @return	Instance of the logger
	 */
	public static Logger getInstance() {
		return ourInstance;
	}
	
	
	/**
	 * Private constructor
	 */
	private Logger() {
	}
	
	
	/**
	 * Logs a message
	 * 
	 * @param logMessage	The message
	 * 
	 * @throws IOException 
	 */
	public void log ( String logMessage ) {
		
		System.out.println( logMessage );
		
	}
	
	
	/**
	 * Logs an error.
	 * 
	 * @param errorMessage	Error message
	 */
	public void error ( String errorMessage ) {
		
		System.err.println( errorMessage );
		
	}


	/**
	 * Logs a message if in logger is in debug mode.  
	 * 
	 * @param debugMessage	Debug message
	 */
	public void debug ( String debugMessage ) {
		
		if ( debug )
			log ( debugMessage );
		
	} 
	
	
	/**
	 * Checks for debug mode.
	 * 
	 * @return	true, if debug mode is set
	 */
	public boolean isDebug() {
		return debug;
	}

	
	/**
	 * Setter for debug mode.
	 * 
	 * @param debug	New state of debug mode
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}


}
