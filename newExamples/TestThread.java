package newExamples;

public class TestThread {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Logger.getInstance().setDebug(false);
		
		// Set up simulation
		Simulation sim = new Simulation();
		sim.setBasepath("/home/ebopp/workspace/multifit/data/");
		
		// Load systems
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/rep-sample");

		// Fit systems
		sim.fitSystems();
		
	}

}
