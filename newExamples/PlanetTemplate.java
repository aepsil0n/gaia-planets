package newExamples;

import java.util.Random;

import gaia.cu1.params.GaiaParam;
import gaiasimu.universe.source.stellar.ComponentAstrometry;
import gaiasimu.universe.source.stellar.ComponentPhotometry;
import gaiasimu.universe.source.stellar.ExoPlanet;
import gaiasimu.universe.source.stellar.ExoPlanetPhysicalParameters;
import gaiasimu.universe.source.stellar.OrbitalParams;
import gaiasimu.universe.source.stellar.Star;
import gaiasimu.universe.source.stellar.StellarAstrometry;
import gaiasimu.universe.source.stellar.StellarPhysicalParameters;

public class PlanetTemplate {
	
	/** Eccentricity of orbit */
	private double eccentricity;
	
	/** Inclination of orbital plane [rad] */
	private double inclination;
	
	/** Longitude of the ascending node [rad] */
	private double nodeAngle;
	
	/** Argument of periastron [rad] */
	private double omega2;
	
	/** Time of periastron [days] */
	private double timePeriastron;
	
	/** Orbital period [days] */
	private double period;
	
	/** Projected mass of the planet, m sin i [M_J] */
	private double mSinI;
	
	/** Name of the planet */
	private String name;
	
	
	/**
	 * Basic constructor.
	 * 
	 * @param mSinI				Projected mass of the planet [Jupiter masses]
	 * @param period			Orbital period [days]
	 * @param eccentricity		Orbit eccentricity
	 * @param inclination		Inclination angle of orbital plane [rad]
	 * @param nodeAngle			Longitude of the ascending node [rad]
	 * @param omega2			Argument of periastron [rad]
	 * @param timePeriastron	Time of periastron [rad]
	 */
	public PlanetTemplate ( double mSinI, double period, double eccentricity,
			double inclination, double nodeAngle, double omega2, double timePeriastron, String name ) {
		
		// Set private variables
		this.mSinI = mSinI;
		this.period = period;
		this.eccentricity = eccentricity;
		this.inclination = inclination;
		this.nodeAngle = nodeAngle;
		this.omega2 = omega2;
		this.timePeriastron = timePeriastron;
		
	}
	
	
	/**
	 * Clones planet template.
	 * 
	 * @return	Clone of planet template
	 */
	@Override
	public PlanetTemplate clone() {
		return new PlanetTemplate ( mSinI, period, eccentricity, inclination, nodeAngle, omega2, timePeriastron, name );
	}
	
	
	/**
	 * Creates an exoplanet based on this template.
	 * 
	 * @param astrometry	The system astrometry
	 * @param star			The parent star
	 * @param index			Index of this planet in the system
	 * @return	The created exoplanet
	 */
	public ExoPlanet createPlanet ( StellarAstrometry astrometry, Star star, int index ) {

		// set orbital parameters
		double massPlanet = mSinI / Math.sin(inclination);
		OrbitalParams orbParam = new OrbitalParams();
		double massM1 = ((StellarPhysicalParameters) star.getPhysParam()).getMass();
		double massM2 = massPlanet * GaiaParam.Nature.JUPITER_MASS / GaiaParam.Nature.SUN_MASS;
		double systemSeparation = Math.pow(massM1 + massM2, 1.0/3.0) * Math.pow(period/GaiaParam.Nature.JULIANYEAR_DAY, 2.0/3.0);
		orbParam.semiMajorAxis = massM1 / (massM1 + massM2) * systemSeparation;
		orbParam.period = period;
		orbParam.timeperiastron = timePeriastron;
		orbParam.eccentricity = eccentricity;
		orbParam.omega2 = omega2;
		if (orbParam.eccentricity == 0)
			orbParam.omega2 = 0;
		orbParam.nodeangle = nodeAngle;
		orbParam.inclination = inclination;
		
		// Dummy values 
		double tEq = 400.0;			// dummy value
		double plRadius = 0.1; // dummy value
		
		// Obtain distance via parallax from astrometry 
		double distance = 1e3 / astrometry.getVarpi();
		
		// TODO put a correct value for absolute visual and bolometric magnitude, vsini, V-I
		double magMV2 = 32.58, MBol2 = magMV2-4; //Mag MV from Baraffe models.
		double vSini = 0.0, vmi2 = 9.9999;
		
		// Initialize ComponentAstrometry pointing to the primary and then correct with setCompanion()
		ComponentAstrometry compAstrom = new ComponentAstrometry(index, orbParam);
		ExoPlanetPhysicalParameters compPhys =
			new ExoPlanetPhysicalParameters(tEq, plRadius, vSini, massM2, MBol2, star.getStarPhysicalParams());
		
		ComponentPhotometry compPhot =
			new ComponentPhotometry(compAstrom, magMV2, distance, vmi2, star.getPhotometry().getAbsV(), star.getPhotometry().getExtinctionCurve());
		
		ExoPlanet planet = new ExoPlanet(star.getRootId(), compPhot, compAstrom, compPhys);
		compPhot.setTargetAstroSource(planet);
		compAstrom.setTargetAstroSource(planet);
		compPhys.setTargetAstroSource(planet);
		
		return planet;
		
	}
	
	
	/**
	 * Randomizes the template's inclination in (0, pi) and its mass accordingly.
	 */
	public void randomizeInclination() {
		
		// Generate random new inclination
		Random random = new Random();
		double newIncl = 0;
		while ( Math.sin(newIncl) == 0 )
			newIncl = random.nextDouble() * Math.PI;
		
		// Calculate randomized mass and inclination
		inclination = newIncl;
		
	}

	// Setters & getters
	// TODO: documentation!

	public double getEccentricity() {
		return eccentricity;
	}


	public void setEccentricity(double eccentricity) {
		this.eccentricity = eccentricity;
	}


	public double getInclination() {
		return inclination;
	}


	public void setInclination(double inclination) {
		this.inclination = inclination;
	}


	public double getNodeAngle() {
		return nodeAngle;
	}


	public void setNodeAngle(double nodeAngle) {
		this.nodeAngle = nodeAngle;
	}


	public double getOmega2() {
		return omega2;
	}


	public void setOmega2(double omega2) {
		this.omega2 = omega2;
	}


	public double getTimePeriastron() {
		return timePeriastron;
	}


	public void setTimePeriastron(double timePeriastron) {
		this.timePeriastron = timePeriastron;
	}


	public double getPeriod() {
		return period;
	}


	public void setPeriod(double period) {
		this.period = period;
	}


	public double getProjectedMass() {
		return mSinI;
	}


	public void setProjectedMass ( double mSinI ) {
		this.mSinI = mSinI;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
}
