package newExamples;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.ComponentAstrometry;
import gaiasimu.universe.source.stellar.ComponentPhotometry;
import gaiasimu.universe.source.stellar.SpectralType;
import gaiasimu.universe.source.stellar.Star;
import gaiasimu.universe.source.stellar.StarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarAstrometry;

public class StarTemplate {

	/** Absolute magnitude in V band */
	private double magMv;
	
	/** V-I color index */
	private double vMinusI;
	
	/** Absorption in V band */
	private double absV;
	
	/** FeH metallicity */
	private double feH;
	
	/** Alpha element enhancements */
	private double alphaE;
	
	/** Mass of the star */
	private double mass;
	
	/** Spectral type as string */
	private String spType;
	
	/** Bolometric magnitude */
	private double magBol;
	
	/** Population according to Besancon model */
	private int pop;
	
	/** Name of the star */
	private String name;
	
	
	/**
	 * Basic constructor.
	 * 
	 * @param magMv		Absolute magnitude in V band [mag]
	 * @param vMinusI	V-I color index [mag]
	 * @param absV		Absorption in V band
	 * @param feH		FeH metallicity
	 * @param alphaE	Alpha element enhancements
	 * @param mass		Mass of the star [Solar masses]
	 * @param spType	Spectral type as string
	 * @param magBol	Bolometric magnitude [mag]
	 * @param pop		Population according to Besancon model
	 */
	public StarTemplate ( double magMv, double vMinusI, double absV, double feH, double alphaE, double mass,
			String spType, double magBol, int pop, String name ) {
		
		// Set values
		this.magMv = magMv;
		this.vMinusI = vMinusI;
		this.absV = absV;
		this.feH = feH;
		this.alphaE = alphaE;
		this.mass = mass;
		this.spType = spType;
		this.magBol = magBol;
		this.pop = pop;
		this.name = name;
		
	}
	
	
	/**
	 * Clones the star template.
	 * 
	 * @return A clone of the template
	 */
	@Override
	public StarTemplate clone() {
		
		return new StarTemplate(magMv, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop, name);
		
	}
	
	
	/**
	 * Creates a star from the template
	 */
	public Star createStar( StellarAstrometry astrometry ) {
		
		// not important but required, arbitrary so far
		long idRoot = 1223345453L;
		
		// Generate physics.
		SpectralType spectralType = null;
		try {
			spectralType = new SpectralType( spType );
		} catch (SimuException e) {
			e.printStackTrace();
		}
		StarPhysicalParameters starPhysicalParameters = new StarPhysicalParameters(mass, spectralType, magBol, pop, feH, alphaE);
		
		// Generate empty astrometry
		ComponentAstrometry primaryAstrometry = new ComponentAstrometry();
		ComponentPhotometry primaryPhotometry = new ComponentPhotometry(primaryAstrometry, magMv, 1.0e3 / astrometry.getVarpi(), vMinusI, absV);
		
		Star star = new Star(idRoot, primaryPhotometry, primaryAstrometry, starPhysicalParameters);
		
		return star;
		
	}

	
	// Setters & getters
	// TODO: documentation!

	public double getMagMv() {
		return magMv;
	}


	public void setMagMv(double magMv) {
		this.magMv = magMv;
	}


	public double getVMinusI() {
		return vMinusI;
	}


	public void setVMinusI(double minusI) {
		vMinusI = minusI;
	}


	public double getAbsV() {
		return absV;
	}


	public void setAbsV(double absV) {
		this.absV = absV;
	}


	public double getFeH() {
		return feH;
	}


	public void setFeH(double feH) {
		this.feH = feH;
	}


	public double getAlphaE() {
		return alphaE;
	}


	public void setAlphaE(double alphaE) {
		this.alphaE = alphaE;
	}


	public double getMass() {
		return mass;
	}


	public void setMass(double mass) {
		this.mass = mass;
	}


	public String getSpType() {
		return spType;
	}


	public void setSpType(String spType) {
		this.spType = spType;
	}


	public double getMagBol() {
		return magBol;
	}


	public void setMagBol(double magBol) {
		this.magBol = magBol;
	}


	public int getPop() {
		return pop;
	}


	public void setPop(int pop) {
		this.pop = pop;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


}
