package newExamples;

public enum FitQuantity {

	/** Possible fitting parameters */
	INCLINATION,	// Inclination of planetary orbit [rad]
	NODEANGLE,		// Node angle [rad]
	TIMEPERI,		// Time of periastron [JD]
	OMEGA2,			// Argument of periastron [rad]
	ALPHA_OFFSET,	// Right ascension offset [mas]
	DELTA_OFFSET,	// Declination offset [mas]
	PARALLAX,		// Parallax [mas]
	MU_ALPHA,		// Right ascension proper motion [mas/yr]
	MU_DELTA;		// Declination proper motion [mas/yr]
	
}
