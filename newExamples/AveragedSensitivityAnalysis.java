package newExamples;

public class AveragedSensitivityAnalysis {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Set up simulation
		Simulation sim = new Simulation();
		sim.setBasepath("/home/ebopp/workspace/avgsens/data/");
		sim.setRandGeo(true); // randomized orbital geometry for averaging
		
		// Load period and eccentricity variated systems
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/sens-period");
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/sens-ecc");
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/sens-eclat");
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/sens-msini");
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/sens-period2");
		
		// Inclination angles
		final int NFITS = 100;
		final int NINCLS = 5;
		final double PI2 = Math.PI/2;
		final double START = PI2*0.75;
		final double END = PI2;
		final double[] INCS = new double[NINCLS];
		for(int i = 0; i < INCS.length; i++)
			INCS[i] = (START+(END-START)*(double)i/(double)(NINCLS-1));
		
		// Fit systems
		sim.fitSystems(NFITS, INCS);
		
	}

}
