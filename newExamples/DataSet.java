package newExamples;

import gaiasimu.universe.source.AstrometricParam;
import gaiasimu.universe.source.stellar.StellarAstrometry;

import java.util.ArrayList;


/**
 * This class represents a bunch of generated data, either continuous projected orbits or a
 * series of single transit data. It provides functionality to write output to files via plotting
 * routines.
 *  
 * @author ebopp
 *
 */
public class DataSet {

	/** List of transit data included */
	private ArrayList<TransitData> transits;
	
	/** Reference astrometry */
	private StellarAstrometry astroRef;
	
	
	// Constructor(s)
	
	/**
	 * Default constructor. Adds no transits.
	 */
	public DataSet () {
		transits = new ArrayList<TransitData>();
	}
	
	
	/**
	 * Constructor providing reference astrometry.
	 * 
	 * @param astrometry	The reference astrometry for plotting purposes
	 */
	public DataSet ( StellarAstrometry astroRef ) {
		
		transits = new ArrayList<TransitData>();
		this.astroRef = astroRef;
		
	}
	
	

	
	

	// List management 
	
	/**
	 * Adds new transit data to list of transits.
	 * 
	 * @param transit	Transit data
	 * @return			Given transit data for further processing
	 */
	public TransitData addTransit ( TransitData transit ) {
		this.transits.add ( transit );
		return transit;
	}
	
	/**
	 * Setter for list of transit data
	 *  
	 * @param transits	List of transits
	 */
	public void setTransits(ArrayList<TransitData> transits) {
		this.transits = transits;
	}

	/**
	 * Getter for list of transit data
	 * 
	 * @return	List of transits
	 */
	public ArrayList<TransitData> getTransits() {
		return transits;
	}
	
	
	/**
	 * Getter for reference astrometry
	 * @return	The reference astrometry
	 */
	public StellarAstrometry getReferenceAstrometry() {
		return astroRef;
	}
	
	
	/**
	 * Setter for reference astrometry.
	 * 
	 * @param astroRef	The reference astrometry
	 */
	public void setReferenceAstrometry ( StellarAstrometry astroRef ) {
		this.astroRef = astroRef;
	}
	
	
	/**
	 * Setter for reference astrometry creating new astrometry from astrometric parameters.
	 *  
	 * @param alpha			Right ascension [deg]
	 * @param delta			Declination [deg]
	 * @param parallax		Parallax [mas]
	 * @param muAlphaStar	True arc right ascension proper motion [mas/yr]
	 * @param muDelta		Declination proper motion [mas/yr]
	 * @param radVel		Radial velocity [km/s]
	 */
	public void setReferenceAstrometry(double alpha, double delta, double parallax, double muAlphaStar, double muDelta, double radVel) {
		this.astroRef = new StellarAstrometry ( new AstrometricParam ( alpha, delta, parallax, muAlphaStar, muDelta, radVel ) );
	}

	
}
