package newExamples;

import java.io.File;
import java.io.IOException;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.gaia.Gaia;
import gaiasimu.universe.source.stellar.StarSystemSimuException;


/**
 * The class SimulatedSystem represents a star system.
 */
public class SimulatedSystem implements Logging {
	
	/** Simulation to which the system belongs */
	private Simulation simulation;
	
	/** Name of star as referenced by in exoplanet.eu */
	private String name;
	
	/** Data generator for the system */
	private DataGenerator generator;
	
	/** Simulated observational data */
	private ObservationalDataSet obsData;
	
	/** Output directory for any data plotted by this system */
	private String outputDir;

	/** System template */
	private SystemTemplate template;
	
	
	/**
	 * Constructor.
	 * 
	 * @param name			Name of the system
	 * @param simulation	Parent simulation
	 * 
	 * @throws SimuException
	 */
	public SimulatedSystem(String name, Simulation simulation) {
		
		this.name = name;
		this.simulation = simulation;
		
	}
	
	
	/**
	 * Simulates observation of the system and generates thus observational data.
	 * 
	 * @param addNoise		true, if gaussian noise i.e. statistical errors should be included
	 * 
	 * @throws SimuException
	 * @throws GaiaException
	 * @throws IOException
	 * @throws StarSystemSimuException
	 */
	public synchronized ObservationalDataSet generateData(boolean addNoise) {
		
		logger.debug("Generating data for "+name);
		
		if(generator == null)
			generator = new DataGenerator(template.createModel(getSimulation(), true, false, false), simulation);
		
		try {
			
			obsData = generator.getObservationalData(addNoise);
			return obsData;
			
		} catch (SimuException e) {
			e.printStackTrace();
		} catch (GaiaException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (StarSystemSimuException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	/**
	 * Fits a set of inclinations specified by lowest inclination, highest inclination and step size. 
	 * 
	 * @param subCatalogue	Sub catalogue of Gaia to be used for the fit
	 * @param N				How often should each fit be repeated
	 * @param incs			Array of inclination angles to fit
	 */
	public void fitInclinations(int N, double[] incs) {
		
		// Inclination loop
		int i=0;
		for(double inc: incs) {
			
			// Initialize a new thread for this inclination
			FittingRun fitRun = new FittingRun(N, inc, outputDir+i+"/", this, false);
			Thread fitThread = new Thread(fitRun);
			fitThread.start();
			
			// Increment
			i++;
			
		}
		
	}
	
	
	/**
	 * Setter for system name
	 * 
	 * @param name Name of system
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * Getter for system name
	 * 
	 * @return Name of system
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Getter for Gaia instance
	 * @return Gaia instance
	 */
	public Gaia getGaia() {
		return simulation.getGaia();
	}

	
	/**
	 * Getter for associated simulation
	 * 
	 * @return Simulation to which the system belongs
	 */
	public Simulation getSimulation() {
		return simulation;
	}

	
	/**
	 * Plots transit data
	 * 
	 * @param fname	Name of file to plot to
	 */
	public void plotTransitData(String fname) {
		obsData.plotTransitData((fname.startsWith(outputDir)? "": outputDir) + fname);
	}

	
	/**
	 * Plots the star trajectory
	 * 
	 * @param fname	Name of file to plot to
	 */
	public void plotTrajectory(String fname) {
		generator.getOrbit().plotTrajectory((fname.startsWith(outputDir)? "": outputDir) + fname);
	}


	/**
	 * Setter for output directory. Automatically adds a slash if none is provided.
	 * 
	 * @param outputDir	The output directory
	 */
	public void setOutputDir(String outputDir) {
		
		// Make output directory in case it does not exists
		if ( ! outputDir.endsWith("/") )
			outputDir += "/";
		(new File(outputDir)).mkdir();
		
		this.outputDir = outputDir;
		
	}


	/**
	 * Getter fot output directory.
	 * 
	 * @return	The output directory
	 */
	public String getOutputDir() {
		return outputDir;
	}


	/**
	 * Setter for model template.
	 * 
	 * @param sysTemp
	 */
	public void setModelTemplate(SystemTemplate sysTemp) {
		
		this.template = sysTemp;
		
	}


	/**
	 * Getter for model template.
	 * 
	 * @return Model template
	 */
	public SystemTemplate getModelTemplate() {
		return template;
	}
	
}
