package newExamples;

import gaia.cu1.params.GaiaParam;
import gaiasimu.universe.source.Photometry;


/**
 * Wrapper class for error model functionality.
 * 
 * @author ebopp
 */
public class ErrorModel {

	/** Normal cut-off point */
	private final double NORMAL_CUTOFF = 12;

	/** Cut-off point for fainter cut-off error model */
	private final double FAINT_CUTOFF = 13.6719;

	/** Factor to convert from parallax to true arc right ascension error */
	private final double ALPHASTAR_FACTOR = 0.787;

	/** Factor to convert from parallax to declination error */
	private final double DELTA_FACTOR = 0.699;

	/** Factor to convert from parallax to true arc right ascension proper motion error */
	private final double MUALPHASTAR_FACTOR = 0.556;

	/** Factor to convert from parallax to declination proper motion error */
	private final double MUDELTA_FACTOR = 0.496;

	/** The source's Gaia magnitude */
	private double magG;

	/** The source's V-I color index */
	private double magVI; 

	/** Whether the fainter cut-off magnitude should be used */
	private boolean useFaintCutOff;

	/** Check if some variable was changed */
	private boolean changed;

	/** Storage variable to save calculation time if no parameter was changed since last invocation of parallaxError() */
	private double storedSigPar;

	/**
	 * Constructor based on given photometric values.
	 * 
	 * @param magG	The source's Gaia magnitude 
	 * @param magVI	The source's V-I color index
	 * @param useFaintCutOff	Whether the fainter cut-off magnitude should be used
	 */
	public ErrorModel ( double magG, double magVI, boolean useFaintCutOff ) {

		// Set values
		this.magG = magG;
		this.magVI = magVI;
		this.useFaintCutOff = useFaintCutOff;

		// The values have been changed
		changed = true;

	}


	/**
	 * Constructor based on given photometry.
	 * 
	 * @param phot	The source photometry
	 * @param useFaintCutOff	Whether the fainter cut-off magnitude should be used
	 */
	public ErrorModel ( Photometry phot, boolean useFaintCutOff ) {

		// Obtain values from photometry
		this.magG = phot.getMeanAbsoluteMv();
		this.magVI = phot.getMeanVI();
		this.useFaintCutOff = useFaintCutOff;

		// The values have been changed
		changed = true;

	}


	/**
	 * Cut-off function required for error estimation
	 * 
	 * @param magG		Gaia magnitude
	 * @param cutOff	Cut-off magnitude of function
	 * @return z as described in Gaia astrometric performance
	 */
	private double cutOffFunction (double magG, double cutOff) {

		double cutOffRes = Math.pow(10, 0.4*(cutOff-15));
		double normRes = Math.pow(10, 0.4*(magG-15));
		return Math.max(normRes, cutOffRes);

	}


	/**
	 * Gets parallax error estimate for given source in mas
	 * 
	 * @return Estimate for parallax error
	 */
	public double parallaxError() {

		if ( changed ) {

			// Color correction term depending on V-I color index
			double colorCorrection = 0.986 + (1-0.986) * magVI;

			// Cut-off function
			double z = cutOffFunction ( magG, useFaintCutOff ? FAINT_CUTOFF : NORMAL_CUTOFF );

			// Parallax error
			storedSigPar = 1.0e-3 * Math.pow(9.3 + 658.1 * z + 4.568 * Math.pow(z, 2), 0.5) * colorCorrection;
			
			// Reset changed flag
			changed = false;
			
		}

		return storedSigPar;

	}


	/**
	 * Gets true arc right ascension error estimate for given source in deg
	 * 
	 * @return Estimate for parallax error
	 */
	public double alphaStarError() {
		return GaiaParam.Nature.MILLIARCSECOND_RADIAN * GaiaParam.Nature.RADIAN_DEGREE * ALPHASTAR_FACTOR * parallaxError();
	}


	/**
	 * Gets declination error estimate for given source in deg
	 * 
	 * @return Estimate for parallax error
	 */
	public double deltaError() {
		return GaiaParam.Nature.MILLIARCSECOND_RADIAN * GaiaParam.Nature.RADIAN_DEGREE * DELTA_FACTOR * parallaxError();
	}


	/**
	 * Gets true arc right ascension proper motion error estimate for given source in mas
	 * 
	 * @return Estimate for parallax error
	 */
	public double muAlphaStarError() {
		return MUALPHASTAR_FACTOR * parallaxError();
	}


	/**
	 * Gets declination error estimate for given source in mas
	 * 
	 * @return Estimate for parallax error
	 */
	public double muDeltaError() {
		return MUDELTA_FACTOR * parallaxError();
	}


	public double getMagG() {
		return magG;
	}


	public void setMagG(double magG) {

		this.magG = magG;

		// The values have been changed
		changed = true;

	}


	public double getMagVI() {
		return magVI;
	}


	public void setMagVI(double magVI) {

		this.magVI = magVI;

		// The values have been changed
		changed = true;

	}


	public boolean isUseFaintCutOff() {
		return useFaintCutOff;
	}


	public void setUseFaintCutOff(boolean useFaintCutOff) {

		this.useFaintCutOff = useFaintCutOff;

		// The values have been changed
		changed = true;

	}

}
