package newExamples;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StarSystemSimuException;

import java.io.IOException;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

/**
 * This class fits one planet with a variety of inclinations. 
 */
public class FitSetOfInclinations {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException
	 * @throws StarSystemSimuException 
	 */
	public static void main(String[] args) throws SimuException, 
			IOException, GaiaException, FunctionEvaluationException,
			IllegalArgumentException, OptimizationException, StarSystemSimuException {
		
		// Initiate random default exoplanet orbital parameters not to be varied
		double eccentricity		= 0.2;		// Eccentricity of orbit
		double nodeAngle		= 0.25;		// Position angle of the line of nodes [rad]
		double omega2			= -1.44;		// Argument of periastron [rad]
		double timePeriastron	= 0.0;		// Time of periastron [days]
		
		// Star data -- corresponds to a sun-like star at 5 pc distance
		double magMv    = 6.83;			// Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
		double vMinusI  = Conversion.colorBVtoVI(0.656);	// Intrinsec Mean (V-I) colour. 51 Peg: +0.8
		double absV     = 0.0;				// Interstellar absortion in the V band
		double ra       = -86.0;			// RA  ICRS (deg).
		double dec      = 47.2;				// DEC ICRS (deg).
		double distance = 5.0;				// Distance from Sol [pc]
		double parallax = 1e3/distance;		// Parallax [mas]
		double muRa     = 1.0;				// Proper motion RA  [mas/yr]
		double muDec    = -2.5;				// Proper motion DEC [mas/yr]
		double vRad     = -1.3;				// Radial velocity (km/s)
		double feH      = 0.05;				// [Fe/H]
		double alphaE   = 0.0;				// Alpha elements enhancement
		double mass     = 1.0;				// (Msun?)
		double magBol   = magMv + Conversion.colorBVtoBC(0.656);	// (Compute from bolometric corrections in Allen)
		int    pop      = 6;				// (Besancon model. 6: 5-7 Gyr)
		String spType   = "M4 V";			// String defining the spectral type
		
		// Generate simulation
		Simulation sim = new Simulation();
		sim.setBasepath("/home/eduard/dev/science/testdata/");
		Logger.getInstance().setDebug(true);
		
		// Test system properties
		String names[] = new String[]{"gamma"};
		double projMasses[] = new double[]{5.0};
		double periods[] = new double[]{1000.0};
		
		// Inclination angles
		final int NFITS = 2;
		final int NINCLS = 5;
		final double PI2 = Math.PI/2;
		final double[] INCS = new double[NINCLS];
		for(int i = 0; i < INCS.length; i++)
			INCS[i] = PI2*(double)(i+1)/(double)(NINCLS);
		
		// Loop through different inclinations for the given planet
		for(int i = 0; i < names.length; i++) {
			
			String name = names[i];
			double period = periods[i];
			double mSinI = projMasses[i];
			
			// Create template of system
			SystemTemplate sysTemp = new SystemTemplate();
			sysTemp.provide(new AstrometryTemplate(ra, dec, parallax, muRa, muDec, vRad));
			sysTemp.provide(new StarTemplate(magMv, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop, name));
			sysTemp.provide(new PlanetTemplate(mSinI, period, eccentricity, Math.PI/2, nodeAngle, omega2, timePeriastron, name+" b"));
			
			// Add solar system to simulation and attach model template 
			SimulatedSystem system = sim.addSystem(name);
			system.setModelTemplate(sysTemp);
				
			// Fit the system and save the result
			system.fitInclinations(NFITS, INCS);
			
		} // end simulation loop
		
	} // end main method
	
} // end class
