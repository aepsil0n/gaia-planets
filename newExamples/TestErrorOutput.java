package newExamples;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StarSystemSimuException;

import java.io.IOException;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

/**
 * The Class Test.
 * TODO: Rewrite to match new output pattern!
 */
public class TestErrorOutput {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException
	 * @throws StarSystemSimuException 
	 */
	public static void main(String[] args) throws SimuException, 
			IOException, GaiaException, FunctionEvaluationException,
			IllegalArgumentException, OptimizationException, StarSystemSimuException {
		
		// Exoplanet orbital parameters
		double eccentricity   = 0.0;		// Eccentricity of orbit
		double inclination    = 0.0;	// Inclination angle (rad)
		double nodeAngle      = 0.0;		// Position angle of the line of nodes (rad)
		double omega2         = 0.0;		// Argument of periastron (rad)
		double period         = 100.0;		// Orbital period (days)
		double timePeriastron = 0.0;		// Time of periastron (days)
		double massPlanet     = 1.0;		// Mass of planet (Jupiter masses)
		
		// Star data
		double magMv    = 10.17;			// Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
		double vMinusI  = 1.52;				// Intrinsec Mean (V-I) colour. 51 Peg: +0.8
		double absV     = 0.0;				// Interstellar absortion in the V band
		double ra       = 343.31972458;		// RA  ICRS (deg). 51 Peg: 344.3665854
		double dec      = -14.263700556;	// DEC ICRS (deg). 51 Peg: 20.76883222
		double parallax = 1e3/4.7;			// (mas). 51 Peg: 64.07 mas
		double distance = 4.7;				// Distance from Sol (pc)
		double muRa     = 1.0;				// Proper motion RA  (mas/yr)
		double muDec    = -2.0;				// Proper motion DEC (mas/yr)
		double vRad     = -1.7;				// Radial velocity (km/s)
		double feH      = 0.05;				// [Fe/H]
		double alphaE   = 0.0;				// Alpha elements enhancement
		double mass     = 1.0;				// (Msun?)
		double magBol   = 12.1;				// (Compute from bolometric corrections in Allen)
		int    pop      = 6;				// (Besancon model. 6: 5-7 Gyr)
		String spType   = "M4 V";			// String defining the spectral type
		
		// Generate simulation
		Simulation sim = new Simulation();
		
		// Generate solar system, set parameters, generate planet and star; finalize
		SimulatedSystem system = sim.addSystem ( "GJ 876" );
		
		// Create template of system
		SystemTemplate sysTemp = new SystemTemplate();
		sysTemp.provide ( new AstrometryTemplate ( ra, dec, parallax, muRa, muDec, vRad ) );
		sysTemp.provide ( new StarTemplate ( magMv, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop ) );
		sysTemp.provide ( new PlanetTemplate ( massPlanet, period, timePeriastron, eccentricity, inclination, omega2, nodeAngle ) );
		
		// Create observation model from template
		Model obsModel = sysTemp.createModel ( sim, true, false );
		system.setObservationModel ( obsModel );
		
		// Plot without any errors
		system.generateData ( 20.0, true, false, false );
		system.plotTrajectory ( "/home/ebopp/workspace/concept-output/data/ellipse.out" );
		system.plotTransitData ( "/home/ebopp/workspace/concept-output/data/noerror.out" );
		
		// Plot with statistical errors
		system.generateData ( 20.0, true, true, false );
		system.plotTransitData ( "/home/ebopp/workspace/concept-output/data/statonly.out" );
		
		// Plot with systematic errors only
		system.generateData ( 20.0, true, false, true );
		system.plotTransitData ( "/home/ebopp/workspace/concept-output/data/sysonly.out" );
		
		// Plot with both systematic and statistic errors
		system.generateData ( 20.0, true, true, true );
		system.plotTransitData ( "/home/ebopp/workspace/concept-output/data/complete.out" );
		
	}
	
}
