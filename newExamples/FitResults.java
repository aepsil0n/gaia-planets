package newExamples;

/**
 * The Class ExoplanetInclinationFitResults stores the results from an exoplanet inclination fit run.
 */
public class FitResults {
	
	
	
	/** The best fit epoch astrometry (mas). */
	private double[] fit;
	
	/** The best fit variables (inclination angle, rad). */
	private double[] variables;
	
	/** The best fit errors (mas). */
	private double[] errors;
	
	/** The best fit covariance. */
	private double[][] covariance;
	
	/** Reduced chi square of fit */
	private double chi2;
	
	/**
	 * The Constructor.
	 * 
	 * @param fit			the best fit epoch astrometry (mas)
	 * @param variables		the best fit variables (inclination angle, rad)
	 * @param errors		the best fit errors (mas)
	 * @param covariance	the best fit covariance
	 * @param chi2			chi square of fit
	 */
	public FitResults(
			double[] fit,
			double[] variables,
			double[] errors,
			double[][] covariance,
			double chi2) {
		super();
		this.fit		= fit;
		this.variables	= variables;
		this.errors		= errors;
		this.covariance	= covariance;
		this.setChiSquare(chi2);
		
		// adjust inclination to [-pi,pi]
		double incl = this.variables[0];
		while (incl > Math.PI) {
			incl -= Math.PI;
		}
		while (incl < -Math.PI) {
			incl += Math.PI;
		}
		this.variables[0] = incl;
	}
	
	
	/**
	 * Gets the best fit epoch astrometry (mas).
	 *
	 * @return the best fit epoch astrometry (mas)
	 */
	public double[] getFit() {
		return fit;
	}
	
	
	/**
	 * Sets the best fit epoch astrometry (mas).
	 *
	 * @param fit the new best fit epoch astrometry (mas)
	 */
	public void setFit(double[] fit) {
		this.fit = fit;
	}
	
	
	/**
	 * Gets the best fit variables (inclination angle, rad).
	 *
	 * @return the best fit variables (inclination angle, rad)
	 */
	public double[] getVariables() {
		return variables;
	}
	
	
	/**
	 * Sets the best fit variables (inclination angle, rad).
	 *
	 * @param variables the new best fit variables (inclination angle, rad)
	 */
	public void setVariables(double[] variables) {
		this.variables = variables;
	}
	
	
	/**
	 * Gets the best fit errors (mas).
	 *
	 * @return the best fit errors (mas)
	 */
	public double[] getErrors() {
		return errors;
	}
	
	
	/**
	 * Sets the best fit errors (mas).
	 *
	 * @param errors the new errors (mas)
	 */
	public void setErrors(double[] errors) {
		this.errors = errors;
	}
	
	
	/**
	 * Gets the best fit covariances.
	 *
	 * @return the best fit covariances
	 */
	public double[][] getCovariance() {
		return covariance;
	}
	
	
	/**
	 * Sets the best fit covariances.
	 *
	 * @param covariance the new best fit covariances
	 */
	public void setCovariance(double[][] covariance) {
		this.covariance = covariance;
	}


	public void setChiSquare(double chi2) {
		this.chi2 = chi2;
	}


	public double getChiSquare() {
		return chi2;
	}	

	
}
