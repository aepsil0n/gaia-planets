package newExamples;

import gaia.cu1.tools.numeric.algebra.GVector2d;
import gaiasimu.SimuException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;


/**
 * This class is a data set with specific output routines for theoretical continuous astrometric signals.
 * 
 * @author ebopp
 */
public class TheoreticalDataSet extends DataSet implements Logging {
	
	/**
	 * Basic constructor
	 */
	public TheoreticalDataSet() {
		super();
	}
	
	
	/**
	 * Prints star trajectory with a given writer
	 * The algorithm automatically calculates the necessary time sample to produce a smooth orbital curve
	 * 
	 * Output format: Time as JD, LPC a, LPC d, LSC AL, LSC AC
	 *  
	 * @param out		Writer for output
	 * @throws IOException 
	 * @throws SimuException 
	 */
	private void plotTrajectory ( Writer out ) throws IOException, SimuException {
		
		for ( TransitData tr: getTransits() ) {
			
			double timeJD = tr.getTime().getJD();
			GVector2d lpc = tr.getLPC();
			
			// Assemble output string
			String str = String.format("%20.13e %20.13e %20.13e\n",
					timeJD,
					lpc.getX(), lpc.getY());
			
			// Write to writer
			out.write ( str );
			
		}
		
	}
	
	
	/**
	 * Prints star trajectory to standard output
	 * Format: JD, LPC a, LPC d
	 *  
	 * @throws IOException 
	 */
	public void plotTrajectory () {
		
		try {
			
			plotTrajectory(new PrintWriter(System.out));
			
		} catch (IOException e) {
			
			logger.error ( "IOException occured drawing trajectory to standard output!" );
			e.printStackTrace();
			
		} catch (SimuException e) {
			
			logger.error ( "SimuException occured drawing trajectory to standard output!" );
			e.printStackTrace();
			
		}
		
	}
	
	
	/**
	 * Prints star trajectory to standard output
	 * Format: JD, LPC a, LPC d
	 *  
	 * @param fname		Name of output file
	 */
	public void plotTrajectory ( String fname ) {
		
		File file = new File(fname);
		
		try {
			
			logger.debug("Plotting orbit to " + fname);
			
			FileWriter filewriter = new FileWriter(file, false);
			BufferedWriter buffwriter = new BufferedWriter(filewriter);
			plotTrajectory ( buffwriter );
			buffwriter.close();
			
		} catch (FileNotFoundException e) {
			
			logger.error ( "File not found: "+fname+" -- drawing of trajectory failed!)" );
			e.printStackTrace();
			
		} catch (IOException e) {
			
			logger.error ( "IOException occured drawing trajectory to file!" );
			e.printStackTrace();
			
		} catch (SimuException e) {
			
			logger.error ( "SimuException occured drawing trajectory to file!" );
			e.printStackTrace();
			
		}
		
	}
	
	
}
