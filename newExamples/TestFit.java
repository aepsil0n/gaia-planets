package newExamples;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StarSystemSimuException;

import java.io.IOException;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

/**
 * Test class fitting one planet to demonstrate functionality 
 */
public class TestFit {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException
	 * @throws StarSystemSimuException 
	 */
	public static void main(String[] args) throws SimuException, 
			IOException, GaiaException, FunctionEvaluationException,
			IllegalArgumentException, OptimizationException, StarSystemSimuException {
		
		// Initiate random default exoplanet orbital parameters not to be varied
		double eccentricity		= 0.2;		// Eccentricity of orbit
		double inclination		= 2.5;		// Inclination angle [rad]
		double nodeAngle		= 0.25;		// Position angle of the line of nodes [rad]
		double omega2			= -1.44;		// Argument of periastron [rad]
		double timePeriastron	= 0.0;		// Time of periastron [days]
		double period			= 500.0;	// Orbital period [days]
		double massPlanet		= 2.0;		// Planet mass [MJ]
		
		// Star data -- corresponds to a sun-like star at 5 pc distance
		double magMv    = 4.83;			// Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
		double vMinusI  = Conversion.colorBVtoVI(0.656);	// Intrinsec Mean (V-I) colour. 51 Peg: +0.8
		double absV     = 0.0;				// Interstellar absortion in the V band
		double ra       = 126.0;			// RA  ICRS (deg).
		double dec      = -24.3;			// DEC ICRS (deg).
		double distance = 5.0;				// Distance from Sol [pc]
		double parallax = 1e3/distance;		// Parallax [mas]
		double muRa     = 1.0;				// Proper motion RA  [mas/yr]
		double muDec    = -2.5;				// Proper motion DEC [mas/yr]
		double vRad     = -1.3;				// Radial velocity (km/s)
		double feH      = 0.05;				// [Fe/H]
		double alphaE   = 0.0;				// Alpha elements enhancement
		double mass     = 1.0;				// (Msun?)
		double magBol   = Conversion.colorBVtoMagBol(0.656);	// (Compute from bolometric corrections in Allen)
		int    pop      = 6;				// (Besancon model. 6: 5-7 Gyr)
		String spType   = "M4 V";			// String defining the spectral type
		
		Logger.getInstance().setDebug(true);
		
		// Generate simulation
		Simulation sim = new Simulation();
		sim.setBasepath ( "/home/ebopp/workspace/testfit/data/" );
		
		// Generate solar system, set parameters, generate planet and star; finalize
		SimulatedSystem system = sim.addSystem ( "test" );
		
		// Create template of system
		SystemTemplate sysTemp = new SystemTemplate();
		sysTemp.provide ( new AstrometryTemplate ( ra, dec, parallax, muRa, muDec, vRad ) );
		sysTemp.provide ( new StarTemplate ( magMv, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop ) );
		sysTemp.provide ( new PlanetTemplate ( massPlanet, period, eccentricity, inclination, nodeAngle, omega2, timePeriastron ) );
		
		// Create fitting model for system
		Model fitModel = sysTemp.createModel ( sim, false, true );
		system.setFittingModel ( fitModel );
		
		// Create observation model from template
		Model obsModel = sysTemp.createModel ( sim, true, false );
		system.setObservationModel ( obsModel );
		
		// Generate data with full simulation
		system.generateData ( 20.0, true, true, true );
		system.plotTransitData ( "simu-obs" );
		
		// Fitting process
		system.fitExoPlanets ( 20.0, true );
		
	}
}
