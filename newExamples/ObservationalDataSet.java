package newExamples;

import gaia.cu1.tools.numeric.algebra.GVector2d;
import gaiasimu.GaiaSimuTime;
import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StellarAstrometry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;


/**
 * This class is a data set with specific output routines for single transit data.
 * 
 * @author ebopp
 */
public class ObservationalDataSet extends DataSet implements Logging {

	// Constructors
	
	/**
	 * Basic constructor
	 */
	public ObservationalDataSet() {
		super();
	}
	
	
	// Output routines for observational data
	
	/**
	 * Plots the transit data to a given writer object
	 * 
	 * @param out	Writer used for output
	 * 
	 * @throws SimuException 
	 * @throws IOException 
	 */
	private void plotTransitData ( Writer out ) throws SimuException, IOException {
		
		ArrayList<TransitData> transits = super.getTransits();
		
		if ( transits == null || transits.size() == 0 ) {
			logger.error ( "Set has no data. Plotting aborted!" );
			return;
		}
		
		StellarAstrometry astroRef = super.getReferenceAstrometry(); 
		
		if ( astroRef == null ) {
			logger.error ( "No reference astrometry provided. Plotting aborted!" );
			return;
		}
		
		for ( TransitData tr: transits ) {
			
			// do NOT take into account relativistic effects, GaiaSimu algorithm does NOT work as it is supposed to
			boolean relativity = false;
			
			// Get observational data of transit
			GaiaSimuTime time = tr.getTime();
			double scanAngle = tr.getScanAngle();
			GVector2d lsc = tr.getLSC();
			GVector2d lscError = tr.getLSCError();
			
			// Convert to LPC
			GVector2d lpc = tr.getLPC();
			GVector2d lpcError = tr.getLPCError();
			
			// Calculate reference position
			GVector2d lpcRef = Conversion.fromCoMRStoLPC ( astroRef.getCoMRSPosition ( time, relativity ), astroRef.getAlpha(), astroRef.getDelta() );
			GVector2d lscRef = Conversion.fromLPCtoLSC(lpcRef, scanAngle);
			
			String outString = String.format("%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",
					lpc.getX(), lpc.getY(),
					lpcError.getX(), lpcError.getY(),
					lpcRef.getX(), lpcRef.getY(),
					lsc.getX(), lsc.getY(),
					lscError.getX(), lscError.getY(),
					lscRef.getX(), lscRef.getY(),
					scanAngle, time.getJD());
			
			out.write(outString);
			
		}
		
	}
	
	
	/**
	 * Plots transit data to file.
	 * 
	 * @param fname	File name.
	 */
	public void plotTransitData ( String fname ) {
		
		File file = new File(fname);
		
		try {
			
			logger.debug ( "Plotting transit data to " + fname + "." );
			
			FileWriter filewriter = new FileWriter(file, false);
			BufferedWriter buffwriter = new BufferedWriter(filewriter);
			plotTransitData ( buffwriter );
			buffwriter.close();
			
		} catch (FileNotFoundException e) {
			
			logger.error ( "File not found: "+fname+" \n\t --> drawing of trajectory failed!)" );
			e.printStackTrace();
			
		} catch (IOException e) {
			
			logger.error ( "File IO error: "+fname+" \n\t --> drawing of trajectory failed!)" );
			e.printStackTrace();
			
		} catch (SimuException e) {
			
			logger.error ( "SimuException\n\t --> drawing of transit data failed!)" );
			e.printStackTrace();
			
		}
		
	}

}
