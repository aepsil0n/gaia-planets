package newExamples;

public class DatabaseMultiFit {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Set up simulation
		Simulation sim = new Simulation();
		sim.setBasepath("/home/ebopp/workspace/multifit/active/");
		
		// Load systems
		sim.loadSystemsFromFile("/home/ebopp/workspace/exobtain/queries/single");

		// Inclination angles
		final int NFITS = 1000;
		final int NINCLS = 10;
		final double PI2 = Math.PI/2;
		final double[] INCS = new double[NINCLS];
		for(int i = 0; i < INCS.length; i++)
			INCS[i] = PI2*(double)(i+1)/(double)(NINCLS);
		
		// Fit systems
		sim.fitSystems(NFITS, INCS);
		
	}

}
