package newExamples;

import gaiasimu.SimuException;
import gaiasimu.gaia.Gaia;

import java.util.ArrayList;
import java.util.Iterator;

public class GaiaWrapper implements Logging {

	/** List of Gaia instances */
	private ArrayList<Gaia> gaiaInstances;
	
	/** Running iterator giving out instances */
	Iterator<Gaia> gaiaIterator;
	
	/**
	 * Constructor instantiating as many Gaias as specified.
	 * 
	 * @param NGaias	Number of Gaia instances to be instantiated
	 */
	public GaiaWrapper(int NGaias) {
		
		// Create array
		gaiaInstances = new ArrayList<Gaia>();
		
		// Instantiate Gaia as often as specified by parameter
		for(int i = 0; i < NGaias; i++) {
			try {
				gaiaInstances.add(new Gaia());
			} catch (SimuException e) {
				logger.error("Failed to instantiate Gaia!");
				e.printStackTrace();
			}
		}
		
		// Initialize iterator
		gaiaIterator = gaiaInstances.iterator();
		
	}

	/**
	 * Returns one of the Gaia instances maintained by this object.
	 * 
	 * @return A Gaia instance
	 */
	public synchronized Gaia getInstance() {
		
		if(!gaiaIterator.hasNext())
			gaiaIterator = gaiaInstances.iterator();
		
		return (Gaia) gaiaIterator.next();
		
	}
	
	
}
