package newExamples;

public class AveragedMultiFit {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Set up simulation
		Simulation sim = new Simulation();
		sim.setBasepath("/home/eduard/desktop/testdata/");
		sim.setRandGeo(true);
		sim.setFaintCutOff(false);
		sim.setSubCatalogue(20.0);
		
		// Load systems
		sim.loadSystemsFromFile("/home/eduard/dev/science/esac" +
				"/workspace/exobtain/queries/real-1muas");
		
		// Inclination angles
		final int NFITS = 100;
		final int NINCLS = 9;
		final double PI2 = Math.PI/2;
		final double START = PI2/9.0;
		final double END = PI2;
		final double[] INCS = new double[NINCLS];
		for(int i = 0; i < INCS.length; i++)
			INCS[i] = (START+(END-START)*(double)i/(double)(NINCLS-1));
		
		// Fit systems
		sim.fitSystems(NFITS, INCS);
		
	}

}
