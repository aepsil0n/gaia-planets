package newExamples;

import gaiasimu.universe.source.AstrometricParam;
import gaiasimu.universe.source.stellar.StellarAstrometry;

public class AstrometryTemplate {

	/** Right ascension [deg] */
	private double alpha;

	/** Declination [deg] */
	private double delta;
	
	/** Parallax [mas] */
	private double parallax;

	/** Right ascension proper motion, true arc [mas/yr] */
	private double muAlphaStar;
	
	/** Declination proper motion [mas/yr] */
	private double muDelta;
	
	/** Radial velocity [km/s] */
	private double radVel;
	

	/**
	 * Basic constructor.
	 * 
	 * @param alpha			Right ascension [deg]
	 * @param delta			Declination [deg]
	 * @param parallax		Parallax [mas]
	 * @param muAlphaStar	Right ascension proper motion, true arc [mas/yr]
	 * @param muDelta		Declination proper motion [mas/yr]
	 * @param radVel		Radial velocity [km/s]
	 */
	public AstrometryTemplate ( double alpha, double delta, double parallax, double muAlphaStar, double muDelta, double radVel ) {
		
		// Set parameters
		this.alpha = alpha;
		this.delta = delta;
		this.parallax = parallax;
		this.muAlphaStar = muAlphaStar;
		this.muDelta = muDelta;
		this.radVel = radVel;
		
	}
	
	
	/**
	 * Clones the astrometry template.
	 * 
	 * @return	Clone of astrometry template
	 */
	@Override
	public AstrometryTemplate clone () {
		return new AstrometryTemplate(alpha, delta, parallax, muAlphaStar, muDelta, radVel);
	}
	
	
	/**
	 * Creates an ideal stellar astrometry object from the exact parameters.
	 * 
	 * @return	The stellar astrometry
	 */
	public StellarAstrometry createAstrometry() {
		
		return new StellarAstrometry ( new AstrometricParam ( alpha, delta, parallax, muAlphaStar, muDelta, radVel ) );
		
	}

	// Setters and getters
	// TODO: documentation
	
	public double getAlpha() {
		return alpha;
	}


	public double getDelta() {
		return delta;
	}


	public double getParallax() {
		return parallax;
	}


	public double getMuAlphaStar() {
		return muAlphaStar;
	}


	public double getMuDelta() {
		return muDelta;
	}


	public double getRadVel() {
		return radVel;
	}


	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}


	public void setDelta(double delta) {
		this.delta = delta;
	}


	public void setParallax(double parallax) {
		this.parallax = parallax;
	}


	public void setMuAlphaStar(double muAlphaStar) {
		this.muAlphaStar = muAlphaStar;
	}


	public void setMuDelta(double muDelta) {
		this.muDelta = muDelta;
	}


	public void setRadVel(double radVel) {
		this.radVel = radVel;
	}
	
}
