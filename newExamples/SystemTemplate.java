package newExamples;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.Star;
import gaiasimu.universe.source.stellar.StarSystemSimuException;
import gaiasimu.universe.source.stellar.StellarAstrometry;

import java.util.ArrayList;
import java.util.Random;

public class SystemTemplate implements Logging {

	/** Astrometry template */
	private AstrometryTemplate astTemp;
	
	/** Star template */
	private StarTemplate stTemp;
	
	/** List of planetary templates */
	private ArrayList<PlanetTemplate> plTemps;
	
	
	/**
	 * Basic constructor.
	 */
	public SystemTemplate() {
		
		plTemps = new ArrayList<PlanetTemplate>();
		
	}
	
	
	/**
	 * Clones this system template.
	 * 
	 * @return A clone of this system template
	 */
	@Override
	public SystemTemplate clone() {
		
		// Copy everything
		SystemTemplate theClone = new SystemTemplate();
		theClone.provide(astTemp.clone());
		theClone.provide(stTemp.clone());
		for(PlanetTemplate plt: plTemps)
			theClone.provide(plt.clone());
		
		// Return it
		return theClone;
		
	}


	/**
	 * Creates a model out of the set of templates. The model is already finalized.
	 * 
	 * @param sim			The simulation to which the model should belong
	 * @param makeErrors	Add random systematic errors
	 * @param randIncl		Randomize the inclination
	 * @param randGeo		Randomize the geometry; only has effect if systematic errors are introduced
	 * @return	The created model
	 */
	public Model createModel(Simulation sim, boolean makeErrors, boolean randIncl, boolean randGeo) {
		
		// Check for relevant templates
		if(astTemp == null) {
			logger.log("No astrometry template provided! Model not created.");
			return null;
		}
		
		if(stTemp == null) {
			logger.log("No star template provided! Model not created.");
			return null;
		}
		
		if(plTemps.size() == 0) {
			logger.log("No planet template provided! Model not created.");
			return null;
		}
		
		// Create clean reference astrometry
		StellarAstrometry refAstro = null;
		refAstro = astTemp.createAstrometry();
		AstrometryTemplate astTempErr = astTemp.clone();

		// Introduce systematic errors if requested
		if(makeErrors) {
			// Random setup
			ErrorModel errorModel = new ErrorModel(stTemp.getMagMv(), stTemp.getVMinusI(), sim.isFaintCutOff());
			Random random = new Random();
			
			// Randomize astrometry
			double SUBCORR = 1.0; 
			if (sim.getSubCatalogue() < 5.0)
				SUBCORR = Math.sqrt(5.0/sim.getSubCatalogue());
			astTempErr.setAlpha(astTempErr.getAlpha() + SUBCORR*random.nextGaussian() * errorModel.alphaStarError() / Math.cos(astTempErr.getDelta()));
			astTempErr.setDelta(astTempErr.getDelta() + SUBCORR*random.nextGaussian() * errorModel.deltaError());
			astTempErr.setMuAlphaStar(astTempErr.getMuAlphaStar() + SUBCORR*random.nextGaussian() * errorModel.muAlphaStarError());
			astTempErr.setMuDelta(astTempErr.getMuDelta() + SUBCORR*random.nextGaussian() * errorModel.muDeltaError());
			astTempErr.setParallax(astTempErr.getParallax() + SUBCORR*random.nextGaussian() * errorModel.parallaxError());
			
			// Randomize orbital geometry
			if(randGeo) {
				plTemps.get(0).setNodeAngle(random.nextDouble()*2.0*Math.PI);
				plTemps.get(0).setOmega2(random.nextDouble()*2.0*Math.PI);
				plTemps.get(0).setTimePeriastron(random.nextDouble()*plTemps.get(0).getPeriod());
			}
		}
		
		// Create model from templates
		Model model = new Model ( sim );
		
		StellarAstrometry astro = astTempErr.createAstrometry();
		model.setAstrometry ( astro );
		model.setRefAstro ( refAstro );
		Star star = stTemp.createStar ( astro );
		model.setStar ( star );
		for ( PlanetTemplate plTemp: plTemps )
		{
			PlanetTemplate plTempNew = plTemp.clone();
			if ( randIncl )
				plTempNew.randomizeInclination();
			model.addPlanet ( plTempNew.createPlanet(astro, star, model.getPlanets().size() ) );
		}
		
		// Finalize the model system
		try {
			model.finalizeSystem();
		} catch (StarSystemSimuException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SimuException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return model;
		
	}
	
	
	/**
	 * Providing method for star template
	 * 
	 * @param stTemp	Star template
	 */
	public void provide ( StarTemplate stTemp ) {
		this.stTemp = stTemp;
	}
	
	
	/**
	 * Providing method for star template
	 * 
	 * @param stTemp	Star template
	 */
	public void provide ( AstrometryTemplate astTemp ) {
		this.astTemp = astTemp;
	}
	
	
	/**
	 * Providing method for star template
	 * 
	 * @param stTemp	Star template
	 */
	public void provide ( PlanetTemplate plTemp ) {
		this.plTemps.add(plTemp);
	}


	/**
	 * Getter for astrometry template.
	 * 
	 * @return	Astrometry template
	 */
	public AstrometryTemplate getAstrometryTemplate() {
		return astTemp;
	}


	/**
	 * Getter for star template.
	 * 
	 * @return	Star template
	 */
	public StarTemplate getStarTemplate() {
		return stTemp;
	}


	/**
	 * Getter for planet template list.
	 * 
	 * @return	List of planet templates
	 */
	public ArrayList<PlanetTemplate> getPlanetTemplates() {
		return plTemps;
	}


	/**
	 * Sets inclination of primary planet to a new value.
	 * NOTE: Only use with lock on this system template, when multi-threading!
	 * TODO: do this less dirty...
	 * 
	 * @param inc	New inclination of primary planet.
	 */
	public void setPrimaryInclination(double inc) {
		
		plTemps.get(0).setInclination(inc);
		
	}
	
}
