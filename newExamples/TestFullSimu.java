package newExamples;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StarSystemSimuException;

import java.io.IOException;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

/**
 * Test class invoking full simulation of data generation for several test cases.
 * The output is used in page 6 of report 01 to document capabilities of data generation algorithms.
 * 
 * TODO: Rewrite to match new output system.
 */
public class TestFullSimu {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException
	 * @throws StarSystemSimuException 
	 */
	public static void main(String[] args) throws SimuException, 
			IOException, GaiaException, FunctionEvaluationException,
			IllegalArgumentException, OptimizationException, StarSystemSimuException {
		
		// Initiate random default exoplanet orbital parameters not to be varied
		double eccentricity   = 0.3;		// Eccentricity of orbit
		double inclination    = .75*Math.PI;	// Inclination angle (rad)
		double nodeAngle      = 0.25;		// Position angle of the line of nodes (rad)
		double omega2         = 0.44;		// Argument of periastron (rad)
		double timePeriastron = 0.0;		// Time of periastron (days)
		
		// Star data -- corresponds to a sun-like star at 5 pc distance
		double magMv    = 4.83;			// Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
		double vMinusI  = Conversion.colorBVtoVI(0.656);	// Intrinsec Mean (V-I) colour. 51 Peg: +0.8
		double absV     = 0.0;				// Interstellar absortion in the V band
		double ra       = 26.0;				// RA  ICRS (deg).
		double dec      = -84.3;			// DEC ICRS (deg).
		double distance = 5.0;				// Distance from Sol [pc]
		double parallax = 1e3/distance;		// Parallax [mas]
		double muRa     = 3.0;				// Proper motion RA  [mas/yr]
		double muDec    = -2.5;				// Proper motion DEC [mas/yr]
		double vRad     = -1.3;				// Radial velocity (km/s)
		double feH      = 0.05;				// [Fe/H]
		double alphaE   = 0.0;				// Alpha elements enhancement
		double mass     = 1.0;				// (Msun?)
		double magBol   = Conversion.colorBVtoMagBol(0.656);	// (Compute from bolometric corrections in Allen)
		int    pop      = 6;				// (Besancon model. 6: 5-7 Gyr)
		String spType   = "M4 V";			// String defining the spectral type
		
		// Generate simulation
		Simulation sim = new Simulation();
		sim.setBasepath ( "/home/ebopp/workspace/full-simu/data/" );
		
		// Setup sets of masses and periods
		double[] masses	= new double[] { 0.5, 2.0, 0.5, 2.0 };
		double[] periods = new double[] { 5e2, 5e2, 5e3, 5e3 };
		String[] names = new String[] { "alpha", "beta", "gamma", "delta" };
		
		// Run loop of data generations
		for ( int i = 0; i < masses.length; i++ ) {
			
			// Generate solar system, set parameters, generate planet and star; finalize
			SimulatedSystem system = sim.addSystem ( names[i] );
			
			// Create template of system
			SystemTemplate sysTemp = new SystemTemplate();
			sysTemp.provide ( new AstrometryTemplate ( ra, dec, parallax, muRa, muDec, vRad ) );
			sysTemp.provide ( new StarTemplate ( magMv, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop ) );
			sysTemp.provide ( new PlanetTemplate ( masses[i], periods[i], timePeriastron, eccentricity, inclination, omega2, nodeAngle ) );
			
			// Create observation model from template
			Model obsModel = sysTemp.createModel ( sim, true, false );
			system.setObservationModel ( obsModel );
			
			// Create system
			SimulatedSystem system = sim.addSystem("GJ 876");
			system.setObservationModel(obsModel);
			
			// Plot without any errors
			system.generateData ( 20.0, true, false, false );
			system.plotTrajectory ( "/home/ebopp/workspace/full-simu/data/"+names[i]+"-ellipse.out" );
			system.plotTransitData ( "/home/ebopp/workspace/full-simu/data/"+names[i]+"-noerror.out" );
			
			// Plot with both systematic and statistic errors
			system.generateData ( 20.0, true, true, true );
			system.plotTransitData ( "/home/ebopp/workspace/full-simu/data/"+names[i]+"-complete.out" );
			
		}
		
	}
	
}
