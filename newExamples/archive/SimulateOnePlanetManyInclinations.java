package newExamples.archive;

import gaia.cu1.mdb.cu3.localplanecoords.dm.FovTransitInfo;
import gaia.cu1.mdb.cu3.localplanecoords.dm.LpcCentroid;
import gaia.cu1.tools.dal.gbin.GbinReaderV2;
import gaia.cu1.tools.dm.GaiaRoot;
import gaia.cu1.tools.exception.GaiaException;
import gaiasimu.SimuException;
import gaiasimu.gaia.payload.psf.CachePSFManager;
import gaiasimu.universe.outputfile.WriteMDBICD;
import gaiasimu.universe.source.AstrometricParam;
import gaiasimu.universe.source.Photometry;
import gaiasimu.universe.source.stellar.OrbitalParams;
import gaiasimu.universe.source.stellar.SpectralType;
import gaiasimu.universe.source.stellar.StarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarAstrometry;
import gaiasimu.universe.source.stellar.StellarSource;
import gaiasimu.utils.cache.CacheManager;
import gog.GogGlobals;
import gog.GogRun;
import gog.configuration.GogConfigurationMgr;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import newExamples.gaia.cu4.du438.current_cycle.DmsSimuDU437;

/**
 * The Class Test.
 */
public class SimulateOnePlanetManyInclinations {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 */
	public static void main(String[] args) throws SimuException, IOException, GaiaException{
		
		// GOG configuration file
		String[] gogConfig = { "newDataInput/gogconfig.xml" };
		

		// Output UMSellar.gbin directory
		String   umStellarFileDir = "newGogTemp/";
		String   umStellarFile    = umStellarFileDir + "UMStellar.gbin";
		String   gogOutputDir     = "newDataOutput/";
		String[] gogOutputFiles   = {
				gogOutputDir + "gog7beta_combinedAstrometric.gbin",
				gogOutputDir + "gog7beta_combinedAstroPhysical.gbin",
				gogOutputDir + "gog7beta_combinedPhotometric.gbin",
				gogOutputDir + "gog7beta_combinedRVS.gbin",
				gogOutputDir + "gog7beta_epochAstrometric.gbin",
				gogOutputDir + "gog7beta_epochPhotometric.gbin",
				gogOutputDir + "gog7beta_epochRVS.gbin",
				gogOutputDir + "gog7beta_sourceRVS.gbin" };
		
		String GOG_EPOCH_ASTROMETRY_FILE = gogOutputDir + "gog7beta_epochAstrometric.gbin";
		
		// Output text file
		String outputFileName = gogOutputDir + "simulateOnePlanetManyInclinations.txt";
		
		// Exoplanet orbital parameters
		double eccentricity   = 0.1;
		double inclinationMax = 1 * Math.PI / 2;  // (rad)
		int    nInclination   = 90;   // Number of inclination angles up to inclinationMax
		double nodeAngle      = 0.4;  // position angle of the line of nodes (rad)
		double omega2         = 0.1;  // argument of periastron (rad)
		double period         = 365.25;  // (days)
		double semiMajorAxis  = 0.2;  // semi-maj. axis (A.U.) of the secondary (a2)
		double timePeriastron = 1.0;  // (days)
		//double massPlanet     =  50;  // (Jupiter masses)
		double massPlanet     = 5;   // (Jupiter masses)
		
		// Generate orbital parameters
		/*
		OrbitalParams orbitalParams = new OrbitalParams(
				period,
				timePeriastron,
				semiMajorAxis,
				eccentricity,
				omega2,
				inclination,
				nodeAngle);
		*/
		
		// Star data
		long   idRoot   = 1223345453L;
		double magMv    = 12.0;         // Absolute magnitude
		double vMinusI  = 2.0;         // Intrinsec Mean (V-I) colour
		double absV     = 0.1;         // Interstellar absortion in the V band
		double ra       = 344.3665854; // RA  ICRS (deg)
		double dec      = 20.76883222; // DEC ICRS (deg)
		//double parallax = 65.10;       // (mas)
		//double muRa     = 208.07;      // Proper motion RA  (mas/yr)
		//double muDec    = 60.96;       // Proper motion DEC (mas/yr)
		double parallax = 25;       // (mas)
		double distance = 1000 / parallax;// (pc)
		double muRa     = 0;      // Proper motion RA  (mas/yr)
		double muDec    = 0;       // Proper motion DEC (mas/yr)
		double vRad     = -31.2;       // Radial velocity (km/s)
		double feH      = 0.1;         // [Fe/H]
		double alphaE   = 0.0;         // Alpha elements enhancement
		double mass     = 1.0;         // (Msun?)
		double magBol   = 4.3;         // (Compute from bolometric corrections in Allen)
		int    pop      = 6;           // (Besancon model. 6: 5-7 Gyr)
		String spType   = "B5IV";      // String defining the spectral type
		double teff     =  5500;        // (K?)
		double logg     = 4.4;         // log g (cgs)
		double radius   = 2.0;         // (Rsun?)
		
		// Open output file
		PrintWriter outputFile = new PrintWriter(new FileWriter( outputFileName )); 
		
		// Generate spectral type
		SpectralType spectralType = new SpectralType( spType );
		
		// Generate photometry
		Photometry photometry = new Photometry(magMv, distance, vMinusI, absV);
		
		// Generate astrometry
		StellarAstrometry stellarAstrometry = new StellarAstrometry(
				new AstrometricParam(ra, dec, parallax, muRa, muDec, vRad));
		
		// Generate physics.
		// Basic constructor for StarSystem
		StarPhysicalParameters starPhysicalParameters =
//			new StarPhysicalParameters( teff, feH, logg, mass, spectralType, magBol, radius, pop);
			new StarPhysicalParameters(mass, spectralType, magBol, pop, feH, alphaE);
		
		// Generate star
		gaiasimu.universe.source.stellar.Star star =
			new gaiasimu.universe.source.stellar.Star(
					idRoot, photometry, stellarAstrometry, starPhysicalParameters);
		
		// Generate object list
		ArrayList<gaiasimu.universe.source.AstroSource> objList = new ArrayList<gaiasimu.universe.source.AstroSource>() ; // simulated data
		
		// Loop on inclinations
		for ( int i = 0; i < nInclination; i++ ){
			
			double inclination = inclinationMax * i / nInclination;
			System.out.println(inclination);
			
			// DU437 simulator
			DmsSimuDU437 dmsSimuDU437 = new DmsSimuDU437();
			OrbitalParams orbitalParams2 = dmsSimuDU437.generateOrbitalParams(
					star, massPlanet, period, timePeriastron, eccentricity, omega2, nodeAngle, inclination);
				
			StellarSource systemOnePlanet = dmsSimuDU437.generateSystem(star, orbitalParams2);
			
			// Write sources to UMStellar file
			objList.add( systemOnePlanet );
			
		}
		
		// Erase old UMStellar file, if available, and write the new one
		( new File ( umStellarFile ) ).delete();
		WriteMDBICD writer = new WriteMDBICD( umStellarFileDir );
		writer.launchWrite( objList, false );
		
		
		// Erase ould GOG output files, if available
		for (String gogOutputFile : gogOutputFiles){
			( new File ( gogOutputFile ) ).delete();			
		};
		
		
		
		// Run GOG
		//GogRun.main( gogConfig );
		GogConfigurationMgr.getConfig( gogConfig[0], Long.toString(System.currentTimeMillis()));
        CachePSFManager.setPsfDir(GogGlobals.getPsfPath());
        CacheManager.getInstance("gaiasimu-cache.xml");
        GogRun gr = new GogRun();
        gr.start();

		
        
		// Read the first elements from epoch astrometry GOG file
		ArrayList<GaiaRoot> epochAstrometryArrayList = new ArrayList<GaiaRoot>();		
		GbinReaderV2 epochAstrometryReader = new GbinReaderV2 ( GOG_EPOCH_ASTROMETRY_FILE );		
		epochAstrometryReader.readAllToList( epochAstrometryArrayList );
		epochAstrometryReader.close();
		
		for (int i = 0; i < nInclination; i++){
			
			double inclination = inclinationMax * i / nInclination;
			
			LpcCentroid epochAstrometry = (LpcCentroid) epochAstrometryArrayList.get( i * 3 );
			FovTransitInfo[] transitInfo = epochAstrometry.getTransits();
			
			System.out.printf("Inclination %10.5f:", inclination);
			outputFile.printf("Inclination %10.5f:", inclination);
			
			for (int j = 0; j < transitInfo.length; j++){
				
				System.out.printf(" %20.12e %20.12e",
						transitInfo[j].getCentroidPosAl(), transitInfo[j].getCentroidPosAc());
				outputFile.printf(" %20.12e %20.12e",
						transitInfo[j].getCentroidPosAl(), transitInfo[j].getCentroidPosAc());
				
			}
			
			System.out.printf("\n");
			outputFile.printf("\n");
			
		}
		
		
		// Close output file
		outputFile.close();
		
	}
	
	    
		
}
	
	
