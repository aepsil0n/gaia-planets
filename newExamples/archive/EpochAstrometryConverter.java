package newExamples.archive;

import gaia.cu1.mdb.cu2.um.dm.UMAstroRoot;
import gaia.cu1.mdb.cu2.um.dm.UMPhotoRoot;
import gaia.cu1.mdb.cu2.um.umtypes.dm.UMStellarSource;
import gaia.cu1.mdb.cu3.agis.dm.Source;
import gaia.cu1.mdb.cu3.localplanecoords.dm.FovTransitInfo;
import gaia.cu1.mdb.cu3.localplanecoords.dm.LpcCentroid;
import gaia.cu1.tools.dal.gbin.GbinReaderV2;
import gaia.cu1.tools.dm.GaiaRoot;
import gaia.cu1.tools.exception.GaiaException;
import gaiasimu.SimuException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class EpochAstrometryConverter {
	
	public static void main(String[] args) throws GaiaException, SimuException, IOException {
		
		// Input Gbin file
		final String INPUT_PATH               = "./newDataInput/";
		final String GBIN_EPOCH_ASTROMETRY    = "./newDataOutput/" + "gog7beta_epochAstrometric.gbin"; // "gog7beta_simu8944_epochAstrometric.gbin";
		
		// Output text file
		final String OUTPUT_FILE              = "./newDataOutput/epochAstrometryConverter.txt";
		PrintWriter outputFile             = new PrintWriter( new FileWriter( OUTPUT_FILE ) );
		
				
		// Read epoch astrometry GBin file
		
		// Parse data to ArrayList		
		ArrayList<GaiaRoot> epochAstrometryArrayList = new ArrayList<GaiaRoot>();		
		GbinReaderV2 epochAstrometry = new GbinReaderV2 (GBIN_EPOCH_ASTROMETRY);		
		epochAstrometry.readAllToList( epochAstrometryArrayList );
		epochAstrometry.close();
		
		System.out.printf("\n" + "Epoch astrometry\n");
		outputFile.printf("\n" + "Epoch astrometry\n");
		// Combined astrometry data: terminal output
		for (GaiaRoot element : epochAstrometryArrayList){
			
			// Parse object into Source
			LpcCentroid epochAstrometryData = (LpcCentroid)element;
			
			// Parse source data
			long   sourceId   = epochAstrometryData.getSourceId();
			int    nTransits  = epochAstrometryData.getNTransits();
			double alpha0     = epochAstrometryData.getAlpha0();
			double delta0     = epochAstrometryData.getDelta0();
			FovTransitInfo[] transitInfo =  epochAstrometryData.getTransits();
			
			// Terminal output: basic data
			System.out.printf("Source ID: %20d; N transits: %4d; alpha ref: %15.5e; delta ref: %15.5e\n",
					sourceId, nTransits, alpha0, delta0);
			outputFile.printf("Source ID: %20d; N transits: %4d; alpha ref: %15.5e; delta ref: %15.5e\n",
					sourceId, nTransits, alpha0, delta0);
			
			// Terminal output: transit info
			for (FovTransitInfo fovTransitInfo : transitInfo){
				long    transitId          = fovTransitInfo.getTransitId();
				long    obsTime            = fovTransitInfo.getObsTime();
				double  centroidPosAl      = fovTransitInfo.getCentroidPosAl();
				float   centroidPosErrorAl = fovTransitInfo.getCentroidPosErrorAl();
				double  centroidPosAc      = fovTransitInfo.getCentroidPosAc();
				float   centroidPosErrorAc = fovTransitInfo.getCentroidPosErrorAc();
				double  scanPosAngle       = fovTransitInfo.getScanPosAngle();
				float   varPiFactorAl      = fovTransitInfo.getVarPiFactorAl();
				float   varPiFactorAc      = fovTransitInfo.getVarPiFactorAc();
				short   nCcdTrans          = fovTransitInfo.getNCcdTrans(); // Does not seem useful: equals zero
				// short[] strips             = transitInfo[0].getStrips();    // Does not seem useful: seems empty
				System.out.printf("Transit ID: %4d; Obs. time: %20d;" +
						" centroidPosAl: %15.5e +- %15.5e;" +
						" centroidPosAc: %15.5e +- %15.5e;" +
						" scanPosAngle: %15.5e; N CCD transit: %4d;" +
						" parallax factor AL: %15.5e; parallax factor AC: %15.5e; \n",
						transitId, obsTime,
						centroidPosAl, centroidPosErrorAl,
						centroidPosAc, centroidPosErrorAc,
						scanPosAngle, nCcdTrans,
						varPiFactorAl, varPiFactorAc);
				outputFile.printf("Transit ID: %4d; Obs. time: %20d;" +
						" centroidPosAl: %15.5e +- %15.5e;" +
						" centroidPosAc: %15.5e +- %15.5e;" +
						" scanPosAngle: %15.5e; N CCD transit: %4d;" +
						" parallax factor AL: %15.5e; parallax factor AC: %15.5e; \n",
						transitId, obsTime,
						centroidPosAl, centroidPosErrorAl,
						centroidPosAc, centroidPosErrorAc,
						scanPosAngle, nCcdTrans,
						varPiFactorAl, varPiFactorAc);
				
			};
			
			
		};
		
		// Close output file
		outputFile.close();
		
	}
	
	
}
