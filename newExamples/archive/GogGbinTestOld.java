package newExamples.archive;

import gaia.cu1.mdb.cu2.um.dm.UMAstroRoot;
import gaia.cu1.mdb.cu2.um.dm.UMPhotoRoot;
import gaia.cu1.mdb.cu2.um.umtypes.dm.UMStellarSource;
import gaia.cu1.mdb.cu3.agis.dm.Source;
import gaia.cu1.mdb.cu3.localplanecoords.dm.FovTransitInfo;
import gaia.cu1.mdb.cu3.localplanecoords.dm.LpcCentroid;
import gaia.cu1.params.GaiaParam;
import gaia.cu1.tools.dal.gbin.GbinReaderV2;
import gaia.cu1.tools.dm.GaiaRoot;
import gaia.cu1.tools.exception.GaiaException;
import gaia.cu1.tools.util.Image;
import gaia.cu4.du436.wp00300.model.body.Star;
import gaiasimu.SimuException;
import gaiasimu.gaia.payload.bam.BamFringeGeneratorImpl;
import gaiasimu.universe.generator.ExoPlanetGenerator;
import gaiasimu.universe.generator.milkywaygenerator.StarGenerator;
import gaiasimu.universe.generator.spacechopper.skyregion.SkyRegion;
import gaiasimu.universe.outputfile.UMGbin2AsciiAdapter;
import gaiasimu.universe.source.AstroSource;
import gaiasimu.universe.source.Photometry;
import gaiasimu.universe.source.stellar.OrbitalParams;
import gaiasimu.universe.source.stellar.SpectralType;
import gaiasimu.universe.source.stellar.StarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarAstrometry;
import gaiasimu.universe.source.stellar.StellarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarSource;

import java.util.ArrayList;
import java.util.Random;

import newExamples.gaia.cu4.du438.current_cycle.DmsSimuDU437;

public class GogGbinTestOld {
	
	public static void main(String[] args) throws GaiaException, SimuException {
		
		// Input Gbin file
		final String INPUT_PATH               = "./newDataInput/";
		final String GBIN_STELLAR             = INPUT_PATH + "UMStellar.gbin";
		final String GBIN_COMBINED_ASTROMETRY = INPUT_PATH + "gog7beta_simu8944_combinedAstrometric.gbin";
		final String GBIN_EPOCH_ASTROMETRY    = INPUT_PATH + "gog7beta_epochAstrometric.gbin"; // "gog7beta_simu8944_epochAstrometric.gbin";
		
		// Read metadata
		GbinReaderV2 reader = new GbinReaderV2 (GBIN_EPOCH_ASTROMETRY);
//System.out.printf( reader.getGbinMetaData().toString() );
		reader.close();
		
		
		
		// Read combined astrometry GBin file
		
		// Parse data to ArrayList		
		ArrayList<GaiaRoot> combinedAstrometryArrayList = new ArrayList<GaiaRoot>();		
		GbinReaderV2 combinedAstrometry = new GbinReaderV2 (GBIN_COMBINED_ASTROMETRY);		
		combinedAstrometry.readAllToList( combinedAstrometryArrayList );
		combinedAstrometry.close();
		
		// Combined astrometry data: terminal output
		for (GaiaRoot element : combinedAstrometryArrayList){
			
			// Parse object into Source
			Source combinedAstrometryData = (Source)element;
			
			// Parse source data
			long   sourceId   = combinedAstrometryData.getSourceId();
			int    nObs       = combinedAstrometryData.getNObs()[0];
			double alpha      = combinedAstrometryData.getAlpha();
			double alphaError = combinedAstrometryData.getAlphaError();
			double delta      = combinedAstrometryData.getDelta();
			double deltaError = combinedAstrometryData.getDeltaError();
			double varpi      = combinedAstrometryData.getVarpi();
			double varpiError = combinedAstrometryData.getVarpiError();
			System.out.printf( "SourceId:" + sourceId + "\n");
			
			// Terminal output
			System.out.printf("Source ID: %20d; N obs: %4d;" +
					" alpha: %15.5e +- %15.5e;" +
					" delta: %15.5e +- %15.5e;" +
					" parallax: %15.5e +- %15.5e\n",
					sourceId, nObs, alpha, alphaError, delta, deltaError, varpi, varpiError);
			
		};
		
		
		
		// Read epoch astrometry GBin file
		
		// Parse data to ArrayList		
		ArrayList<GaiaRoot> epochAstrometryArrayList = new ArrayList<GaiaRoot>();		
		GbinReaderV2 epochAstrometry = new GbinReaderV2 (GBIN_EPOCH_ASTROMETRY);		
		epochAstrometry.readAllToList( epochAstrometryArrayList );
		epochAstrometry.close();
		
		// Combined astrometry data: terminal output
		for (GaiaRoot element : epochAstrometryArrayList){
			
			// Parse object into Source
			LpcCentroid epochAstrometryData = (LpcCentroid)element;
			
			// Parse source data
			long   sourceId   = epochAstrometryData.getSourceId();
			int    nTransits  = epochAstrometryData.getNTransits();
			double alpha0     = epochAstrometryData.getAlpha0();
			double delta0     = epochAstrometryData.getDelta0();
			FovTransitInfo[] transitInfo =  epochAstrometryData.getTransits();
			
			// Terminal output: basic data
			System.out.printf("Source ID: %20d; N transits: %4d; alpha ref: %15.5e; delta ref: %15.5e\n",
					sourceId, nTransits, alpha0, delta0);
			
			// Terminal output: first transit info
			long    transitId          = transitInfo[0].getTransitId();
			long    obsTime            = transitInfo[0].getObsTime();
			double  centroidPosAl      = transitInfo[0].getCentroidPosAl();
			float   centroidPosErrorAl = transitInfo[0].getCentroidPosErrorAl();
			double  centroidPosAc      = transitInfo[0].getCentroidPosAc();
			float   centroidPosErrorAc = transitInfo[0].getCentroidPosErrorAc();
			double  scanPosAngle       = transitInfo[0].getScanPosAngle();
			float   varPiFactorAl      = transitInfo[0].getVarPiFactorAl();
			float   varPiFactorAc      = transitInfo[0].getVarPiFactorAc();
			short   nCcdTrans          = transitInfo[0].getNCcdTrans(); // Does not seem useful: equals zero
			// short[] strips             = transitInfo[0].getStrips();    // Does not seem useful: seems empty
			System.out.printf("Transit ID: %4d; Obs. time: %20d;" +
					" centroidPosAl: %15.5e +- %15.5e;" +
					" centroidPosAc: %15.5e +- %15.5e;" +
					" scanPosAngle: %15.5e; N CCD transit: %4d;" +
					" parallax factor AL: %15.5e; parallax factor AC: %15.5e; \n",
					transitId, obsTime,
					centroidPosAl, centroidPosErrorAl,
					centroidPosAc, centroidPosErrorAc,
					scanPosAngle, nCcdTrans,
					varPiFactorAl, varPiFactorAc);
			
			
		};
		
		// Exoplanet orbital parameters
		double eccentricity   = 0.1;
		double inclination    = 0.5;  // (rad)
		double nodeAngle      = 0.4;  // position angle of the node (rad)
		double omega2         = 0.1;  // argument of periastron (rad)
		double period         = 1.5;  // (days)
		double semiMajorAxis  = 0.2;  //  semi-maj. axis (A.U.) of the secondary (a2)
		double timePeriastron = 1.0;  // (days)
		double massPlanet     = 1.0;  // (Jupiter masses)
		
		// Generate orbital parameters
		OrbitalParams orbitalParams = new OrbitalParams(
				period,
				timePeriastron,
				semiMajorAxis,
				eccentricity,
				omega2,
				inclination,
				nodeAngle);
		
		
		// Star data
		long   idRoot   = 1223345453L;
		double magMv    = 8.0; // Absolute magnitude
		double distance = 100; // (pc?)
		double vMinusI  = 2.0; // Intrinsec Mean (V-I) colour
		double absV     = 0.1; // Interstellar absortion in the V band
		double x        = 10;  // Galactic coordinates and proper motions		
		double y        =  9;
		double z        =  8;
		double U        = 0.1;
		double V        = 0.2;		 
		double W        = 0.3;
		double teff     = 5500; // (K?)
		double feH      = 0.1;  // [Fe/H]
		double logg     = 4.4;  // log g (cgs)
		double mass     = 1.2;  // (Msun?)
		double magBol   = 4.3;  // (Compute from bolometric corrections)
		double radius   = 2.0;  // (Rsun?)
		int    pop      = 6;    // (Besancon model. 6: 5-7 Gyr)
				
		// Generate spectral type
		SpectralType spectralType = new SpectralType(teff, logg);
		
		// Generate photometry
		Photometry photometry = new Photometry(magMv, distance, vMinusI, absV);
		
		// Generate astrometry
		StellarAstrometry stellarAstrometry = new StellarAstrometry( x, y, z, U, V, W );
		
		// Generate physics.
		// Use of this constructur is discouraged, but it should be enough to generate photometry
		StarPhysicalParameters starPhysicalParameters =
			new StarPhysicalParameters( teff, feH, logg, mass, spectralType, magBol, radius, pop);
		
		// Generate star
		gaiasimu.universe.source.stellar.Star star =
			new gaiasimu.universe.source.stellar.Star(
					idRoot, photometry, stellarAstrometry, starPhysicalParameters);
		
		
		// Exoplanet generator
		ExoPlanetGenerator exoPlanetGenerator = new ExoPlanetGenerator();
		
		// Generate exoplanet
		// StellarSource exoplanet =  exoPlanetGenerator.generateSystem( star, orbitalParams );		
/*		Exception in thread "main" gaiasimu.SimuException: Mass of component 1 not initialized
		at gaiasimu.universe.source.stellar.StarSystem.computeMass(StarSystem.java:171)
		at gaiasimu.universe.source.stellar.StarSystem.setPhysicalParameters(StarSystem.java:153)
		at gaiasimu.universe.source.stellar.StarSystem.<init>(StarSystem.java:115)
		at gaiasimu.universe.generator.ExoPlanetGenerator.generateSystem(ExoPlanetGenerator.java:174)
		at newExamples.GogGbinTest.main(GogGbinTest.java:232)
*/		
		
		// DU437 simulator
		DmsSimuDU437 dmsSimuDU437 = new DmsSimuDU437();
		OrbitalParams orbitalParams2 = dmsSimuDU437.generateOrbitalParams(
				star, massPlanet, period, timePeriastron, eccentricity, omega2, nodeAngle, inclination);
			
		StellarSource systemRandom    = dmsSimuDU437.generate(star, new Random());
		StellarSource systemOnePlanet = dmsSimuDU437.generateSystem(star, orbitalParams2);
		
		// Conversion: GBin to ASCII: nothing really happens
		String[] arguments = {GBIN_STELLAR, "newDataOutput/erase.txt", "newDataOutput/erase2.txt"};
		UMGbin2AsciiAdapter.main( arguments );
		
		// BAM fringe generator
		BamFringeGeneratorImpl bamFringeGenerator = new BamFringeGeneratorImpl();
		Image bamImage;
		bamImage = bamFringeGenerator.getFringe(0);
		bamImage.saveToFits("newDataOutput/bamImageRef.fits");
		bamImage = bamFringeGenerator.getFringe(286e-9);
		bamImage.saveToFits("newDataOutput/bamImage1Pix.fits");
	}
	
	
}
