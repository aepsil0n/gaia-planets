package newExamples.archive;

import gaia.cu1.params.GaiaParam;
import gaia.cu1.params.MissingParams;
import gaiasimu.GaiaSimuTime;
import gaiasimu.GaiaSimuEnums.SimuTimeScale;
import gaiasimu.gaia.payload.psf.CachePSFManager;
import gaiasimu.utils.cache.CacheManager;
import gog.GogConstants;
import gog.GogGlobals;
import gog.GogRun;
import gog.configuration.GogConfigurationMgr;
import gog.exception.GogException;

public class Erase {
	
	public static void main( String[] args ) throws GogException{
		
		GaiaSimuTime x = GogConstants.UM_INITIAL_TIME;
		double t = x.getElapsedNanoSecs();
		System.out.printf("%20.12e\n", t);
		System.out.printf(x.asIsoDate() + "\n");
		
		double yearsSubCatalog = 1.5;
		double missionStart = MissingParams.MISSION_START * GaiaParam.Nature.JULIANYEAR_DAY	* GaiaParam.Nature.DAY_SECOND * 1e9;
		double timeSpan = yearsSubCatalog * GaiaParam.Nature.JULIANYEAR_DAY	* GaiaParam.Nature.DAY_SECOND * 1e9;
		System.out.printf("%20.10e %20.10e\n", missionStart, missionStart + timeSpan);
		
		
	}	
	
}
