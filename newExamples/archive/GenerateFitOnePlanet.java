package newExamples.archive;

import gaia.cu1.tools.exception.GaiaException;

import gaiasimu.SimuException;
import gaiasimu.universe.source.stellar.StarSystemSimuException;

import java.io.IOException;

import newExamples.SimulatedSystem;
import newExamples.Simulation;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

/**
 * The Class Test.
 */
public class GenerateFitOnePlanet {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException
	 * @throws StarSystemSimuException 
	 */
	public static void main(String[] args) throws SimuException, 
			IOException, GaiaException, FunctionEvaluationException,
			IllegalArgumentException, OptimizationException, StarSystemSimuException {
		
		// Exoplanet orbital parameters
		double eccentricity   = 0.0;		// Eccentricity of orbit
		double inclination    = Math.PI/4;	// Inclination angle (rad)
		double nodeAngle      = 0.0;		// Position angle of the line of nodes (rad)
		double omega2         = 0.0;		// Argument of periastron (rad)
		double period         = 100.0;		// Orbital period (days)
		double timePeriastron = 0.0;		// Time of periastron (days)
		double massPlanet     = 1.0;		// Mass of planet (Jupiter masses)
		
		// Star data
		double magMv    = 20.17;			// Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
		double vMinusI  = 1.52;				// Intrinsec Mean (V-I) colour. 51 Peg: +0.8
		double absV     = 0.0;				// Interstellar absortion in the V band
		double ra       = 343.31972458;		// RA  ICRS (deg). 51 Peg: 344.3665854
		double dec      = -14.263700556;	// DEC ICRS (deg). 51 Peg: 20.76883222
		double parallax = 1e3/4.7;			// (mas). 51 Peg: 64.07 mas
		double distance = 4.7;				// Distance from Sol (pc)
		double muRa     = 0;				// Proper motion RA  (mas/yr)
		double muDec    = 0;				// Proper motion DEC (mas/yr)
		double vRad     = -1.7;				// Radial velocity (km/s)
		double feH      = 0.05;				// [Fe/H]
		double alphaE   = 0.0;				// Alpha elements enhancement
		double mass     = 0.33;				// (Msun?)
		double magBol   = 12.1;				// (Compute from bolometric corrections in Allen)
		int    pop      = 6;				// (Besancon model. 6: 5-7 Gyr)
		String spType   = "M4 V";			// String defining the spectral type
		
		// Generate simulation
		Simulation sim = new Simulation();
		
		// Generate solar system, set parameters, generate planet and star; finalize
		SimulatedSystem system = sim.addSystem("GJ 876");
		system.setAstrometry(ra, dec, parallax, muRa, muDec, vRad);
		system.createStar(magMv, distance, vMinusI, absV, feH, alphaE, mass, spType, magBol, pop);
		system.addPlanet(massPlanet, period, timePeriastron, eccentricity, inclination, omega2, nodeAngle);
		system.finalizeSystem();//*/
		
		// Generated simulated observation data
		system.generateData ( 20.0, true, false, false );
		
		// Plot transit data to file
		system.plotTransitData ( "/home/ebopp/workspace/erroroutput/data/gsimu.out" );
		
	}
	
}
