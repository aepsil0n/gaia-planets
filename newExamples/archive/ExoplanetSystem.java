package newExamples.archive;

import java.io.IOException;
import java.util.*;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.OptimizationException;

import gaia.cu1.tools.exception.GaiaException;
import gaiasimu.SimuException;
import gaiasimu.universe.source.AstrometricParam;
import gaiasimu.universe.source.Photometry;
import gaiasimu.universe.source.stellar.SpectralType;
import gaiasimu.universe.source.stellar.Star;
import gaiasimu.universe.source.stellar.StarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarAstrometry;

/**
 * The class ExoplanetSystem contains a star and multiple exoplanets to be simulated
 */

public class ExoplanetSystem {
	
	/** An array of the planets in the system */
	private ArrayList<Exoplanet> planets;
	
	/** The star of the system */
	private Star star;
	
	/** Name of star as referenced by in exoplanet.eu */
	private String name;
	
	/** Constructors */
	public ExoplanetSystem(String name)
	{
		this.planets = new ArrayList<Exoplanet>();
		this.name = name;
	}
	public ExoplanetSystem()
	{
		this.planets = new ArrayList<Exoplanet>();
	}
	
	/**
	 * Simulates observation process calling functionality of ExoplanetInclination
	 * NOTE: currently works only for single-planet system
	 * 
	 * @throws IOException 
	 * @throws GaiaException 
	 * @throws SimuException 
	 * @throws IllegalArgumentException 
	 * @throws FunctionEvaluationException 
	 * @throws OptimizationException 
	 */
	public void simulate() throws OptimizationException, FunctionEvaluationException, IllegalArgumentException, SimuException, GaiaException, IOException {
		
		// Selects first planet of array list to simulate
		Exoplanet simuPlanet = planets.get(0);
		
		// Fit inclination angle
		simuPlanet.fitExoplanetInclination();

	}
	
	
	/**
	 * Setter for array of planets
	 * 
	 * @param planets
	 */
	public void setPlanets(ArrayList<Exoplanet> planets) {
		this.planets = planets;
	}

	/**
	 * Getter for array of planets
	 * 
	 * @return Array of planets in the system
	 */
	public ArrayList<Exoplanet> getPlanets() {
		return planets;
	}
	
	/**
	 * Adds a new planet to the system
	 * 
	 * @param planet
	 */
	public Exoplanet addPlanet(Exoplanet planet) {
		planets.add(planet);
		return planet;
	}

	/**
	 * Adds a new planet to the system
	 * 
	 * @param star
	 * @param massPlanet
	 * @param period
	 * @param timePeriastron
	 * @param eccentricity
	 * @param inclination
	 * @param omega2
	 * @param nodeAngle
	 * @param subCatalogue
	 * @param useAc
	 * @throws SimuException
	 * @throws GaiaException
	 * @throws IOException
	 */
	public Exoplanet addPlanet(
			Star    star,
			double  massPlanet,
			double  period,
			double  timePeriastron,
			double  eccentricity,
			double	inclination,
			double  omega2,
			double  nodeAngle,
			double  subCatalogue,
			boolean useAc) throws SimuException, GaiaException, IOException {
		Exoplanet planet = new Exoplanet(this,
				new ExoplanetInclination(star, massPlanet, period, timePeriastron, eccentricity, inclination, omega2,
						nodeAngle, subCatalogue, useAc));
		return this.addPlanet(planet);
	}

	/**
	 * Setter for system's star
	 * 
	 * @param star
	 */
	public void setStar(Star star) {
		this.star = star;
	}

	/**
	 * Creates system's star generating a new Star object and all requirements from given
	 * physical parameters
	 * 
	 * NOTE: Can be done with different parameters!
	 * Maybe astrometry, photometry... separately?
	 * 
	 * @param spType	String defining the spectral type
	 * @param magMv		Absolute magnitude. 51 Peg: +4.7. GOG does nothing for G < 12.0
	 * @param distance	Distance in parsec (pc)
	 * @param vMinusI	Intrinsec Mean (V-I) colour. 51 Peg: +0.8
	 * @param absV		Interstellar absorption in the V band
	 * @param ra		Right Ascension ICRS (deg). 51 Peg: 344.3665854
	 * @param dec		Declination ICRS (deg). 51 Peg: 20.76883222
	 * @param parallax	Parallax in mas. 51 Peg: 64.07 mas
	 * @param muRa		Proper motion RA  (mas/yr)
	 * @param muDec		Proper motion DEC (mas/yr)
	 * @param vRad		Radial velocity (km/s)
	 * @param mass		Mass in solar masses (?)
	 * @param magBol	Bolometric magnitude (Compute from bolometric corrections in Allen!)
	 * @param pop		Population number in Besancon model. 6: 5-7 Gyr
	 * @param feH		[Fe/H] ratio
	 * @param alphaE	Alpha elements enhancement
	 * @throws SimuException 
	 */
	public Star createStar(String spType, double magMv, double distance, double vMinusI,
			double absV, double ra, double dec, double parallax, double muRa, double muDec,
			double vRad, double mass, double magBol, int pop, double feH, double alphaE) throws SimuException {
		// not sure if important? random so far...
		long idRoot = 1223345453L;
		
		// Generate spectral type
		SpectralType spectralType = new SpectralType( spType );
		
		// Generate photometry
		Photometry photometry = new Photometry(magMv, distance, vMinusI, absV);
		
		// Generate astrometry
		StellarAstrometry stellarAstrometry = new StellarAstrometry(
				new AstrometricParam(ra, dec, parallax, muRa, muDec, vRad));
		
		// Generate physics.
		// Basic constructor for StarSystem
		StarPhysicalParameters starPhysicalParameters = 
//			new StarPhysicalParameters( teff, feH, logg, mass, spectralType, magBol, radius, pop);
			new StarPhysicalParameters(mass, spectralType, magBol, pop, feH, alphaE);
		
		// Eventually generate new star
		star = new Star(idRoot, photometry, stellarAstrometry, starPhysicalParameters);
		return star;
	}
	
	/**
	 * Getter for system's star
	 * 
	 * @return The star of the system
	 */
	public Star getStar() {
		return star;
	}

	/**
	 * Setter for system name
	 * 
	 * @param name Name of system
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for system name
	 * 
	 * @return Name of system
	 */
	public String getName() {
		return name;
	}

}
