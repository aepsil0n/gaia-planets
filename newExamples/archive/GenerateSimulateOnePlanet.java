package newExamples.archive;

import gaia.cu1.params.GaiaParam;
import gaia.cu1.tools.exception.ErrorMessageFormat;
import gaia.cu1.tools.exception.GaiaException;
import gaiasimu.GaiaPhotometrySystem;
import gaiasimu.GaiaSimuEnums.FoV;
import gaiasimu.SimuException;
import gaiasimu.gaia.Gaia;
import gaiasimu.gaia.payload.psf.CachePSFManager;
import gaiasimu.universe.generator.ExoPlanetGenerator;
import gaiasimu.universe.outputfile.WriteMDBICD;
import gaiasimu.universe.source.AstrometricParam;
import gaiasimu.universe.source.Photometry;
import gaiasimu.universe.source.stellar.OrbitalParams;
import gaiasimu.universe.source.stellar.SpectralType;
import gaiasimu.universe.source.stellar.StarPhysicalParameters;
import gaiasimu.universe.source.stellar.StellarAstrometry;
import gaiasimu.universe.source.stellar.StellarSource;
import gaiasimu.utils.cache.CacheManager;
import gog.GogAsciiWriter;
import gog.GogConstants;
import gog.GogGlobals;
import gog.GogSimulation;
import gog.MDBObject;
import gog.RunnableGogSimulation;
import gog.combined.CombinedDataWriter;
import gog.configuration.GogConfiguration;
import gog.configuration.GogConfigurationMgr;
import gog.epoch.EpochDataWriter;
import gog.exception.GogErrorMessageKeys;
import gog.exception.GogException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import newExamples.gaia.cu4.du438.current_cycle.DmsSimuDU437;

// TODO: Auto-generated Javadoc
/**
 * The Class Test.
 */
public class GenerateSimulateOnePlanet {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 * 
	 * @throws SimuException the simu exception
	 * @throws IOException 
	 * @throws GaiaException 
	 */
	public static void main(String[] args) throws SimuException, IOException, GaiaException{
		
		// GOG configuration file
		String[] gogConfig = { "newDataInput/gogconfig.xml" };
		
		// Output UMSellar.gbin directory
		String   umStellarFileDir = "newGogTemp/";
		String   umStellarFile    = umStellarFileDir + "UMStellar.gbin";
		String   gogOutputDir     = "newDataOutput/";
		String[] gogOutputFiles   = {
				gogOutputDir + "gog7beta_combinedAstrometric.gbin",
				gogOutputDir + "gog7beta_combinedAstroPhysical.gbin",
				gogOutputDir + "gog7beta_combinedPhotometric.gbin",
				gogOutputDir + "gog7beta_combinedRVS.gbin",
				gogOutputDir + "gog7beta_epochAstrometric.gbin",
				gogOutputDir + "gog7beta_epochPhotometric.gbin",
				gogOutputDir + "gog7beta_epochRVS.gbin",
				gogOutputDir + "gog7beta_sourceRVS.gbin" };
		
		// Exoplanet orbital parameters
		double eccentricity   = 0.1;
		double inclination    = 1 * Math.PI / 2;  // (rad)
		double nodeAngle      = 0.4;  // position angle of the line of nodes (rad)
		double omega2         = 0.1;  // argument of periastron (rad)
		double period         = 900;  // (days)
		double semiMajorAxis  = 0.2;  // semi-maj. axis (A.U.) of the secondary (a2)
		double timePeriastron = 1.0;  // (days)
		//double massPlanet     =  50;  // (Jupiter masses)
		double massPlanet     = 50;  // (Jupiter masses)
		
		// Generate orbital parameters
		/*
		OrbitalParams orbitalParams = new OrbitalParams(
				period,
				timePeriastron,
				semiMajorAxis,
				eccentricity,
				omega2,
				inclination,
				nodeAngle);
		*/
		
		// Star data
		long   idRoot   = 1223345453L;
		double magMv    = 12.0;         // Absolute magnitude
		double vMinusI  = 2.0;         // Intrinsec Mean (V-I) colour
		double absV     = 0.1;         // Interstellar absortion in the V band
		double ra       = 344.3665854; // RA  ICRS (deg)
		double dec      = 20.76883222; // DEC ICRS (deg)
		//double parallax = 65.10;       // (mas)
		//double muRa     = 208.07;      // Proper motion RA  (mas/yr)
		//double muDec    = 60.96;       // Proper motion DEC (mas/yr)
		double parallax = 10;       // (mas)
		double distance = 1000 / parallax;// (pc)
		double muRa     = 5;      // Proper motion RA  (mas/yr)
		double muDec    = 5;       // Proper motion DEC (mas/yr)
		double vRad     = -31.2;       // Radial velocity (km/s)
		double feH      = 0.1;         // [Fe/H]
		double alphaE   = 0.0;         // Alpha elements enhancement
		double mass     = 1.2;         // (Msun?)
		double magBol   = 4.3;         // (Compute from bolometric corrections in Allen)
		int    pop      = 6;           // (Besancon model. 6: 5-7 Gyr)
		String spType   = "B5IV";      // String defining the spectral type
		double teff     =  5500;        // (K?)
		double logg     = 4.4;         // log g (cgs)
		double radius   = 2.0;         // (Rsun?)
				
		// Generate spectral type
		SpectralType spectralType = new SpectralType( spType );
		
		// Generate photometry
		Photometry photometry = new Photometry(magMv, distance, vMinusI, absV);
		
		// Generate astrometry
		StellarAstrometry stellarAstrometry = new StellarAstrometry(
				new AstrometricParam(ra, dec, parallax, muRa, muDec, vRad));
		
		// Generate physics.
		// Basic constructor for StarSystem
		StarPhysicalParameters starPhysicalParameters =
//			new StarPhysicalParameters( teff, feH, logg, mass, spectralType, magBol, radius, pop);
			new StarPhysicalParameters(mass, spectralType, magBol, pop, feH, alphaE);
		
		// Generate star
		gaiasimu.universe.source.stellar.Star star =
			new gaiasimu.universe.source.stellar.Star(
					idRoot, photometry, stellarAstrometry, starPhysicalParameters);
		
		
		// Exoplanet generator
		ExoPlanetGenerator exoPlanetGenerator = new ExoPlanetGenerator();
		
		// Generate exoplanet
		//StellarSource exoplanet =  exoPlanetGenerator.generateSystem( star, orbitalParams );		
/*		Exception in thread "main" gaiasimu.SimuException: Mass of component 1 not initialized
		at gaiasimu.universe.source.stellar.StarSystem.computeMass(StarSystem.java:171)
		at gaiasimu.universe.source.stellar.StarSystem.setPhysicalParameters(StarSystem.java:153)
		at gaiasimu.universe.source.stellar.StarSystem.<init>(StarSystem.java:115)
		at gaiasimu.universe.generator.ExoPlanetGenerator.generateSystem(ExoPlanetGenerator.java:174)
		at newExamples.GogGbinTest.main(GogGbinTest.java:232)
*/		
		
		// DU437 simulator
		DmsSimuDU437 dmsSimuDU437 = new DmsSimuDU437();
		OrbitalParams orbitalParams2 = dmsSimuDU437.generateOrbitalParams(
				star, massPlanet, period, timePeriastron, eccentricity, omega2, nodeAngle, inclination);
			
		StellarSource systemRandom    = dmsSimuDU437.generate(star, new Random());
		StellarSource systemOnePlanet = dmsSimuDU437.generateSystem(star, orbitalParams2);
		
		// Write sources to UMStellar file
		ArrayList<gaiasimu.universe.source.AstroSource> objList = new ArrayList<gaiasimu.universe.source.AstroSource>() ; // simulated data
		//objList.add( systemRandom );
		objList.add( systemOnePlanet );
		
		// Erase old UMStellar file, if available, and write the new one
		( new File ( umStellarFile ) ).delete();
		WriteMDBICD writer = new WriteMDBICD( umStellarFileDir );
		writer.launchWrite( objList, false );
		
		
		// Erase ould GOG output files, if available
		for (String gogOutputFile : gogOutputFiles){
			( new File ( gogOutputFile ) ).delete();			
		};
		
		
		
		
		
				
		
		
		
	        


				
		
		// Run GOG
		//GogRun.main( gogConfig );
		
		
		
		
		
		
		// Fragment GOG into pieces
		
		// a) Parse command line arguments
        String gogConfigFileAbsName = gogConfig[0];
        String simulationId = new Long(System.currentTimeMillis()).toString();
    	
        // b) Basic steps
        GogConfigurationMgr.getConfig( gogConfigFileAbsName, simulationId );
        CachePSFManager.setPsfDir( GogGlobals.getPsfPath() );
        CacheManager.getInstance("gaiasimu-cache.xml");
        
        
        
        
        
        
        // c) Main execution method of the Gog simulation.We split the execution process in the following subprocesses
        // 1 - Gaia initialization
        GogConfiguration gc = GogConfigurationMgr.getConfiguration();
               
        // 2 - Generation of the list of sources according the universe model or user sources given as input
        RunnableGogSimulation gogSimulation = new RunnableGogSimulation( gc.getSimulationReference() );
        
        
        // 3 - Delegation to the subprocess that perform astrosourcelist analysis and MDBObject calculation and writting
        
        // gogSimulation.run();
        // start time
        long t0 = System.currentTimeMillis();
	    // initialisation common to processSource and processSpectra : go
        List<gaiasimu.universe.source.AstroSource> sourceList = gogSimulation.initSources();
        
        
	    // gogSimulation.processSourceList(sourceList);
		int sourceListSize = sourceList.size();
		// go
		for (int i = 0; i < sourceListSize; i++) {
			// write AstroSource in the universe model catalogue data
			gaiasimu.universe.source.AstroSource as = sourceList.get(i);
			Random random = new Random();
			random.setSeed(i);
			
			// gogsprocessSource(as, random);
			Gaia gaia = GogSimulation.getGaia();
			double sourceMagnitude = as.getPhotometry().getGaiaMag(GaiaPhotometrySystem.photometryG,
					GogConstants.CATALOGUE_TIME, gc.isPhotometryFromSpectrum());
			
			// AstroSource as= (AstroSource)aSourcesList.get(i).getComponents()[j];
			gaia.cu1.tools.numeric.algebra.GVector3d positionICRS = as.getAstrometry().getICRSPosition(GogConstants.CATALOGUE_TIME);
			// compute all possible transits for this source, using ScanningLaw.inverseScan
			double halfAperture = GaiaParam.Satellite.FOV_AC * GaiaParam.Nature.DEGREE_RADIAN / 2;
			// This method returns the list of all the transits
			double t1 = System.currentTimeMillis();
			List<gaiasimu.gaia.spacecraft.Transit> fovTransits = gaia.attitude.inverseScan(positionICRS, halfAperture, gaia
					.getTelescope(FoV.FOV1));
			List<gaiasimu.gaia.spacecraft.Transit> fovTransits2 = gaia.attitude.inverseScan(positionICRS, halfAperture, gaia
					.getTelescope(FoV.FOV2));
			// System.out.println("HA:" + halfAperture + "HA2:" +halfAperture2 + "ZA:" + zAngle + "ZA2:" + zAngle2);
			fovTransits.addAll(fovTransits2);
			// keep here only the number of transits that have been requested
			if ((gc.getTransitNumber() > 0) && (gc.getTransitNumber() < fovTransits.size())) {
				List<gaiasimu.gaia.spacecraft.Transit> fovTransitsSub = fovTransits.subList(0, gc.getTransitNumber());
				fovTransits = fovTransitsSub;
			}
			// create and write MDBObject parameters as requested
			long t2 = System.currentTimeMillis();
			MDBObject mdbo = createMDBObject(as, fovTransits, random);
			mdbo.update();
			writeMDBObject(mdbo);
			if (as instanceof gaiasimu.universe.source.stellar.StarSystem) {
				gaiasimu.universe.source.AstroSource[] astroSourceComponents = as.getComponents();
				for (gaiasimu.universe.source.AstroSource asComponent : astroSourceComponents) {
					MDBObject componentMDBObject = createMDBObject(asComponent, fovTransits, new Random());
					componentMDBObject.update();
					writeMDBObject(componentMDBObject);
				}
			}

			
			
		}

		

		System.out.println("La cagastes, Burt Lancaster");
	    
	    
	    
	    
	    
	    
	    
		
	}
	
	
	
	/**
	 * This method performs the creation of the MDBObjects for a particular astrosource setting up the configuration.
	 * flags
	 * 
	 * @param anAstroSource
	 *            anAstroSource
	 * @param aFovTransits
	 *            aFovTransits
	 * @param aRandom
	 *            aRandom
	 * @throws GogException
	 *             GogException
	 * @return MDBObject MDBObject
	 */
	private static MDBObject createMDBObject(final gaiasimu.universe.source.AstroSource anAstroSource, final List<gaiasimu.gaia.spacecraft.Transit> aFovTransits,
			final Random aRandom) throws GogException {
		try {
			MDBObject mdbo = new MDBObject(anAstroSource, aRandom, aFovTransits);
			return mdbo;
		} catch (Exception e) {
			throw new GogException(ErrorMessageFormat.format(GogErrorMessageKeys.Data.CREATION_FAILED, "MDB Object : "
					+ e), e);
		}
	}
		
	
	
    /**
     * 
     * @param mdbo
     * @throws GogException
     * @throws SimuException
     */
	private static void writeMDBObject(final MDBObject mdbo) throws GogException, SimuException {
		
		// ?????
		GogConfiguration gc = GogConfigurationMgr.getConfiguration();

		if (gc.getOutputType().equalsIgnoreCase("mdb")) {
			writeDALMDBObject(mdbo);
		} else if (gc.getOutputType().equalsIgnoreCase("ascii")) {
			GogAsciiWriter gaw = new GogAsciiWriter();
			gaw.initializeOutputFiles();
			gaw.writeAsciiMDBObject(mdbo);
		} else {
			throw new GogException(ErrorMessageFormat.format(GogErrorMessageKeys.Data.INVALID_TYPE,
					"ascii | mdb are the accepted types "));
		}

	}
	
	
	
    /**
     * 
     * @param mdbo
     * @throws GogException
     */
	private static void writeDALMDBObject(final MDBObject mdbo) throws GogException {
		
		// ?????
		GogConfiguration gc = GogConfigurationMgr.getConfiguration();
		
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogBpEpochAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogRpEpochAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogRvsEpochAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogBpCombinedAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogRpCombinedAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		gaia.cu1.mdb.cu2.sd.dm.Auxiliary gogRvsCombinedAuxData = new gaia.cu1.mdb.cu2.sd.dmimpl.AuxiliaryImpl();
		
		try {

			long fullId = mdbo.getAstroSource().getFullId();
			
			if (gc.isEpochDataGenerationEnabled()) {

				EpochDataWriter edw = new EpochDataWriter(mdbo.getEpochData(), gc.getOutputDALFormat());
				edw.writeEpochDataAsDAL();
			}

			if (gc.isCombinedDataGenerationEnabled()) {

				CombinedDataWriter cdw = new CombinedDataWriter(mdbo.getCombinedData(), gc.getOutputDALFormat());
				cdw.writeCombinedDataAsDAL();
			}

			// Close and free GT
		} catch (Exception e) {

			throw new GogException(ErrorMessageFormat.format(GogErrorMessageKeys.Data.WRITTING_FAILED,
					"the MDB data : " + e, e));
		}

	}
	
	
}
