package newExamples;

import gaia.cu1.tools.numeric.algebra.GVector2d;
import gaiasimu.GaiaSimuTime;

public class TransitData implements Logging {
	
	// The local scan coordinates
	private GVector2d lsc;
	
	// Error of local scan coordinates
	private GVector2d lscError;
	
	// Time of transit
	private GaiaSimuTime gst;
	
	// Scanning angle at time of transit
	private double scanAngle;
	
	// True, if there is across scan information 
	private boolean useAc;
	
	
	/**
	 *  Constructor from basic values
	 * @param coord			Local scan/plane coordinates as vector (a, d)
	 * @param coordError	Error estimate for local scan/plane coordinates (delta a, delta d)
	 * @param isLPC			true: coordinates are treated as LPC, false: coordinates are treated as LSC
	 * @param gst			Time as GaiaSimuTime object
	 * @param scanAngle		Scanning angle in radians
	 * @param useAc			Is across information available?
	 */
	public TransitData ( GVector2d coord, GVector2d coordError, boolean isLPC, GaiaSimuTime gst, double scanAngle, boolean useAc ) {
		
		// Set scan angle first for transformations
		setScanAngle(scanAngle);
		
		// Set coordinates accoring to system, in which they are provided
		if ( isLPC ) {
			setLPC(coord);
		} else {
			setLSC(coord);
			setLSCError(coordError);
		}
		
		// Set rest of parameters
		setTime(gst);
		setUseAc(useAc);
		
	}

	
	// Getters and setters
	// TODO: documentation
	
	public void setUseAc(boolean useAc) {
		this.useAc = useAc;
	}

	public boolean isUseAc() {
		return useAc;
	}

	public void setScanAngle(double scanAngle) {
		this.scanAngle = scanAngle;
	}

	public double getScanAngle() {
		return scanAngle;
	}

	public void setTime(GaiaSimuTime gst) {
		this.gst = gst;
	}

	public GaiaSimuTime getTime() {
		return gst;
	}

	public void setLSC(GVector2d lsc) {
		this.lsc = lsc;
	}

	public GVector2d getLSC() {
		return lsc;
	}
	
	public void setLPC(GVector2d lpc) {
		this.lsc = Conversion.fromLPCtoLSC(lpc, scanAngle);
	}

	public GVector2d getLPC() {
		return Conversion.fromLSCtoLPC(lsc, scanAngle);
	}

	public GVector2d getLPCError() {
		return Conversion.fromLSCtoLPCError(lscError, scanAngle);
	}

	public void setLSCError(GVector2d lscError) {
		this.lscError = lscError;
	}

	public GVector2d getLSCError() {
		return lscError;
	}
	
}
